<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit62fda02abcef1e02f32aa3ce3628809f
{
    public static $files = array (
        '2c102faa651ef8ea5874edb585946bce' => __DIR__ . '/..' . '/swiftmailer/swiftmailer/lib/swift_required.php',
        'e40631d46120a9c38ea139981f8dab26' => __DIR__ . '/..' . '/ircmaxell/password-compat/lib/password.php',
        '3919eeb97e98d4648304477f8ef734ba' => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib/Crypt/Random.php',
        '58571171fd5812e6e447dce228f52f4d' => __DIR__ . '/..' . '/laravel/framework/src/Illuminate/Support/helpers.php',
    );

    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Component\\Filesystem\\' => 29,
            'Symfony\\Component\\EventDispatcher\\' => 34,
        ),
        'R' => 
        array (
            'Roomy\\' => 6,
        ),
        'P' => 
        array (
            'Patchwork\\' => 10,
        ),
        'O' => 
        array (
            'Omnipay\\PayPal\\' => 15,
        ),
        'M' => 
        array (
            'Monolog\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Component\\Filesystem\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/filesystem',
        ),
        'Symfony\\Component\\EventDispatcher\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/event-dispatcher',
        ),
        'Roomy\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app/Roomy',
        ),
        'Patchwork\\' => 
        array (
            0 => __DIR__ . '/..' . '/patchwork/utf8/src/Patchwork',
        ),
        'Omnipay\\PayPal\\' => 
        array (
            0 => __DIR__ . '/..' . '/omnipay/paypal/src',
        ),
        'Monolog\\' => 
        array (
            0 => __DIR__ . '/..' . '/monolog/monolog/src/Monolog',
        ),
    );

    public static $prefixesPsr0 = array (
        'W' => 
        array (
            'Whoops' => 
            array (
                0 => __DIR__ . '/..' . '/filp/whoops/src',
            ),
            'Way\\Generators' => 
            array (
                0 => __DIR__ . '/..' . '/way/generators/src',
            ),
        ),
        'S' => 
        array (
            'System' => 
            array (
                0 => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib',
            ),
            'Symfony\\Component\\Translation\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/translation',
            ),
            'Symfony\\Component\\Security\\Core\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/security-core',
            ),
            'Symfony\\Component\\Routing\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/routing',
            ),
            'Symfony\\Component\\Process\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/process',
            ),
            'Symfony\\Component\\HttpKernel\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/http-kernel',
            ),
            'Symfony\\Component\\HttpFoundation\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/http-foundation',
            ),
            'Symfony\\Component\\Finder\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/finder',
            ),
            'Symfony\\Component\\DomCrawler\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/dom-crawler',
            ),
            'Symfony\\Component\\Debug\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/debug',
            ),
            'Symfony\\Component\\CssSelector\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/css-selector',
            ),
            'Symfony\\Component\\Console\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/console',
            ),
            'Symfony\\Component\\BrowserKit\\' => 
            array (
                0 => __DIR__ . '/..' . '/symfony/browser-kit',
            ),
            'Stack' => 
            array (
                0 => __DIR__ . '/..' . '/stack/builder/src',
            ),
        ),
        'P' => 
        array (
            'Psr\\Log\\' => 
            array (
                0 => __DIR__ . '/..' . '/psr/log',
            ),
            'Predis' => 
            array (
                0 => __DIR__ . '/..' . '/predis/predis/lib',
            ),
            'Philf\\Setting' => 
            array (
                0 => __DIR__ . '/..' . '/philf/setting/src',
            ),
            'PHPParser' => 
            array (
                0 => __DIR__ . '/..' . '/nikic/php-parser/lib',
            ),
        ),
        'O' => 
        array (
            'Omnipay\\Common\\' => 
            array (
                0 => __DIR__ . '/..' . '/omnipay/common/src',
            ),
        ),
        'N' => 
        array (
            'Net' => 
            array (
                0 => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib',
            ),
        ),
        'M' => 
        array (
            'Math' => 
            array (
                0 => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib',
            ),
        ),
        'L' => 
        array (
            'Laravel\\Cashier\\' => 
            array (
                0 => __DIR__ . '/..' . '/laravel/cashier/src',
            ),
        ),
        'K' => 
        array (
            'KevBaldwyn\\Image' => 
            array (
                0 => __DIR__ . '/..' . '/kevbaldwyn/image/src',
            ),
        ),
        'J' => 
        array (
            'Jeremeamia\\SuperClosure' => 
            array (
                0 => __DIR__ . '/..' . '/jeremeamia/SuperClosure/src',
            ),
        ),
        'I' => 
        array (
            'Imagecow' => 
            array (
                0 => __DIR__ . '/..' . '/imagecow/imagecow',
            ),
            'Illuminate' => 
            array (
                0 => __DIR__ . '/..' . '/laravel/framework/src',
            ),
        ),
        'G' => 
        array (
            'Guzzle\\Tests' => 
            array (
                0 => __DIR__ . '/..' . '/guzzle/guzzle/tests',
            ),
            'Guzzle' => 
            array (
                0 => __DIR__ . '/..' . '/guzzle/guzzle/src',
            ),
        ),
        'F' => 
        array (
            'File' => 
            array (
                0 => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib',
            ),
        ),
        'C' => 
        array (
            'Crypt' => 
            array (
                0 => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib',
            ),
            'ClassPreloader' => 
            array (
                0 => __DIR__ . '/..' . '/classpreloader/classpreloader/src',
            ),
            'Carbon' => 
            array (
                0 => __DIR__ . '/..' . '/nesbot/carbon/src',
            ),
        ),
        'B' => 
        array (
            'Boris' => 
            array (
                0 => __DIR__ . '/..' . '/d11wtq/boris/lib',
            ),
        ),
    );

    public static $classMap = array (
        'Activity' => __DIR__ . '/../..' . '/app/models/Activity.php',
        'AddAddressToProperties' => __DIR__ . '/../..' . '/app/database/migrations/2015_01_03_051115_add_address_to_properties.php',
        'AddAmountToAppliedTenantsTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_08_12_112504_add_amount_to_applied_tenants_table.php',
        'AddBanksToPropertiesTable' => __DIR__ . '/../..' . '/app/database/migrations/2015_01_19_121152_add_banks_to_properties_table.php',
        'AddCashierColumns' => __DIR__ . '/../..' . '/app/database/migrations/2014_09_06_070558_add_cashier_columns.php',
        'AddChoiceToProperties' => __DIR__ . '/../..' . '/app/database/migrations/2015_01_03_070254_add_choice_to_properties.php',
        'AddDepositPaidAndRentPaidToTenantTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_08_22_131557_add_deposit_paid_and_rent_paid_to_tenant_table.php',
        'AddDepositToTenantsTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_08_22_140812_add_deposit_to_tenants_table.php',
        'AddDesToProperties' => __DIR__ . '/../..' . '/app/database/migrations/2015_01_03_103500_add_des_to_properties.php',
        'AddFieldsToActivitiesTable' => __DIR__ . '/../..' . '/app/database/migrations/2015_07_28_091636_add_fields_to_activities_table.php',
        'AddFieldsToPropertiesTable' => __DIR__ . '/../..' . '/app/database/migrations/2015_07_28_133123_add_fields_to_properties_table.php',
        'AddFieldsToTenantsTable' => __DIR__ . '/../..' . '/app/database/migrations/2015_07_28_145839_add_fields_to_tenants_table.php',
        'AddFieldsToTheTenantsTable' => __DIR__ . '/../..' . '/app/database/migrations/2015_07_29_053416_add_fields_to_the_tenants_table.php',
        'AddHelpTable' => __DIR__ . '/../..' . '/app/database/migrations/2015_07_28_132428_add_help_table.php',
        'AddKeywordToSavesearch' => __DIR__ . '/../..' . '/app/database/migrations/2015_01_08_063105_add_keyword_to_savesearch.php',
        'AddMainRenterToAppliedTenants' => __DIR__ . '/../..' . '/app/database/migrations/2015_07_28_132527_add_main_renter_to_applied_tenants.php',
        'AddMoreInfoToApplicationsTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_08_12_102757_add_more_info_to_applications_table.php',
        'AddPaypalEmailToUserTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_08_25_140349_add_paypal_email_to_user_table.php',
        'AddPriceToProperties' => __DIR__ . '/../..' . '/app/database/migrations/2015_01_03_055931_add_price_to_properties.php',
        'AddSubscriptionIdToPropertyTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_08_30_095934_add_subscription_id_to_property_table.php',
        'AddUseridToRatingsTable' => __DIR__ . '/../..' . '/app/database/migrations/2015_01_14_142319_add_userid_to_ratings_table.php',
        'AdminController' => __DIR__ . '/../..' . '/app/controllers/adminController.php',
        'Application' => __DIR__ . '/../..' . '/app/models/Application.php',
        'Applied_tenant' => __DIR__ . '/../..' . '/app/models/Applied_tenant.php',
        'BaseController' => __DIR__ . '/../..' . '/app/controllers/BaseController.php',
        'CreateActivitiesTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_07_22_110206_create_activities_table.php',
        'CreateApplicationsTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_07_22_110131_create_applications_table.php',
        'CreateAppliedTenantsTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_07_22_122041_create_applied_tenants_table.php',
        'CreateDepositTransactionsTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_08_25_080210_create_deposit_transactions_table.php',
        'CreateHelpTable' => __DIR__ . '/../..' . '/app/database/migrations/2015_07_28_112034_create_help_table.php',
        'CreatePaypalDataTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_08_25_101107_create_paypal_data_table.php',
        'CreatePropertiesTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_07_22_101758_create_properties_table.php',
        'CreatePropertyPhotosTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_07_22_105228_create_property_photos_table.php',
        'CreateRentTransactionsTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_08_25_080131_create_rent_transactions_table.php',
        'CreateRolesTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_07_22_095638_create_roles_table.php',
        'CreateSendRequestTable' => __DIR__ . '/../..' . '/app/database/migrations/2015_07_28_091335_create_send_request_table.php',
        'CreateTableRatings' => __DIR__ . '/../..' . '/app/database/migrations/2015_01_06_081446_create_table_ratings.php',
        'CreateTableSavesearchs' => __DIR__ . '/../..' . '/app/database/migrations/2015_01_06_112154_create_table_savesearchs.php',
        'CreateTenantsTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_07_22_110243_create_tenants_table.php',
        'CreateUsersTable' => __DIR__ . '/../..' . '/app/database/migrations/2014_07_22_095623_create_users_table.php',
        'DatabaseSeeder' => __DIR__ . '/../..' . '/app/database/seeds/DatabaseSeeder.php',
        'Deposit_transaction' => __DIR__ . '/../..' . '/app/models/Deposit_transaction.php',
        'Help' => __DIR__ . '/../..' . '/app/models/Help.php',
        'HomeController' => __DIR__ . '/../..' . '/app/controllers/HomeController.php',
        'IlluminateQueueClosure' => __DIR__ . '/..' . '/laravel/framework/src/Illuminate/Queue/IlluminateQueueClosure.php',
        'LandlordController' => __DIR__ . '/../..' . '/app/controllers/LandlordController.php',
        'Normalizer' => __DIR__ . '/..' . '/patchwork/utf8/src/Normalizer.php',
        'Omnipay\\Omnipay' => __DIR__ . '/..' . '/omnipay/common/src/Omnipay/Omnipay.php',
        'Property' => __DIR__ . '/../..' . '/app/models/Property.php',
        'Property_photo' => __DIR__ . '/../..' . '/app/models/Property_photo.php',
        'Rating' => __DIR__ . '/../..' . '/app/models/Rating.php',
        'Rent_transaction' => __DIR__ . '/../..' . '/app/models/Rent_transaction.php',
        'RenterController' => __DIR__ . '/../..' . '/app/controllers/RenterController.php',
        'Role' => __DIR__ . '/../..' . '/app/models/Role.php',
        'RolesTableSeeder' => __DIR__ . '/../..' . '/app/database/seeds/RolesTableSeeder.php',
        'Rooms' => __DIR__ . '/../..' . '/app/controllers/Rooms.php',
        'Savesearch' => __DIR__ . '/../..' . '/app/models/Savesearch.php',
        'SendRequest' => __DIR__ . '/../..' . '/app/models/SendRequest.php',
        'SessionHandlerInterface' => __DIR__ . '/..' . '/symfony/http-foundation/Symfony/Component/HttpFoundation/Resources/stubs/SessionHandlerInterface.php',
        'Stripe' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Stripe.php',
        'Stripe_Account' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Account.php',
        'Stripe_ApiConnectionError' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/ApiConnectionError.php',
        'Stripe_ApiError' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/ApiError.php',
        'Stripe_ApiRequestor' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/ApiRequestor.php',
        'Stripe_ApiResource' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/ApiResource.php',
        'Stripe_ApplicationFee' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/ApplicationFee.php',
        'Stripe_ApplicationFeeRefund' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/ApplicationFeeRefund.php',
        'Stripe_AttachedObject' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/AttachedObject.php',
        'Stripe_AuthenticationError' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/AuthenticationError.php',
        'Stripe_Balance' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Balance.php',
        'Stripe_BalanceTransaction' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/BalanceTransaction.php',
        'Stripe_BitcoinReceiver' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/BitcoinReceiver.php',
        'Stripe_BitcoinTransaction' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/BitcoinTransaction.php',
        'Stripe_Card' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Card.php',
        'Stripe_CardError' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/CardError.php',
        'Stripe_Charge' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Charge.php',
        'Stripe_Coupon' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Coupon.php',
        'Stripe_Customer' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Customer.php',
        'Stripe_Error' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Error.php',
        'Stripe_Event' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Event.php',
        'Stripe_FileUpload' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/FileUpload.php',
        'Stripe_InvalidRequestError' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/InvalidRequestError.php',
        'Stripe_Invoice' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Invoice.php',
        'Stripe_InvoiceItem' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/InvoiceItem.php',
        'Stripe_List' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/List.php',
        'Stripe_Object' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Object.php',
        'Stripe_Plan' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Plan.php',
        'Stripe_RateLimitError' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/RateLimitError.php',
        'Stripe_Recipient' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Recipient.php',
        'Stripe_Refund' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Refund.php',
        'Stripe_RequestOptions' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/RequestOptions.php',
        'Stripe_SingletonApiResource' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/SingletonApiResource.php',
        'Stripe_Subscription' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Subscription.php',
        'Stripe_Token' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Token.php',
        'Stripe_Transfer' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Transfer.php',
        'Stripe_Util' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Util.php',
        'Stripe_Util_Set' => __DIR__ . '/..' . '/stripe/stripe-php/lib/Stripe/Util/Set.php',
        'Tenant' => __DIR__ . '/../..' . '/app/models/Tenant.php',
        'TestCase' => __DIR__ . '/../..' . '/app/tests/TestCase.php',
        'Url' => __DIR__ . '/../..' . '/app/models/Url.php',
        'User' => __DIR__ . '/../..' . '/app/models/User.php',
        'UserController' => __DIR__ . '/../..' . '/app/controllers/UserController.php',
        'Whoops\\Module' => __DIR__ . '/..' . '/filp/whoops/src/deprecated/Zend/Module.php',
        'Whoops\\Provider\\Zend\\ExceptionStrategy' => __DIR__ . '/..' . '/filp/whoops/src/deprecated/Zend/ExceptionStrategy.php',
        'Whoops\\Provider\\Zend\\RouteNotFoundStrategy' => __DIR__ . '/..' . '/filp/whoops/src/deprecated/Zend/RouteNotFoundStrategy.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit62fda02abcef1e02f32aa3ce3628809f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit62fda02abcef1e02f32aa3ce3628809f::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit62fda02abcef1e02f32aa3ce3628809f::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit62fda02abcef1e02f32aa3ce3628809f::$classMap;

        }, null, ClassLoader::class);
    }
}
