(function($)
{
	if (typeof Dropzone != 'undefined')
		Dropzone.autoDiscover = false;

	if ($.fn.dropzone != 'undefined')
		$('.dropzone').dropzone();
})(jQuery);

Dropzone.autoDiscover = false;
Dropzone.options.profilePicUpload = {
    url: routes.postEditProfile,
    maxFileSize: 3,
    autoProcessQueue: true,
    headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    paramName: "profile_pic",
    maxFiles: 1
};