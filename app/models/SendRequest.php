<?php

class SendRequest extends \Eloquent {
	protected $fillable = ['property_id','choice','user_id','first_name','last_name','email','phone_number','description'];
		protected $table = 'send_request';
}