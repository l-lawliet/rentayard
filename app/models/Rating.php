<?php

class Rating extends \Eloquent {
	protected $fillable = ['user_id','property_id','overall','safety','pet_friendly','walkability','restaurants',
							'parks','schools','traffic','parking','entertainment','transportation','community','cleanliness','review_title',
							'description','know_by'];
		protected $table = 'ratings';
}