<?php

class Role extends \Eloquent {
    use SoftDeletingTrait;
    protected $fillable = [];

    public function users(){
        $this->hasMany('User');
    }

}