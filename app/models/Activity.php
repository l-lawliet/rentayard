<?php

class Activity extends \Eloquent {
	protected $fillable = [];
    use SoftDeletingTrait;

    public function application(){
        return $this->belongsTo('Application')->with('Property','primary_tenant');
    }

    public function property(){
        return $this->belongsTo('Property');
    }

    public function user(){
        return $this->belongsTo('User');
    }
}