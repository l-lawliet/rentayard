<?php

class Rent_transaction extends \Eloquent {
	protected $fillable = [];

    public function user(){
        return $this->belongsTo('User');
    }

    public function property(){
        return $this->belongsTo('Property');
    }
    
}