<?php

class Tenant extends \Eloquent {
    use SoftDeletingTrait;
    protected $fillable = [];

    public function property()
    {
        return $this->belongsTo('Property');
    }

    public function user(){
        return $this->belongsTo('User');
    }
}