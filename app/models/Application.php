<?php

class Application extends \Eloquent {
    protected $fillable = [];
    use SoftDeletingTrait;

    public function property(){
        return $this->belongsTo('Property');
    }

    public function applicants(){
        return $this->hasMany('Applied_tenant')->with('user');
    }

    public function primary_tenant(){
        return $this->belongsTo('User','primary_tenant_id');
    }
}