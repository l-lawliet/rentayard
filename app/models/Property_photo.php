<?php

class Property_photo extends \Eloquent {
    use SoftDeletingTrait;
    protected $fillable = [];

    public function property(){
        return $this->belongsTo('Property');
    }
}