<?php

class Property extends \Eloquent {
    use SoftDeletingTrait;
    protected $fillable = [];

    public function landlord(){
        return $this->belongsTo('User','user_id');
    }

    public function photos(){
        return $this->hasMany('Property_photo');
    }

    public function applications(){
        return $this->hasMany('Application')->with('applicants');
    }

    public function applications_with_trashed(){
        return $this->hasMany('Application')->withTrashed()->with('applicants');
    }

    public function tenants(){
        return $this->hasMany('Tenant');
    }

}