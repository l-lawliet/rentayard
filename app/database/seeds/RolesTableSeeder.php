<?php

class RolesTableSeeder extends Seeder {

	public function run()
	{

        $role = new Role;
        $role->label = 'landlord';
        $role->save();

        $role = new Role;
        $role->label = 'renter';
        $role->save();

        $role = new Role;
        $role->label = 'admin';
        $role->save();

	}

}