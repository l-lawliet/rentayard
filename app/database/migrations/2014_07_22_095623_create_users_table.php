<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('password');
            $table->boolean('is_activated');
            $table->string('activation_code');
            $table->string('reset_password_code');
            $table->integer('role_id');
            $table->integer('credits');
            $table->string('pets');
            $table->string('phone');
            $table->integer('salary');
            $table->string('profile_picture');
            $table->text('work_history');
            $table->text('additional_income');
            $table->text('rental_history');
            $table->text('about');
            $table->string('designation');
            $table->text('address');
						$table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
