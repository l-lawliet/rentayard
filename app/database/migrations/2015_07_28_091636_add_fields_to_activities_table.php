<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldsToActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            DB::statement("ALTER TABLE activities CHANGE COLUMN activity activity ENUM('joined_roomy', 'applied', 'friend_applied','landlord_approved','landlord_denied','approved','rejected','friend_approved','friend_rejected','paid_rent','paid_deposit','send_rent_request','send_sale_request')");

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        //rollback
	}

}
