<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('properties', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->integer('user_id');
            $table->integer('rent_start_date');
						$table->integer('month');
						$table->integer('year');
            $table->enum('available', array('0', '1'));
						$table->enum('shared', array('bathrooms', 'bedrooms', 'bathrooms/bedrooms', 'kitchen', 'bathrooms/kitchen', 'bedrooms/kitchen','everything'));
            $table->boolean('accept_application');
            $table->string('city');
            $table->string('state');
            $table->integer('zip');
            $table->integer('rent');
            $table->integer('deposit');
            $table->integer('bedrooms');
            $table->integer('bathrooms');
            $table->integer('parking');
            $table->enum('pets_allowed',array('No','dogs','cats','dogs/cats','everything'));
						$table->enum('utility_bill',array('Water','Electric','Water/Electric','None'));
						$table->enum('Amenities',array('Wifi','Cable','No', 'Wifi/Cable'));
						$table->enum('type',array('locals','hotels'));
						$table->string('property_rules');
						$table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('properties');
	}

}
