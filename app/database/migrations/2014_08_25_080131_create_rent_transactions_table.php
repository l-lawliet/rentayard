<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRentTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rent_transactions', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('amount');
            $table->integer('property_id');
            $table->integer('month');
            $table->enum('mode',array('paypal','offline'));
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rent_transactions');
	}

}
