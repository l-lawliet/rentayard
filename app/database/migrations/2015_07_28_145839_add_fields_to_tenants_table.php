<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldsToTenantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tenants', function(Blueprint $table)
		{
			$table->integer('ask_for_vacate');
            $table->integer('vacate_status');
            $table->integer('vacate_from');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tenants', function(Blueprint $table)
		{
			$table->dropColumn('ask_for_vacate');
			$table->dropColumn('vacate_from');
			$table->dropColumn('vacate_status');
		});
	}

}
