<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activities', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id');
            $table->enum('activity',array(
                                            'joined_rentayawd',         // Joined Roomy
                                            'applied',              // You Applied for a property
                                            'friend_applied',       // Your Friend applied for a property
                                            'landlord_approved',    // Landlord Approved your application
                                            'landlord_denied',      // Landlord Denied your application
                                            'approved',             // You approved the offer by landlord
                                            'rejected',             // You Rejected the offer by landlord
                                            'friend_approved',      // Your friend approved the offer by Landlord
                                            'friend_rejected',      // Your friend rejected the offer by Landlord
                                            'paid_rent',            // Paid rent
                                            'paid_deposit'          // Paid deposit
                                        ));
            $table->integer('property_id')->nullable();
            $table->integer('application_id')->nullable();
			$table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activities');
	}

}
