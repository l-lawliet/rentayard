<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDepositPaidAndRentPaidToTenantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tenants', function(Blueprint $table)
		{
			$table->boolean('depositPaid');
            $table->boolean('rentPaid');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tenant', function(Blueprint $table)
		{
			
		});
	}

}
