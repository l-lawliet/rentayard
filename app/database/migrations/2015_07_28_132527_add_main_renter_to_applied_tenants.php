<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMainRenterToAppliedTenants extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('applied_tenants', function(Blueprint $table)
		{
			$table->integer('main_renter');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('applied_tenants', function(Blueprint $table)
		{
			$table->dropColumn('main_renter');
		});
	}

}
