<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRatings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ratings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('safety');
			$table->integer('pet_friendly');
			$table->integer('walkability');
			$table->integer('restaurants');
			$table->integer('parks');
			$table->integer('schools');
			$table->integer('traffic');
			$table->integer('parking');
			$table->integer('entertainment');
			$table->integer('transportation');
			$table->integer('community');
			$table->integer('cleanliness');
			$table->integer('overall');
			$table->integer('review_title');
			$table->integer('description');
			$table->integer('know_by');
			$table->timestamps();


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ratings');
	}

}
