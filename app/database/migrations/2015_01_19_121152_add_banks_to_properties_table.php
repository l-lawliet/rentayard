<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBanksToPropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('properties', function(Blueprint $table)
		{
			$table->integer('hschool');
			$table->integer('mschool');
			$table->integer('eschool');
			$table->integer('hospital');
			$table->integer('restaurants');
			$table->integer('groceries');
			$table->integer('banks');
			$table->integer('gas');
			$table->integer('bus');
			$table->integer('rails');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('properties', function(Blueprint $table)
		{
			$table->dropColumn('hschool');
			$table->dropColumn('mschool');
			$table->dropColumn('eschool');
			$table->dropColumn('hospital');
			$table->dropColumn('restaurants');
			$table->dropColumn('groceries');
			$table->dropColumn('banks');
			$table->dropColumn('gas');
			$table->dropColumn('bus');
			$table->dropColumn('rails');
		});	
	}

}
