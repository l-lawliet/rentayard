<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressToProperties extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('properties', function(Blueprint $table)
		{
			$table->string('address')->nullable();
			$table->string('street_num')->nullable();
			$table->string('street_name')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('properties', function(Blueprint $table)
		{
			$table->dropColumn(
				'address', 'street_num', 'street_name', 'latitude', 'longitude'
			);
		});		
	}

}
