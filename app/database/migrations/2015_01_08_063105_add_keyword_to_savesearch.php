<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeywordToSavesearch extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('savesearchs', function(Blueprint $table)
		{
			$table->string('keyword',2555)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('savesearchs', function(Blueprint $table)
		{
			$table->dropColumn('keyword');
		});	
	}

}
