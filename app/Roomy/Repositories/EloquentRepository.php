<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 23/07/14
 * Time: 4:34 PM
 */

namespace Roomy\Repositories;

abstract class EloquentRepository {

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function getErrors()
    {
        return $this->errors;
    }

  

} 