<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 29/07/14
 * Time: 1:03 PM
 */

namespace Roomy\Repositories\Admin;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider{

    public function register(){
        $this->app->bind(
            'Roomy\Repositories\Admin\AdminRepository',
            'Roomy\Repositories\Admin\EloquentAdminRepository'
        );
    }

}