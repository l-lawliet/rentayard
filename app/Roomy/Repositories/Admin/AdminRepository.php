<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 29/07/14
 * Time: 1:02 PM
 */

namespace Roomy\Repositories\Admin;


/**
 * Interface LandlordRepository
 * @package Roomy\Repositories\Landlord
 */
interface AdminRepository {

    public function getUsers();

    public function deleteUser($id);

    public function updateSettings($input);

    public function updateEmailTemplates($input);

    public function getUser($id);

    public function updateUser($id, $input);

    public function updateHelp($id);

    public function addPropertyRent($input,$id);

    public function findProperty($id);

    public function updateProperty($id,$input);

    public function updatePropertySale($id,$input);

}