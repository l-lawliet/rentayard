<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 29/07/14
 * Time: 1:02 PM
 */

namespace Roomy\Repositories\Admin;

use Roomy\Repositories\EloquentRepository;
use User;
use Setting;
use Help;
use Property;
use Property_photo;
use Validator;
use Input;
use URL;
use SendRequest;


class EloquentAdminRepository extends EloquentRepository implements AdminRepository {


    public function getUsers()
    {
        $user = User::paginate(10);
        return $user;
    }

    public function deleteUser($id)
    {
        $property= Property::where('user_id',$id)->get();
        foreach($property as $user_pro)
        {
            $user_property = Property::find($user_pro->id);
            $user_property->showcase = 0;
            $user_property->save();
             
        }

//        Applicaton::where('primary_tenant_id',$id)->delete();
//        Applied_tenant::where('user_id',$id)->delete();
//        Deposit_transaction::where('user_id',$id)->delete();
//        Help::where('user_id',$id)->delete();
//        Rent_transaction::where('user_id',$id)->delete();
//        Rating::where('user_id',$id)->delete();
//        Tenant::where('user_id',$id)->delete();
//
//        $property_request= SendRequest::where('user_id',$id)->delete();
        
        return User::find($id)->delete();
    }
    
    public function addUser(){
        
        //to add a user back a user we check the email address then if
        //if exist we resend an email that will clear the soft delete
        
    }

    public function updateSettings($input)
    {
        if($input['logo'] == "")
        {
            Setting::set('sitename',$input['sitename']);
            Setting::set('subscription_amount',$input['subscription_amount']);
            Setting::set('footer',$input['footer']);
            return true;
        }else
        {
            $validator = Validator::make(array(    
            'logo' => $input['logo']),
            array('logo' => 'required|mimes:png'));
            if($validator->fails())
            {
            $error = $validator->messages();
            $this->errors = $error->toArray();
            return false;
            }
            else
            {
            Setting::set('sitename',$input['sitename']);
            Setting::set('subscription_amount',$input['subscription_amount']);
            Setting::set('footer',$input['footer']);
            $file_name = time();
            $file_name .= rand();
            $ext = Input::file('logo')->getClientOriginalExtension();
            Input::file('logo')->move(public_path() . "/uploads", $file_name . "." . $ext);
            $local_url = $file_name . "." . $ext;
            $s3_url = URL::to('/') . '/uploads/' . $local_url;

            Setting::set('logo',$s3_url);

            return true;
            }

        }

   }

    public function updateEmailTemplates($input)
    {
        Setting::set('email.renterApplicationRequest',$input['renterApplicationRequest']);
        Setting::set('email.landlordApplicationConfirmation',$input['landlordApplicationConfirmation']);
        Setting::set('email.coapplicantInvite',$input['coapplicantInvite']);
        Setting::set('email.rentReminder',$input['rentReminder']);
        Setting::set('email.activationMail',$input['activationMail']);
        Setting::set('email.applicationRejection',$input['applicationRejection']);
        return true;
    }

    public function getUser($id)
    {
        $user = User::find($id);

        return $user;
    }

    public function updateHelp($id)
    {
        $help = Help::find($id);
        $help->status = 1;
        return $help->save();
    }

    public function updateUser($id, $input)
    {
        $user = User::find($id);
        $user->first_name   =   $input['first_name'];
        $user->last_name    =   $input['last_name'];
        $user->phone        =   $input['phone'];
        $user->salary       =   $input['salary'];
        $user->designation  =   $input['designation'];
        $user->about        =   $input['about'];
        $user->address      =   $input['address'];
        $user->pets         =   (isset($input['pets']))?implode('|',$input['pets']):'';

        $rental_history = [];

        if(isset($input['houseAddress']))
        {
            for($i=0;$i<count($input['houseAddress']);$i++)
            {
                $rental = [
                    'address'   =>  $input['houseAddress'][$i],
                    'rent'      =>  $input['houseRent'][$i],
                    'tenancyLength'    =>  $input['houseTenancyLength'][$i],
                    'landlordFirstName' =>  $input['houseLandlordFirstName'][$i],
                    'landlordLastName'  =>  $input['houseLandlordLastName'][$i],
                    'landlordEmail'     =>  $input['houseLandlordEmail'][$i],
                    'landlordPhone'     =>  $input['houseLandlordPhone'][$i]
                ];

                $rental_history[] = $rental;
            }

            $user->rental_history = json_encode($rental_history);
        }


        if(isset($input['workCompany']))
        {
            $work_history = [];

            for($i=0;$i<count($input['workCompany']);$i++)
            {
                $work = [
                    'company'   =>  $input['workCompany'][$i],
                    'designation'      =>  $input['workDesignation'][$i],
                    'employmentLength'    =>  $input['workEmploymentLength'][$i],
                    'salary' =>  $input['workSalary'][$i]
                ];

                $work_history[] = $work;
            }

            $user->work_history = json_encode($work_history);
        }


        if(isset($input['incomeSource']))
        {
            $additional_income = [];

            for($i=0;$i<count($input['incomeSource']);$i++)
            {
                $income = [
                    'source'   =>  $input['incomeSource'][$i],
                    'amount'      =>  $input['incomeAmount'][$i]
                ];

                $additional_income[] = $income;
            }

            $user->additional_income = json_encode($additional_income);
        }


        return $user->save();
    }

    public function findProperty($id)
    {
        return Property::where('id',$id)->with('photos','landlord')->first();
    }

    public function updateProperty($id,$input){
        
        $name=$input['name'];
        $city=$input['city'];
        $state=$input['state'];
        $zip=$input['zip'];
        $rent=$input['rent'];
        $bedrooms=$input['bedrooms'];
        $bathrooms=$input['bathrooms'];
        $sqft=$input['sqft'];
        $parking=$input['parking'];
        $rentStartDate=$input['rentStartDate'];


        $rules = array(
        'name' => 'required',
        'city'    =>  'required',
        'state'   =>  'required',
        'zip'     =>  'required|integer',
        'rent'     =>  'required|integer',
        'deposit' =>  'required|integer',
        'bedrooms' =>  'required|integer',
        'bathrooms' =>  'required|integer',
        'sqft'      =>  'required|integer',
        'parking'   =>  'required|integer',
        'rentStartDate' => 'required|integer'
        );
 

        $validator = Validator::make($input,$rules);
   


        

        if($validator->fails())
        {

            $this->errors = $validator->getErrors()->toArray();
            return false;
        
        }
        else
        {

            $property = Property::find($id);
            $property->name = $input['name'];
            $property->city = $input['city'];
            $property->state= $input['state'];
            $property->zip  = $input['zip'];
            $property->rent = $input['rent'];
            $property->deposit  =   $input['deposit'];
            $property->bedrooms =   $input['bedrooms'];
            $property->bathrooms=   $input['bathrooms'];
            $property->sqft    =   $input['sqft'];
            $property->rent_start_date  =   $input['rentStartDate'];
            $property->parking  =   $input['parking'];
            $property->des = $input['des'];
            $property->user_id  =  $id;

            if(isset($input['acceptApplication']))
            {
                $property->accept_application = 1;
            }
            else
            {
                $property->accept_application = 0;
            }

            $property->save();



            return true;
        }
            
    }




    public function addPropertySale($input,$id){
        $name=$input['name'];
        $city=$input['city'];
        $state=$input['state'];
        $zip=$input['zip'];
        $price=$input['price'];
        $bedrooms=$input['bedrooms'];
        $bathrooms=$input['bathrooms'];
        $sqft=$input['sqft'];
        $parking=$input['parking'];
               

        $rules = array(
       'name'    =>  'required',
        'city'    =>  'required',
        'state'   =>  'required',
        'zip'     =>  'required|integer',
        'price'    =>  'required|integer',
        'bedrooms' =>  'required|integer',
        'bathrooms' =>  'required|integer',
        'sqft'      =>  'required|integer',
        'parking'   =>  'required|integer'
        );
 

        $validator = Validator::make($input,$rules);
   


        if($validator->fails())
        {

            $this->errors = $validator->getErrors()->toArray();
            return false;
        
        }
        else
        {
            $addres = $input['address'] . $input['zip'];

            $property = new Property;
            $property->name = $input['name'];
            $property->address = $addres;
            $property->street_num = $input['street_num'];
            $property->street_name = $input['street_name'];
            $property->latitude = $input['lat'];
            $property->longitude = $input['lon'];
            $property->city = $input['city'];
            $property->state= $input['state'];
            $property->zip  = $input['zip'];
            $property->price = $input['price'];
            $property->bedrooms =   $input['bedrooms'];
            $property->bathrooms=   $input['bathrooms'];
            $property->sqft    =   $input['sqft'];
            $property->parking  =   $input['parking'];
            $property->user_id  =   $id;
            $property->choice = 1;
            $property->des = $input['des'];
            $property->hschool = $input['hschool'];
            $property->mschool = $input['mschool'];
            $property->eschool = $input['eschool'];
            $property->hospital = $input['hospital'];
            $property->restaurants = $input['restaurants'];
            $property->groceries = $input['groceries'];
            $property->banks = $input['banks'];
            $property->gas = $input['gasStation'];
            $property->bus = $input['bus'];
            $property->rails = $input['rails'];
            $property->showcase = 1;


            $property->save();

            if(Input::hasFile('photos'))
            {
                $files = Input::file('photos');
                 $filename = strtotime('now').$files->getClientOriginalName();
                    $files->move('uploads/property_photos/',$filename);
                    $photo = new Property_photo;
                    $photo->property_id = $property->id;
                    $photo->filename = $filename;
                    $photo->save();

                // foreach($files as $file) {
                //     $filename = strtotime('now').$file->getClientOriginalName();
                //     $file->move('uploads/property_photos/',$filename);
                //     $photo = new Property_photo;
                //     $photo->property_id = $property->id;
                //     $photo->filename = $filename;
                //     $photo->save();
                // }
            }

            return true;
        }
         
    }
    
    public function addPropertyRent($input,$id){
        
        $name=$input['name'];
        $city=$input['city'];
        $state=$input['state'];
        $zip=$input['zip'];
        $rent=$input['rent'];
        $bedrooms=$input['bedrooms'];
        $bathrooms=$input['bathrooms'];
        $sqft=$input['sqft'];
        $parking=$input['parking'];
        $rentStartDate=$input['rentStartDate'];


       

        $rules = array(
        'name' => 'required',
        'city'    =>  'required',
        'state'   =>  'required',
        'zip'     =>  'required|integer',
        'rent'    =>  'required|integer',
        'deposit' =>  'required|integer',
        'bedrooms' =>  'required|integer',
        'bathrooms' =>  'required|integer',
        'sqft'      =>  'required|integer',
        'parking'   =>  'required|integer',
        'rentStartDate' => 'required|integer'
        );
 

        $validator = Validator::make($input,$rules);
   


        

        if($validator->fails())
        {

            $this->errors = $validator->getErrors()->toArray();
            return false;
        
        }
        else
        {
            $addres = $input['address'] . $input['zip'];

            $property = new Property;
            $property->name = $input['name'];
            $property->address = $addres;
            $property->street_num = $input['street_num'];
            $property->street_name = $input['street_name'];
            $property->latitude = $input['lat'];
            $property->longitude = $input['lon'];
            $property->city = $input['city'];
            $property->state= $input['state'];
            $property->zip  = $input['zip'];
            $property->rent = $input['rent'];
            $property->deposit  =   $input['deposit'];
            $property->bedrooms =   $input['bedrooms'];
            $property->bathrooms=   $input['bathrooms'];
            $property->sqft    =   $input['sqft'];
            $property->rent_start_date  =   $input['rentStartDate'];
            $property->parking  =   $input['parking'];
            $property->user_id  =   $id;
            $property->choice = 0;
            $property->des = $input['des'];

            if(isset($input['acceptApplication']))
            {
                $property->accept_application = 1;
            }
            else
            {
                $property->accept_application = 0;
            }

            if(isset($input['showcase']))
            {
                $property->showcase = 1;
            }
            else
            {
                $property->showcase = 0;
            }

            $property->save();

            if(Input::hasFile('photos'))
            {

                $files = Input::file('photos');
                $filename = strtotime('now').$files->getClientOriginalName();
                    $files->move('uploads/property_photos/',$filename);
                    $photo = new Property_photo;
                    $photo->property_id = $property->id;
                    $photo->filename = $filename;
                    $photo->save();

                // //return $files;
                //     var_dump($files->getClientOriginalName());


                // foreach($files as $file) {

                //     var_dump($file->getClientOriginalName());

                //     $filename = strtotime('now').$file->getClientOriginalName();
                //     $file->move('uploads/property_photos/',$filename);
                //     $photo = new Property_photo;
                //     $photo->property_id = $property->id;
                //     $photo->filename = $filename;
                //     $photo->save();
                // }
            }

            return true;
        }
        
    }

    public function updatePropertySale($id,$input){
       $name=$input['name'];
        $city=$input['city'];
        $state=$input['state'];
        $zip=$input['zip'];
        $price=$input['price'];
        $bedrooms=$input['bedrooms'];
        $bathrooms=$input['bathrooms'];
        $sqft=$input['sqft'];
        $parking=$input['parking'];
        


       

        $rules = array(
       'name'    =>  'required',
        'city'    =>  'required',
        'state'   =>  'required',
        'zip'     =>  'required|integer',
        'price'    =>  'required|integer',
        'bedrooms' =>  'required|integer',
        'bathrooms' =>  'required|integer',
        'sqft'      =>  'required|integer',
        'parking'   =>  'required|integer'
        );
 

        $validator = Validator::make($input,$rules);
   


        if($validator->fails())
        {

            $this->errors = $validator->getErrors()->toArray();
            return false;
        
        }
        else
        {

            $property = Property::find($id);
            $property->name = $input['name'];
            $property->city = $input['city'];
            $property->state= $input['state'];
            $property->zip  = $input['zip'];
            $property->price = $input['price'];
            $property->bedrooms =   $input['bedrooms'];
            $property->bathrooms=   $input['bathrooms'];
            $property->sqft    =   $input['sqft'];
            $property->des = $input['des'];
            $property->parking  =   $input['parking'];
            $property->user_id  =   $id;


            $property->save();



            return true;
        }
         
    }

 


}