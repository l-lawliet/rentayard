<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 23/07/14
 * Time: 6:21 PM
 */

namespace Roomy\Repositories\User;

use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider{


    public function register(){
        $this->app->bind(
            'Roomy\Repositories\User\UserRepository',
            'Roomy\Repositories\User\EloquentUserRepository'
        );
    }

}