<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 23/07/14
 * Time: 6:11 PM
 */

namespace Roomy\Repositories\User;


interface UserRepository {

    /**
     *	Fetch a record by id
     *
     *	@param $id
     *
     * 	@return User
     */

    public function getById($id);

    /**
     * Fetch Ten users with pagination
     *
     * @return Users
     */

    public function getTen();

    /**
     *	Attempt to Login
     *
     *	@param $email
     *	@param $password
     *
     * @return  Response
     */

    public function attemptLogin($email,$password);

    public function forgotPassowrd($loginEmail);

    /**
     * Logout a user
     *
     * @return Response
     */

    public function logout();

    /**
     * Create a user
     *
     * @param $input
     *
     * @return Response
     */

    public function register($input);

    /**
     * Delete a user
     *
     * @param $id
     *
     * @return Response
     */

    public function delete($id);


} 