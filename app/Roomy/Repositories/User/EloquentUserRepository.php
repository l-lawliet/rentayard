<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 23/07/14
 * Time: 6:01 PM
 */

namespace Roomy\Repositories\User;

use Roomy\Repositories\EloquentRepository;
use Roomy\Validators\User as UserValidators;
use User;
use Auth;
use Hash;
use Input;
use Mail;
use Activity;
use Applied_tenant;

class EloquentUserRepository extends EloquentRepository implements UserRepository{

    /**
     * @var User
     */

    private $model;

    function __construct(User $model)
    {
        $this->model = $model;
    }

    function getTen()
    {
        return $this->model->with('role')->paginate(10);
    }

    function register($input)
    {
        $v = new UserValidators\SignupValidator($input);
        if($v->passes())
        {
            $user = User::where('email',Input::get('email'))->first();
            if($user)
            {
                $this->errors = "User already exists";
                return false;
            }
            $user = new User;
            $user->first_name = Input::get('first_name');
            $user->last_name  = Input::get('last_name');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            if(Input::get('role')=='landlord')
            {
                $user->role_id = 1;
            }elseif(Input::get('role')=='renter')
            {
                $user->role_id = 2;
            }else
            {
                $user->role_id = 3;
            }

            $user->is_activated = 0;

            $user->activation_code = Hash::make(time().$user->email);

            $data = array('name' => Input::get('first_name'),'activationCode'=>urlencode($user->activation_code));

            Mail::queue('emails.activation',$data, function($message)
            {
                $message->to(Input::get('email'),Input::get('first_name')." ".Input::get('last_name'))->subject('Activation Mail');
            });

            $user->save();

            Applied_tenant::where('email',Input::get('email'))->update(array('user_id'=>$user->id));

            $activity   =   new Activity;
            $activity->user_id  =   $user->id;
            $activity->activity =   'joined_roomy';

            return $activity->save();
        }
        else
        {
            $this->errors = $v->getErrors()->toArray();
            return false;
        }

    }

    function delete($id)
    {
        if(User::find($id)->delete())
            return true;
        else
            return false;
    }

    function attemptLogin($email,$password)
    {
        if(Auth::attempt(array('email' => $email,'password' => $password)))
        {
            if(Auth::user()->is_activated){
                return true;
            }
            else{
                Auth::logout();
                $this->errors="Please Activate your account.";
                return false;
            }

        }
        else
        {
            $this->errors="Your email or password is wrong";
            return false;
        }
    }


    function forgotPassowrd($loginEmail)
    {
         if($loginEmail != ""){
            $users = User::where('email','=',$loginEmail)->first();
            if(isset($users))
            {
            
                $new_password = time();
                $new_password .= rand();
                $new_password = sha1($new_password);
                $new_password = substr($new_password,0,8);
                $users->password = Hash::make($new_password);
                $users->save();

               $data = array('name' => $users->first_name,'Forgot'=>$new_password);

            Mail::queue('emails.forgotPassword',$data, function($message)
            {
                $message->to(Input::get('login_email'))->subject('Roomy Forgot Passord');
            });


                $this->errors="password reseted successfully. Please check your inbox for new password";
                return true;
            }
            else{
             
                $this->errors="This email ID is not registered with us";
                return false;
            }

        }
        else
        {
            $this->errors="Your email ID is wrong";
            return false;
        }
    }

    function logout()
    {
        return Auth::logout();
    }

    function activate($code)
    {
        $user = User::where('activation_code',$code)->first();
        if($user)
        {
            $user->is_activated = '1';
            $user->activation_code = null;
            $user->save();
            return true;
        }
        else
        {
            $this->errors = "Invalid or Expired Activation Code";
            return false;
        }
    }


}