<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 09/08/14
 * Time: 1:33 PM
 */

namespace Roomy\Repositories\Renter;


/**
 * Interface RenterRepository
 * @package Roomy\Repositories\Renter
 */
interface RenterRepository {

    public function updateProfile($input);

    /**
     * @param $id Property Id
     * @param $input All Inputs
     * @return boolean
     */
    public function applyForProperty($id,$input);

    public function isEveryApplicantRegistered($id);

    public function acceptApplication($id);

    public function rejectApplication($id);

    public function find($id);

    public function findProperty($id);

    public function deleteHelp($id);

    public function paidDeposit($id);

    public function paidRent($id);
} 