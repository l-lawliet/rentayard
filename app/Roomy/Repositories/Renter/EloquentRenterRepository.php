<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 09/08/14
 * Time: 1:36 PM
 */

namespace Roomy\Repositories\Renter;

use Roomy\Repositories\EloquentRepository;
use User;
use Auth;
use Input;
use Application;
use Applied_tenant;
use Property;
use Activity;
use URL;
use Mail;
use Tenant;
use Deposit_transaction;
use Rent_transaction;
use Validator;
use Help;

class EloquentRenterRepository extends EloquentRepository implements RenterRepository {

    public function updateProfile($input)
    {
        $user = User::find(Auth::user()->id);
        $user->first_name   =   $input['first_name'];
        $user->last_name    =   $input['last_name'];
        $user->phone        =   $input['phone'];
        $user->salary       =   $input['salary'];
        $user->designation  =   $input['designation'];
        $user->about        =   $input['about'];
        $user->address      =   $input['address'];
        $user->paypal_email =   $input['paypal_email'];
        $user->pets         =   (isset($input['pets']))?implode('|',$input['pets']):'';

        $rental_history = [];

        if(isset($input['houseAddress']))
        {
            for($i=0;$i<count($input['houseAddress']);$i++)
            {
                $rental = [
                    'address'   =>  $input['houseAddress'][$i],
                    'rent'      =>  $input['houseRent'][$i],
                    'tenancyLength'    =>  $input['houseTenancyLength'][$i],
                    'landlordFirstName' =>  $input['houseLandlordFirstName'][$i],
                    'landlordLastName'  =>  $input['houseLandlordLastName'][$i],
                    'landlordEmail'     =>  $input['houseLandlordEmail'][$i],
                    'landlordPhone'     =>  $input['houseLandlordPhone'][$i]
                ];

                $rental_history[] = $rental;
            }

            $user->rental_history = json_encode($rental_history);
        }


        if(isset($input['workCompany']))
        {
            $work_history = [];

            for($i=0;$i<count($input['workCompany']);$i++)
            {
                $work = [
                    'company'   =>  $input['workCompany'][$i],
                    'designation'      =>  $input['workDesignation'][$i],
                    'employmentLength'    =>  $input['workEmploymentLength'][$i],
                    'salary' =>  $input['workSalary'][$i]
                ];

                $work_history[] = $work;
            }

            $user->work_history = json_encode($work_history);
        }


        if(isset($input['incomeSource']))
        {
            $additional_income = [];

            for($i=0;$i<count($input['incomeSource']);$i++)
            {
                $income = [
                    'source'   =>  $input['incomeSource'][$i],
                    'amount'      =>  $input['incomeAmount'][$i]
                ];

                $additional_income[] = $income;
            }

            $user->additional_income = json_encode($additional_income);
        }


        return $user->save();
    }

    /**
     * @param $id Property Id
     * @param $input All Inputs
     * @return boolean
     */
    public function applyForProperty($id, $input)
    {
        $application    =   new Application;
        $application->property_id   =   $id;
        $application->primary_tenant_id =   Auth::user()->id;
        $application->save();

        $rents_collected = 0;

        $property_url = URL::route('viewProperty',$id);

        $data = array('propertyUrl'=>urlencode($property_url));

        $property = Property::where('id',$id)->with('landlord')->first();

        $landlord = $property->landlord;

        Mail::queue('emails.renterApplicationRequest',$data, function($message) use($landlord)
        {
            $message->to($landlord->email,'')->subject('One Renter applied for your property');
        });

        $activity   =   new Activity;
        $activity->activity =   'applied';
        $activity->user_id  =   Auth::user()->id;
        $activity->application_id   =   $application->id;
        $activity->property_id = $id;
        $activity->save();


        $activity   =   new Activity;
        $activity->activity =   'applied';
        $activity->user_id  =   $property->user_id;
        $activity->application_id   =   $application->id;
        $activity->property_id = $id;
        $activity->save();


        if(isset($input['coapplicantEmail'])) {
            for ($i = 0; $i < count($input['coapplicantEmail']); $i++) {
                $tenant = new Applied_tenant;
                $tenant->application_id = $application->id;
                $tenant->email = $input['coapplicantEmail'][$i];
                $tenant->amount = $input['coapplicantAmountShare'][$i];


                $user = User::where('email', $tenant->email)->first();

                if (isset($user->id)) {
                    if($user->id == Auth::user()->id)
                    {
                        Log::info("testing");
                        $tenant->main_renter = Auth::user()->id;
                    }
                    $tenant->user_id = $user->id;
                    $activity = new Activity;
                    $activity->activity = 'friend_applied';
                    $activity->user_id = $user->id;
                    $activity->application_id = $application->id;
                    $activity->property_id = $id;
                    $activity->save();
                }
                else{
                    $data = array('propertyUrl'=>urlencode($property_url));

                    Mail::queue('emails.coapplicantInvite',$data, function($message) use($tenant)
                    {
                        $message->to($tenant->email,'')->subject('Your Friend invited you as co applicant');
                    });
                }

                $tenant->save();


                $rents_collected = $rents_collected + $tenant->amount;
            }
        }


        $property   =   Property::find($id);

        $tenant =   new Applied_tenant;
        $tenant->application_id =   $application->id;
        $tenant->email          =   Auth::user()->email;
        $tenant->amount         =   $property->rent - $rents_collected;
        $tenant->user_id        =   Auth::user()->id;
        $tenant->save();

        return true;
    }

    public function acceptApplication($id)
    {
        $application    =   Application::find($id);

        if($application->primary_tenant_id==Auth::user()->id)
        {
            $application->status    =   'accepted';
            $application->save();

            $application = Application::where('id',$id)->with('property')->first();

            $coapplicants   =   Applied_tenant::where('application_id',$id)->get();

            $property = Property::find($application->property->id);

            foreach($coapplicants as $coapplicant)
            {

                $activity   =   new Activity();
                $activity->user_id  =   $coapplicant->user_id;
                if($coapplicant->user_id==$application->primary_tenant_id)
                    $activity->activity =   'accepted';
                else
                    $activity->activity = 'friend_accepted';
                $activity->property_id = $property->id;
                $activity->application_id   =   $application->id;
                $activity->save();

                $tenant = new Tenant();

                $tenant->user_id = $coapplicant->user_id;
                $tenant->rent   =   $coapplicant->amount;

                if($coapplicant->user_id==$application->primary_tenant_id)
                {
                    $tenant->deposit    =   $property->deposit;
                    $tenant->depositPaid    =   0;
                }
                else
                {
                    $tenant->deposit    =   0;
                    $tenant->depositPaid    =   1;
                }
                $tenant->property_id    =   $application->property->id;

                $tenant->save();

                $property->showcase =0;
                $property->save();


            }



            $property->accept_application = 0;

            $property->save();

            $activity = new Activity();
            $activity->user_id  =   $application->property->landlord->id;
            $activity->activity =   'accepted';
            $activity->application_id   =   $application->id;
            $activity->save();

            return true;

        }

    }

    public function rejectApplication($id)
    {
        $application    =   Application::find($id);

        if($application->primary_tenant_id==Auth::user()->id)
        {
            $application->status    =   'rejected';
            $application->save();

            $coapplicants   =   Applied_tenant::where('application_id',$id)->get();

            foreach($coapplicants as $coapplicant)
            {

                $activity   =   new Activity();
                $activity->user_id  =   $coapplicant->user_id;
                if($coapplicant->user_id==$application->primary_tenant_id)
                    $activity->activity =   'rejected';
                else
                    $activity->activity = 'friend_rejected';
                $activity->application_id   =   $application->id;
                $activity->save();

            }

            $activity = new Activity();
            $activity->user_id  =   $application->property->landlord->id;
            $activity->activity =   'rejected';
            $activity->application_id   =   $application->id;
            $activity->save();


            return true;

        }

    }

    public function find($id){

        $user = User::find($id);

        $user->pets = explode('|',$user->pets);
        if($user->rental_history!=null){
            $user->rental_history = json_decode($user->rental_history);
        }
        $user->work_history   = json_decode($user->work_history);
        $user->additional_income   = json_decode($user->additional_income);

        return $user;
    }

      public function deleteHelp($id)
    {
       return Help::find($id)->delete();
    }
    
    public function updateDp($input){

        $user = Auth::user();
        $validator = Validator::make(
        array('profile_picture' => Input::file('profile_picture')),
        array('profile_picture' => 'required|mimes:jpeg,jpg,bmp,png'));
        if ($validator->fails()) 
        {
            $this->errors = $validator->messages();
            return false;
        }
        else
        {
            if(Input::hasFile('profile_picture'))
            {
                $image = Input::file('profile_picture');
                $filename = strtotime('now').$image->getClientOriginalName();
                $image->move(public_path('uploads'),$filename);
                $user->profile_picture = $filename;
                return $user->save();
            }
            else
            {
                return false;
            }
        }
    }

    public function isEveryApplicantRegistered($id)
    {
        $application    =   Applied_tenant::where('application_id',$id)->where('user_id',0)->get();

        if(!$application->count())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function findProperty($id)
    {
        return Property::find($id);
    }

    public function paidDeposit($id)
    {

        // Mark as paid deposit

        $tenant = Tenant::where('user_id', Auth::user()->id)->where('property_id', $id)->first();

        if ($tenant->depositPaid)
        {
            return false;
        }
        $tenant->depositPaid = 1;

        $tenant->save();

        // Activity Registration for renter

        $application_id= Application::where('property_id', $id)->where('primary_tenant_id',Auth::user()->id)->first();
        if($application_id->id == null){
            $application_id=  Activity::where('property_id', $id)->where('user_id',Auth::user()->id)->first();
        }
        $activity   =   new Activity;
        $activity->activity =   'paid_deposit';
        $activity->user_id  =   Auth::user()->id;
        $activity->application_id  =   $application_id->id;
        $activity->property_id   =   $tenant->property_id;
        $activity->save();

        // Activity Registration for landlord

        $activity   =   new Activity;
        $activity->activity =   'paid_deposit';
        $activity->user_id  =   Property::find($id)->landlord->id;
        $activity->application_id  =   $application_id->id;
        $activity->property_id   =   $tenant->property_id;
        $activity->save();

        // Store Transaction

        $deposit = new Deposit_transaction;
        $deposit->user_id   =   Auth::user()->id;
        $deposit->property_id   =   $tenant->property_id;
        $deposit->amount    =   round(Input::get('payment_gross'));
        $deposit->status    =   'paid';
        $deposit->mode      =   'paypal';
        return $deposit->save();
    }

    public function paidRent($id)
    {
        // Mark as paid deposit

        $tenant = Tenant::where('user_id', Auth::user()->id)->where('property_id', $id)->first();

        if ($tenant->rentPaid)
        {
            return false;
        }

        $tenant->rentPaid = 1;

        $tenant->save();

        $application_id= Application::where('property_id',$id)->where('primary_tenant_id',Auth::user()->id)->first();
        
        // Activity Registration for renter
        if($application_id == null){
            $application_id=  Activity::where('property_id', $id)->where('user_id',Auth::user()->id)->where('activity','friend_applied')->first();
            $application_applied_id=$application_id->application_id;
        }
        else
        {
            $application_applied_id=$application_id->id;
        }

        $activity   =   new Activity;
        $activity->activity =   'paid_rent';
        $activity->user_id  =   Auth::user()->id;
        $activity->application_id  =   $application_applied_id;
        $activity->property_id   =   $tenant->property_id;
        $activity->save();

        // Activity Registration for landlord

        $activity   =   new Activity;
        $activity->activity =   'paid_rent';
        $activity->user_id  =   Property::find($id)->landlord->id;
        $activity->application_id  =   $application_applied_id;
        $activity->property_id   =   $tenant->property_id;
        $activity->save();

        // Store Transaction

        $deposit = new Rent_transaction;
        $deposit->user_id   =   Auth::user()->id;
        $deposit->property_id   =   $tenant->property_id;
        $deposit->amount    =   round(Input::get('payment_gross'));
        $deposit->mode      =   'paypal';
        return $deposit->save();
    }


} 