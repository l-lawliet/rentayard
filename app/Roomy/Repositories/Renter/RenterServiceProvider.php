<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 29/07/14
 * Time: 1:03 PM
 */

namespace Roomy\Repositories\Renter;

use Illuminate\Support\ServiceProvider;

class RenterServiceProvider extends ServiceProvider{

    public function register(){
        $this->app->bind(
            'Roomy\Repositories\Renter\RenterRepository',
            'Roomy\Repositories\Renter\EloquentRenterRepository'
        );
    }

} 