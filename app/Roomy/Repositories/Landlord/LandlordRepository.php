<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 29/07/14
 * Time: 1:02 PM
 */

namespace Roomy\Repositories\Landlord;


/**
 * Interface LandlordRepository
 * @package Roomy\Repositories\Landlord
 */
interface LandlordRepository {


    /**
     * Update Profile
     */

    public function updateProfile($input);

    /**
     * Update Dp
     *
     * @param   Input
     *
     * @return  boolean
     */

    public function updateDp($input);

    /**
     * Add Property
     *
     * @param   Input
     *
     * @return  boolean
     */

    public function addPropertyRent($input);

    public function addPropertySale($input);

    /**
     * Get Property
     *
     * @param  $id Integer
     *
     * @return  Property
     */

     public function findProperty($id);

    /**
     * Update a Property using Id
     *
     * @param $id Integer
     * @param $input Input
     *
     * @return Boolean
     */

    public function updateProperty($id,$input);
    public function updatePropertySale($id,$input);


    /**
     * @param $id Integer
     * @return Boolean
     */

    public function deletePropertyPhoto($id);


    /**
     * @param $id Integer
     * @return Boolean
     */

    public function deleteProperty($id);

    public function deleteHelp($id);


    public function deleteRequest($id);


    /**
     * @param $id Integer
     * @return Boolean
     */

    public function uploadPropertyPhoto($id);
}