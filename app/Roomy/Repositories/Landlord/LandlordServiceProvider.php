<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 29/07/14
 * Time: 1:03 PM
 */

namespace Roomy\Repositories\Landlord;

use Illuminate\Support\ServiceProvider;

class LandlordServiceProvider extends ServiceProvider{

    public function register(){
        $this->app->bind(
            'Roomy\Repositories\Landlord\LandlordRepository',
            'Roomy\Repositories\Landlord\EloquentLandlordRepository'
        );
    }

} 