<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 29/07/14
 * Time: 1:02 PM
 */

namespace Roomy\Repositories\Landlord;

use Roomy\Repositories\EloquentRepository;
use Roomy\Validators\Landlord as LandlordValidators;
use Auth;
use User;
use Input;
use Property;
use Property_photo;
use Validator;
use SendRequest;
use Help;
use Activity;
use Savesearch;
class EloquentLandlordRepository extends EloquentRepository implements LandlordRepository {

    public function updateProfile($input){
        $user = Auth::user();

        $user->first_name   =   $input['first_name'];
        $user->last_name    =   $input['last_name'];
        $user->paypal_email =   $input['paypal_email'];

        return $user->save();
    }

    public function updateDp($input){

        $user = Auth::user();
        $validator = Validator::make(
        array('profile_picture' => Input::file('profile_picture')),
        array('profile_picture' => 'required|mimes:jpeg,jpg,bmp,png'));
        if ($validator->fails()) 
        {
            $this->errors = $validator->messages();
            return false;
        }
        else
        {
            if(Input::hasFile('profile_picture'))
            {
                $image = Input::file('profile_picture');
                $filename = strtotime('now').$image->getClientOriginalName();
                $image->move(public_path('uploads'), $filename);
                $user->profile_picture = $filename;
                return $user->save();
            }
            else
            {
                return false;
            }
        }
    }

    public function addPropertyRent($input){
        $v = new LandlordValidators\AddPropertyValidator($input);

        if($v->passes())
        {
            $addres = $input['address'] . $input['zip'];

            $property = new Property;
            $property->name = $input['name'];
            $property->address = $addres;
            $property->street_num = $input['street_num'];
            $property->street_name = $input['street_name'];
            $property->latitude = $input['lat'];
            $property->longitude = $input['lon'];
            $property->city = $input['city'];
            $property->state= $input['state'];
            $property->zip  = $input['zip'];
            $property->rent = $input['rent'];
            $property->deposit  =   $input['deposit'];
            $property->bedrooms =   $input['bedrooms'];
            $property->bathrooms=   $input['bathrooms'];
            $property->sqft    =   $input['sqft'];
            $property->rent_start_date  =   $input['rentStartDate'];
            $property->parking  =   $input['parking'];
            $property->user_id  =   Auth::user()->id;
            $property->choice = 0;
            $property->des = $input['des'];

            if(isset($input['acceptApplication']))
            {
                $property->accept_application = 1;
            }
            else
            {
                $property->accept_application = 0;
            }

            if(isset($input['showcase']))
            {
                $property->showcase = 1;
            }
            else
            {
                $property->showcase = 0;
            }

            $property->save();

            if(Input::hasFile('photos'))
            {

                $files = Input::file('photos');
              

                    $filename = strtotime('now').$files->getClientOriginalName();
                    $files->move(public_path('uploads/property_photos/'),$filename);
                    $photo = new Property_photo;
                    $photo->property_id = $property->id;
                    $photo->filename = $filename;
                    $photo->save();

                // //return $files;
                //     var_dump($files->getClientOriginalName());


                // foreach($files as $file) {

                //     var_dump($file->getClientOriginalName());

                //     $filename = strtotime('now').$file->getClientOriginalName();
                //     $file->move('uploads/property_photos/',$filename);
                //     $photo = new Property_photo;
                //     $photo->property_id = $property->id;
                //     $photo->filename = $filename;
                //     $photo->save();
                // }
            }

            return true;
        }
        else
        {
            $this->errors = $v->getErrors()->toArray();
            return false;
        }
    }

        public function addPropertySale($input){
        $v = new LandlordValidators\AddPropertyValidatorSale($input);

        if($v->passes())
        {
            $addres = $input['address'] . $input['zip'];

            $property = new Property;
            $property->name = $input['name'];
            $property->address = $addres;
            $property->street_num = $input['street_num'];
            $property->street_name = $input['street_name'];
            $property->latitude = $input['lat'];
            $property->longitude = $input['lon'];
            $property->city = $input['city'];
            $property->state= $input['state'];
            $property->zip  = $input['zip'];
            $property->price = $input['price'];
            $property->bedrooms =   $input['bedrooms'];
            $property->bathrooms=   $input['bathrooms'];
            $property->sqft    =   $input['sqft'];
            $property->parking  =   $input['parking'];
            $property->user_id  =   Auth::user()->id;
            $property->choice = 1;
            $property->des = $input['des'];
            $property->hschool = $input['hschool'];
            $property->mschool = $input['mschool'];
            $property->eschool = $input['eschool'];
            $property->hospital = $input['hospital'];
            $property->restaurants = $input['restaurants'];
            $property->groceries = $input['groceries'];
            $property->banks = $input['banks'];
            $property->gas = $input['gasStation'];
            $property->bus = $input['bus'];
            $property->rails = $input['rails'];
            $property->showcase = 1;

            $property->save();

            if(Input::hasFile('photos'))
            {
                $files = Input::file('photos');
                 $filename = strtotime('now').$files->getClientOriginalName();
                    $files->move('uploads/property_photos/',$filename);
                    $photo = new Property_photo;
                    $photo->property_id = $property->id;
                    $photo->filename = $filename;
                    $photo->save();

                // foreach($files as $file) {
                //     $filename = strtotime('now').$file->getClientOriginalName();
                //     $file->move('uploads/property_photos/',$filename);
                //     $photo = new Property_photo;
                //     $photo->property_id = $property->id;
                //     $photo->filename = $filename;
                //     $photo->save();
                // }
            }

            return true;
        }
        else
        {
            $this->errors = $v->getErrors()->toArray();
            return false;
        }
    }

    public function findProperty($id)
    {
        return Property::where('id',$id)->with('photos','landlord')->first();
    }

    public function updateProperty($id,$input){
        $v = new LandlordValidators\AddPropertyValidator($input);

        if($v->passes())
        {

            $property = Property::find($id);
            $property->name = $input['name'];
            $property->city = $input['city'];
            $property->state= $input['state'];
            $property->zip  = $input['zip'];
            $property->rent = $input['rent'];
            $property->deposit  =   $input['deposit'];
            $property->bedrooms =   $input['bedrooms'];
            $property->bathrooms=   $input['bathrooms'];
            $property->sqft    =   $input['sqft'];
            $property->rent_start_date  =   $input['rentStartDate'];
            $property->parking  =   $input['parking'];
            $property->des = $input['des'];
            $property->user_id  =   Auth::user()->id;

            if(isset($input['acceptApplication']))
            {
                $property->accept_application = 1;
            }
            else
            {
                $property->accept_application = 0;
            }

            $property->save();



            return true;
        }
        else
        {
            $this->errors = $v->getErrors()->toArray();
            return false;
        }
    }

        public function updatePropertySale($id,$input){
        $v = new LandlordValidators\AddPropertyValidatorSale($input);

        if($v->passes())
        {

            $property = Property::find($id);
            $property->name = $input['name'];
            $property->city = $input['city'];
            $property->state= $input['state'];
            $property->zip  = $input['zip'];
            $property->price = $input['price'];
            $property->bedrooms =   $input['bedrooms'];
            $property->bathrooms=   $input['bathrooms'];
            $property->sqft    =   $input['sqft'];
            $property->des = $input['des'];
            $property->parking  =   $input['parking'];
            $property->user_id  =   Auth::user()->id;


            $property->save();



            return true;
        }
        else
        {
            $this->errors = $v->getErrors()->toArray();
            return false;
        }
    }


    public function deletePropertyPhoto($id)
    {
        return Property_photo::where('id',$id)->delete();
    }

    public function deleteRequest($id)
    {
       return SendRequest::find($id)->delete();
    }

      public function deleteHelp($id)
    {
       return Help::find($id)->delete();
    }

    public function deleteProperty($id)
    {
 
        // Activity::where('property_id',$id)->delete();

        // SendRequest::where('property_id',$id)->delete();

        // Savesearch::where('property_id',$id)->delete();

        // Application::where('property_id',$id)->delete();
        
        return Property::find($id)->delete();
    }

    public function uploadPropertyPhoto($id)
    {
        $validator = Validator::make(
                array('photos' => Input::file('photos')),
                array('photos' => 'required|mimes:jpeg,jpg,bmp,png'));
        if ($validator->fails()) 
        {
            $this->errors = $validator->messages();
            return false;
        }
        else
        {
            if(Input::hasFile('photos'))
            {

                $files = Input::file('photos');

                if(is_array($files))
                {
                    foreach($files as $file) {
                        $filename = strtotime('now').$file->getClientOriginalName();
                        $file->move(public_path('uploads/property_photos/'),$filename);
                        $photo = new Property_photo;
                        $photo->property_id = $id;
                        $photo->filename = $filename;
                        $photo->save();
                    }
                }
                else
                {

                    $filename = strtotime('now').$files->getClientOriginalName();
                    $files->move(public_path('uploads/property_photos/'),$filename);
                    $photo = new Property_photo;
                    $photo->property_id = $id;
                    $photo->filename = $filename;
                    $photo->save();
                }


            }
        }


        return true;
    }


} 