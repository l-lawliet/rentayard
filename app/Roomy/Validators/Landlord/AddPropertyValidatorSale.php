<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 03/01/15
 * Time: 12:10 PM
 */

namespace Roomy\Validators\Landlord;


class AddPropertyValidatorSale extends \Roomy\Validators\Validator {

    /**
     * Validation rules
     */
    public static $rules = array(
        'name'    =>  'required',
        'city'    =>  'required',
        'state'   =>  'required',
        'zip'     =>  'required|integer',
        'price'    =>  'required|integer',
        'bedrooms' =>  'required|integer',
        'bathrooms' =>  'required|integer',
        'sqft'      =>  'required|integer',
        'parking'   =>  'required|integer'
    );

    /**
     * Custom Attribute Names
     */
    public static $customAttributeNames = array (
    );
} 
