<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 24/07/14
 * Time: 1:57 PM
 */

namespace Roomy\Validators\Landlord;


class AddPropertyValidator extends \Roomy\Validators\Validator {

    /**
     * Validation rules
     */
    public static $rules = array(
        'name'    =>  'required',
        'city'    =>  'required',
        'state'   =>  'required',
        'zip'     =>  'required|integer',
        'rent'    =>  'required|integer',
        'deposit' =>  'required|integer',
        'bedrooms' =>  'required|integer',
        'bathrooms' =>  'required|integer',
        'sqft'      =>  'required|integer',
        'parking'   =>  'required|integer',
        'rentStartDate' => 'required|integer'
    );

    /**
     * Custom Attribute Names
     */
    public static $customAttributeNames = array (
    );
} 
