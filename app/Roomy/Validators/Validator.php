<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 24/07/14
 * Time: 1:55 PM
 */

namespace Roomy\Validators;


class Validator {

    protected $input;

    protected $errors;

    public function __construct($input = NULL)
    {
        $this->input = $input ?: \Input::all();
    }

    public function passes()
    {
        $validation = \Validator::make($this->input, static::$rules);
        $validation->setAttributeNames(static::$customAttributeNames);

        if($validation->passes()) return true;

        $this->errors = $validation->messages();

        return false;
    }

    public function getErrors()
    {
        return $this->errors;
    }

} 