<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 24/07/14
 * Time: 1:57 PM
 */

namespace Roomy\Validators\User;


class SignupValidator extends \Roomy\Validators\Validator {

    /**
     * Validation rules
     */
    public static $rules = array(
        'first_name'    =>  'required',
        'last_name'     =>  'required',
        'email'         =>  'required|email',
        'password'      =>  'required',
        'role'          =>  'required'
    );

    /**
     * Custom Attribute Names
     */
    public static $customAttributeNames = array (
        'first_name'    =>  'First Name',
        'last_name'     =>  'Last Name',
        'email'         => 'Email',
        'password'      => 'Password'
    );
} 