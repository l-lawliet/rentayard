<?php

User::setStripeKey(Config::get('app.stripeSKey'));

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('testfoo', function(){

$array = array( "foo" => "Testing",
"bar"=> "foo",
);
return json_encode($array);});

Route::get('/signup',array('as'=>'signup','uses'=>'UserController@signup'));

Route::get('/',array('as'=>'home','uses'=>'UserController@home'));

Route::post('/signup/process',array('as'=>'processSignup','uses'=>'UserController@processSignup'));

Route::get('/activate/{code}',array('as'=>'activate','uses'=>'UserController@activate'));

Route::get('/login', array('as'=>'login','uses'=>'UserController@login'));

Route::get('/login/forgotPassword',array('as'=>'forgotPassword','uses'=>'UserController@forgotPassword'));

Route::post('/login/process',array('as'=>'processLogin','uses'=>'UserController@processLogin'));

Route::post('/login/forgotPassowrd/process',array('as'=>'forgotPasswordprocess','uses'=>'UserController@forgotPasswordprocess'));

Route::get('/logout',array('as'=>'logout','uses'=>'UserController@logout'));

Route::get('/david', function()
{
    return 'Hello World';
});

// cron

Route::get('/server/sendMail', 'HomeController@sendMail');

Route::get('/server/vacate', 'HomeController@vacate');


/*
 * Landlord
 */

Route::get('/landlord',array('as'=>'landlordDashboard','uses'=>'LandlordController@viewDashboard'));

Route::get('/landlord/profile/edit',array('as'=>'landlordEditProfile','uses'=>'LandlordController@editProfile'));

Route::post('/landlord/profile/update',array('as'=>'landlordUpdateProfile','uses'=>'LandlordController@updateProfile'));

Route::post('/landlord/profile_picture/update',array('as'=>'landlordUpdateDp','uses'=>'LandlordController@updateDp'));

Route::get('/landlord/property/add', array('as' => 'addProperty', 'uses' => 'LandlordController@addProperty'));

Route::get('/landlord/property/addRent',array('as'=>'addPropertyRent','uses'=>'LandlordController@addPropertyRent'));

Route::get('/landlord/property/propertiesRentRequest',array('as'=>'PropertyRentRequest','uses'=>'LandlordController@PropertyRentRequest'));

Route::get('/landlord/property/propertiesSaleRequest',array('as'=>'PropertySaleRequest','uses'=>'LandlordController@PropertySaleRequest'));

Route::get('/landlord/property/{id}/propertiesRentRequestBoard',array('as'=>'PropertyRentRequestBoard','uses'=>'LandlordController@PropertyRentRequestBoard'));

Route::get('/landlord/property/{id}/propertiesSaleRequestBoard',array('as'=>'PropertySaleRequestBoard','uses'=>'LandlordController@PropertySaleRequestBoard'));

Route::get('/landlord/property/{id}/deleteSendRequest',array('as'=>'landlordDeletePropertySendRequest','uses'=>'LandlordController@deletePropertySendReqest'));

Route::post('/landlord/property/addRent',array('as'=>'addPropertyRent','uses'=>'LandlordController@addPropertyRentProcess'));

Route::get('/landlord/property/addSale',array('as'=>'addPropertySale','uses'=>'LandlordController@addPropertySale'));

Route::post('/landlord/property/addSale',array('as'=>'addPropertySaleProcess','uses'=>'LandlordController@addPropertySaleProcess'));

Route::get('/landlord/property/{id}/view',array('as'=>'landlordViewProperty','uses'=>'LandlordController@viewProperty'));

Route::post('/landlord/property/{id}/update',array('as'=>'updateProperty','uses'=>'LandlordController@updateProperty'));

Route::get('/landlord/property/{id}/applications',array('as'=>'landlordPropertyApplications','uses'=>'LandlordController@viewPropertyApplications'));

Route::get('/landlord/property/{id}/photos',array('as'=>'landlordPropertyPhotos','uses'=>'LandlordController@viewPropertyPhotos'));

Route::get('/landlord/property/photo/{id}/delete',array('as'=>'landlordPropertyPhotoDelete','uses'=>'LandlordController@deletePropertyPhoto'));

Route::post('/landlord/property/{id}/photo/upload',array('as'=>'uploadPropertyPhoto','uses'=>'LandlordController@uploadPropertyPhoto'));

Route::get('/landlord/property/{id}/rentPayments',array('as'=>'landlordRentPayments','uses'=>'LandlordController@viewRentPayments'));

Route::get('/landlord/property/application/{id}/approve',array('as'=>'approveApplication','uses'=>'LandlordController@approveApplication'));

Route::get('/landlord/property/application/{id}/deny',array('as'=>'denyApplication','uses'=>'LandlordController@denyApplication'));

Route::get('/landlord/property/{id}/delete',array('as'=>'deleteProperty','uses'=>'LandlordController@deleteProperty'));

Route::get('/landlord/subscribe',array('as'=>'subscribe','uses'=>'LandlordController@subscribe'));

Route::post('/landlord/subscribe',array('as'=>'subscribe','uses'=>'LandlordController@subscribed'));

Route::get('/landlord/property/{id}/delete',array('as'=>'landlordDeleteProperty','uses'=>'LandlordController@deleteProperty'));

Route::get('/landlord/landlordHelp',array('as'=>'landlordHelp','uses'=>'LandlordController@landlordHelp'));

Route::get('/landlord/vacate',array('as'=>'landlordVacate','uses'=>'LandlordController@landlordVacate'));

Route::get('/landlord/vacate/approve/{id}/',array('as'=>'landlordVacateApprove','uses'=>'LandlordController@landlordVacateApprove'));

Route::get('/landlord/vacate/decline/{id}',array('as'=>'landlordVacateDecline','uses'=>'LandlordController@landlordVacateDecline'));

Route::post('/landlord/landlordHelp',array('as'=>'test','uses'=>'LandlordController@createlandlordHelp'));

Route::get('/landlord/property/deletelandlordhelp/{id}',array('as'=>'landlordDeleteHelp','uses'=>'LandlordController@deletelandlordHelp'));



/*
 * Renter
 */

Route::get('/renter',array('as'=>'renterDashboard','uses'=>'RenterController@viewDashboard'));

Route::get('/renter/profile/edit',array('as'=>'renterEditProfile','uses'=>'RenterController@editProfile'));

Route::get('/renter/properties',array('as'=>'renterViewProperties','uses'=>'RenterController@viewProperties'));

Route::post('/renter/profile/update',array('as'=>'renterUpdateProfile','uses'=>'RenterController@updateProfile'));

Route::get('renter/applications',[
    'as'    =>  'renterApplications',
    'uses'  =>  'RenterController@applications'
]);

Route::post('/property/{id}/apply',array('as'=>'applyForProperty','uses'=>'RenterController@applyForProperty'));

Route::post('/renter/profile_picture/update',array('as'=>'renterUpdateDp','uses'=>'RenterController@updateDp'));

Route::get('/renter/property/application/{id}/accept',array('as'=>'acceptApplication','uses'=>'RenterController@acceptApplication'));

Route::get('/renter/property/application/{id}/reject',array('as'=>'rejectApplication','uses'=>'RenterController@rejectApplication'));

Route::get('/renter/property/{id}',array('as'=>'viewPropertyAsRenter','uses'=>'RenterController@viewProperty'));

Route::get('/renter/property/{id}/deposit/pay',array('as'=>'payDeposit','uses'=>'RenterController@payDeposit'));

Route::post('/renter/property/{id}/deposit/paid',array('as'=>'paidDeposit','uses'=>'RenterController@paidDeposit'));

Route::get('/renter/property/{id}/rent/pay',array('as'=>'payRent','uses'=>'RenterController@payRent'));

Route::post('/renter/property/{id}/rent/paid',array('as'=>'paidRent','uses'=>'RenterController@paidRent'));

Route::get('/renter/renterHelp',array('as'=>'renterHelp','uses'=>'RenterController@renterHelp'));


Route::post('/renter/renterHelp',array('as'=>'renterhelp','uses'=>'RenterController@createrenterHelp'));

Route::get('/renter/renter-vacate/{id}',array('as'=>'renterVacate','uses'=>'RenterController@renterVacate'));

Route::post('/renter/renterHelp',array('as'=>'test','uses'=>'RenterController@createrenterHelp'));


Route::get('/renter/property/deleterenterhelp/{id}',array('as'=>'renterDeleteHelp','uses'=>'RenterController@deleteRenterHelp'));







/*
 * Admin
 */

Route::get('/admin',array('as'=>'adminDashboard','uses'=>'AdminController@viewDashboard'));

Route::get('/admin/users',array('as'=>'userManagement','uses'=>'AdminController@userManagement'));

Route::get('/admin/addUser', array('as' => 'adminAddUser', 'uses' => 'AdminController@adminAddUser'));

Route::post('/admin/addUser', array('as' => 'adminAddUserProcess', 'uses' => 'AdminController@adminAddUserProcess'));

Route::get('/admin/user/{id}/delete',array('as'=>'deleteUser','uses'=>'AdminController@deleteUser'));

Route::get('/admin/user/{id}/edit',array('as'=>'editUser','uses'=>'AdminController@editUser'));

Route::post('/admin/user/{id}/edit',array('as'=>'editUser','uses'=>'AdminController@updateUser'));

Route::get('/admin/settings',array('as'=>'settings','uses'=>'AdminController@settings'));

Route::post('/admin/settings',array('as'=>'updateSettings','uses'=>'AdminController@updateSettings'));

Route::get('/admin/emailTemplates',array('as'=>'editEmailTemplates','uses'=>'AdminController@editEmailTemplates'));

Route::post('/admin/emailTemplates',array('as'=>'updateEmailTemplates','uses'=>'AdminController@updateEmailTemplates'));

Route::get('/admin/properties',array('as'=>'propertyManagement','uses'=>'AdminController@propertyManagement'));

Route::get('/admin/property/{id}/delete',array('as'=>'deleteProperty','uses'=>'AdminController@deleteProperty'));

Route::get('/admin/rentTransactions',array('as'=>'rentTransactions','uses'=>'AdminController@rentTransactions'));

Route::get('/admin/adminHelp',array('as'=>'adminHelp','uses'=>'AdminController@adminHelp'));

Route::get('/admin/help/resolved/{id}',array('as'=>'resolvedHelp','uses'=>'AdminController@resolvedHelp'));

Route::get('/admin/property/addRent/{id}',array('as'=>'adminAddPropertyRent','uses'=>'AdminController@adminAddPropertyRent'));

Route::post('/admin/property/addRentProcess/{id}',array('as'=>'adminAddPropertyRentProcess','uses'=>'AdminController@adminAddPropertyRentProcess'));

Route::get('/admin/property/addSale/{id}',array('as'=>'adminAddPropertySale','uses'=>'AdminController@adminAddPropertySale'));

Route::post('/admin/property/addSaleProcess/{id}',array('as'=>'adminAddPropertySaleProcess','uses'=>'AdminController@adminAddPropertySaleProcess'));

Route::get('/admin/property/{id}/view',array('as'=>'AdminViewProperty','uses'=>'AdminController@editPropertyrent'));


Route::post('/admin/property/{id}/update',array('as'=>'adminupdateProperty','uses'=>'AdminController@adminupdateProperty'));

 
Route::post('/install/process',array('as'=>'installationprocess','uses'=>'UserController@installationProcess'));


 
/*
 * Public Routes
 */
Route::get('/install', array('as' => 'install', 'uses' => 'UserController@install'));

Route::get('/buy', array('as' => 'buy', 'uses' => 'UserController@buy'));
Route::get('/help', array('as' => 'help', 'uses' => 'UserController@help'));


Route::get('/rent', array('as' => 'rent', 'uses' => 'UserController@rent'));

Route::get('/search/buy', array('as' => 'buySearch', 'uses' => 'UserController@buySearch'));

Route::get('/search/rent', array('as' => 'rentSearch', 'uses' => 'UserController@rentSearch'));

Route::get('/saveHome/{id}', array('as' => 'saveHome', 'uses' => 'UserController@saveHome'));

Route::get('/savedhome', array('as' => 'savedHome', 'uses' => 'UserController@savedHome'));

Route::get('/deleteSavedHome/{id}', array('as' => 'deleteSavedHome', 'uses' => 'UserController@deleteSavedHome'));

Route::get('/savedSearch', array('as' => 'savedSearch', 'uses' => 'UserController@savedSearch'));

Route::get('/userSearch/{key}', array('as' => 'userSearch', 'uses' => 'UserController@userSearch'));

Route::get('/deleteSearch/{key}', array('as' => 'deleteSearch', 'uses' => 'UserController@deleteSearch'));

Route::get('/saveSearch', array('as' => 'saveSearch', 'uses' => 'UserController@saveSearch'));

Route::get('/search', array('as' => 'search', 'uses' => 'UserController@search'));

Route::get('/filterSearch', array('as' => 'filterSearch', 'uses' => 'UserController@filterSearch'));

Route::get('/filterSearchBuy', array('as' => 'filterSearchBuy', 'uses' => 'UserController@filterSearchBuy'));

Route::get('/filterSearchRent', array('as' => 'filterSearchRent', 'uses' => 'UserController@filterSearchRent'));

Route::get('/ratings/{id}', array('as' => 'ratings', 'uses' => 'UserController@ratings'));

Route::post('/review/{id}', array('as' => 'review', 'uses' => 'UserController@review'));

Route::post('/property/{id}',array('as' => 'requestContact', 'uses' => 'LandlordController@requestContact'))->before('auth');

Route::get('/property/{id}',array('as'=>'viewProperty','uses'=>'LandlordController@viewPropertyAsPublic'));

Route::get('/renter/{id}',array('as'=>'viewRenter','uses'=>'RenterController@viewProfile'));

Route::post('/renter/changepassword',array('as'=>'Renterchangepassword','uses'=>'RenterController@renterchangepassword'));



