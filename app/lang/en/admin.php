<?php

return array(
    'dashboard' =>  'Dashboard',
    'userManagement'    =>  'User Management',
    'settings'          =>  'Settings',
    'sitename'          =>  'Sitename',
    'subscription_amount'   =>  'Subscription Amount',
    'footer'            =>  'Footer',
    'propertyManagement'    =>  'Property Management',
    'rentTransactions'   =>  'Rent Transactions',
    'editEmailTemplates'=>  'Edit Email Templates',
    'renterApplicationRequest'  =>  'Renter Application Request',
    'landlordApplicationConfirmation'   =>  'Landlord Application Confirmation',
    'coapplicantInvite'                 =>  'Co-Applicant Invite',
    'rentReminder'                      =>  'Rent Reminder',
    'activationMail'                    =>  'ActivationMail',
    'subscriptionReminderForLandlord'   =>  'Subscription Reminder for Landlord',
    'applicationRejection'              =>  'Application Rejection',
    'help'                              =>  'Help'
);