<?php

return array(

    'addProperty'   =>  'Add Property',
    'selectProperty' => 'Select Property',
    'neighborhood'   => 'Neighborhood Info',
    'homeLike'      => 'Homes you might like…',

    'middleSchool'  => 'Middle School',
    'highSchool'  => 'High School',
    'elementarySchool'  => 'Primary School',

    'hospital'  => 'Hospital',
    'restaurants' => 'Restaurants',
    'groceries' => 'Groceries',
    'banks' => 'Banks',
    'gasStation' => 'Gas Stations',
    'bus' => 'Bus Stop',
    'rails' => 'Railway Station',
    'name'          =>  'Name',
    'city'          =>  'City',
    'state'         =>  'State',
    'zip'           =>  'ZIP',
    'rent'          =>  'Rent',
    'amount'        =>  'Price',
    'deposit'       =>  'Deposit',
    'bedrooms'      =>  'Bedrooms',
    'bathrooms'     =>  'Bathrooms',
    'squareFootage'    =>  'Square Footage',
    'parking'       =>  'Parking',
    'petsAllowed'   =>  'Pets Allowed',
    'rentStartDate' =>  'Rent Start Date',
    'acceptApplications' =>  'Accept Applications ?',
    'showcase' => 'Are you want to Showcase this?',
    'applicationClosed'     =>  'Application Closed',
    'uploadPhotos'      =>  'Upload Photos',
    'cars'          =>  'Cars',
    'car'           =>  'Car',
    'updateProperty'    =>  'Update Property',
    'applyNow'      =>  'Apply Now',
    'RentProperty'   =>  'Rent Property',
    'SaleProperty'   =>  'Sale Property',
    'RentPropertyRequest'   =>  'Rent Property Request',
    'SalePropertyRequest'   =>  'Sale Property Request'

);
