<?php

return array(

    'dashboard' =>  'Dashboard',
    'addPropertyRent' => 'Add Property Rent',
    'addPropertySale' => 'Add Property Sale',
    'addProperty'   =>  'Add Property',
    'viewOrEdit'    =>  'View / Edit',
    'applications'  =>  'Applications',
    'rentPayments'  =>  'Rent Payments',
    'photos'        =>  'Photos',
    'upload'        =>  'Upload',
    'propertysPhotoGallery' =>  '\'s photo gallery',
    'viewAsPublic'  =>  'View As Public',
    'application'   =>  'Application',
    'approve'       =>  'Approve',
    'deny'          =>  'Deny',
    'approved'      =>  'Approved',
    'denied'        =>  'Denied',
    'accepted'      =>  'Accepted',
    'rejected'      =>  'Rejected',

    'neighborhood'      =>  'Landlord Neighborhood',
 
    'applicationAccepted'   =>  'accepted your application for',
    'applicationRejected'   =>  'rejected your application for',
    'renterapplied'   =>  'send you application for rent',
    'tenants'               =>  'Tenants',
    'tenantPaidRent'        =>  'One of your tenant paid rent for ',
    'tenantPaidDeposit'        =>  'One of your tenant paid deposit for ',
    'tenantsendrentRequest'        =>  'A user sent request for rent',
    'tenantsendsaleRequest'        =>  'A user sent request for sale',
    'deleteProperty'        =>  'Delete Property',
    'landlordhelp'        =>  'Landlord Help',
    'renterhelp'        =>  'Renter Help',
    'landlordvacate' => 'Vacation Notice'

);
