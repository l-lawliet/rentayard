<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 31/07/14
 * Time: 1:35 PM
 */

return  [
    'profileUpdated'    =>  'Your Profile has been updated successfully',
    'propertyAdded'     =>  'Your Property has been added successfully',
    'propertyUpdated'   =>  'Your Property has been updated successfully',
    'propertyDeleted'   =>  'Your Property has been deleted successfully',
    'photoDeleted'      =>  'Photo has been deleted successfully',
    'photoUploaded'     =>  'Photo has been uploaded successfully',
    'photoUploaded_error'   =>  'Photo Field Required',
    'profilePictureUpdated' =>  'Your Profile picture has been updated successfully',
    'appliedForProperty'    =>  'You have successfully applied for the property ',
    'applicationApproved'   =>  'Application has been approved',
    'applicationDenied'     =>  'Application has been denied',
    'applicationAccepted'   =>  'Application has been accepted',
    'userDeleted'       =>  'User deleted successfully',
    'settingsUpdated'   =>  'Settings updated successfully',
    'emailTemplatesUpdated' =>  'Email Templates Updated',
    'applicantsNotRegistered'   =>  'Some of the applicants are not registered. Your friends should signup to approve the request',
    'paidRent'          =>  'You paid the rent successfully',
    'paidDeposit'       =>  'You paid the deposit successfully',
    'error'             =>  'Some error occured :(',
    'landlordsHelp'     => 'Your Help has been deleted successfully',
    'propertyRequest'     => 'Your Request has been deleted successfully',
    'AdminHelp'     =>    'Help has been updated successfully'
];