<?php

use Roomy\Repositories\Landlord\LandlordRepository as LandlordRepo;

class LandlordController extends \BaseController {

    /**
     * @var Roomy\Repositories\Landlord
     */

    private $landlord;

    function __construct(LandlordRepo $landlord){
        $this->landlord   = $landlord;
        if(Auth::check())
        {
            $this->user = Auth::user();
            $this->properties = Property::where('user_id',$this->user->id)->with('photos')->get();
        }

        $this->beforeFilter('landlord',[
            'only'  =>  [
                'viewDashboard',
                'editProfile',
                'updateProfile',
                'updateDp',
                'addProperty',
                'viewProperty',
                'updateProperty',
                'viewPropertyPhotos',
                'deletePropertyPhoto',
                'uploadPropertyPhoto',
                'viewRentPayments',
                'addPropertyRent',
                'addPropertyRentProcess',
                'addPropertySale',
                'addPropertySaleProcess',
                'viewPropertyApplications',
                'approveApplication',
                'denyApplication'
            ]
        ]);


    }

    public function viewDashboard(){

        $activities = Activity::where('user_id',Auth::user()->id)->with('application')->with('property')->orderby('created_at','desc')->paginate('10');
         
        return View::Make('landlord.dashboard',array('currentPage'=>'dashboard'))->withUser($this->user)->withProperties($this->properties)->withActivities($activities);
    }


    public function editProfile(){
        return View::Make('landlord.editProfile',array('currentPage'=>'editProfile'))->withUser($this->user)->withProperties($this->properties);
    }

    public function updateProfile(){
        if($this->landlord->updateProfile(Input::all()))
        {
            return Redirect::Route('landlordEditProfile')->with('flash_success',trans('flash.profileUpdated'));
        }
        else
        {
            return Redirect::Route('landlordEditProfile')->with('flash_error',trans('flash.error'));
        }
    }

    public function updateDp(){
        if($this->landlord->updateDp(Input::all()))
        {
            return Redirect::Route('landlordEditProfile')->with('flash_success',trans('flash.profilePictureUpdated'));
        }
        else
        {
            return Redirect::Route('landlordEditProfile')->with('flash_errors',trans('flash.error'));
        }
    }

    public function addProperty()
    {
        return View::Make('landlord.addProperty',array('currentPage'=>'addProperty'))->withUser($this->user)->withProperties($this->properties);
    }

    public function addPropertyRent(){
            return View::Make('landlord.addPropertyRent',array('currentPage'=>'addProperty'))->withUser($this->user)->withProperties($this->properties);
    }

    public function PropertyRentRequest(){
            $propertiesRent = Property::with('photos','landlord')->where('user_id',Auth::user()->id)->where('choice',0)->orderBy('id', 'DESC')->paginate(9);
             
            return View::Make('landlord.propertiesRentRequest',array('currentPage'=>'RentProperty'))->withUser($this->user)->withProperties($this->properties)->withDemo($propertiesRent);
    }

    public function PropertySaleRequest(){
           $propertiesSale = Property::with('photos','landlord')->where('user_id',Auth::user()->id)->where('choice',1)->orderBy('id', 'DESC')->paginate(9);
             
            return View::Make('landlord.propertiesSaleRequest',array('currentPage'=>'SaleProperty'))->withUser($this->user)->withProperties($this->properties)->withDemo($propertiesSale);
    }

    public function PropertyRentRequestBoard($id){
         
            $propertiesRentRequest = SendRequest::where('property_id',$id)->where('choice',0)->orderBy('id', 'DESC')->get();
 
            return View::Make('landlord.propertiesRentRequestBoard',array('currentPage'=>'RentPropertyRequest'))->withUser($this->user)->withProperties($this->properties)->withDemo($propertiesRentRequest);
    }

     public function PropertySaleRequestBoard($id){
         
            $propertiesSaleRequest = SendRequest::where('property_id',$id)->where('choice',1)->orderBy('id', 'DESC')->get();
 
            return View::Make('landlord.propertiesSaleRequestBoard',array('currentPage'=>'SalePropertyRequest'))->withUser($this->user)->withProperties($this->properties)->withDemo($propertiesSaleRequest);
    }


    public function addPropertyRentProcess(){
            $location = Input::get('country');
            $country = strtolower($location);

        if ($country == 'jamaica' || $country == 'trinidad') {
            
                if($var = $this->landlord->addPropertyRent(Input::all()))
                {
                   // Auth::user()->subscription()->increment();
                    return Redirect::Route('landlordDashboard')->with('flash_success',trans('flash.propertyAdded'));
                }
                else
                {
                    return Redirect::Route('addProperty')->with('flash_errors',$this->landlord->errors);
                }
        }  else
                {
                    return Redirect::Route('addProperty')->with('flash_errors','House must be listed in Jamaica or trinidad');
                }

 
    }

    public function addPropertySale()
    {
       return View::Make('landlord.addPropertySale',array('currentPage'=>'addProperty'))->withUser($this->user)->withProperties($this->properties);
        //return View::make('landlord.add');
    }

    public function addPropertySaleProcess()
    {
         if($this->landlord->addPropertySale(Input::all()))
        {
            Auth::user()->subscription()->increment();
            return Redirect::Route('landlordDashboard')->with('flash_success',trans('flash.propertyAdded'));
        }
        else
        {
            return Redirect::Route('addProperty')->with('flash_errors',$this->landlord->errors);
        }
    }

    public function viewProperty($id){
        $property = $this->landlord->findProperty($id);
        if($property->choice == 0)
        {
            return View::make('landlord.viewProperty',array('currentPage'=>'viewProperty'.$id))->withUser($this->user)->withProperties($this->properties)->withProperty($property);
        }
        else
        {
            return View::make('landlord.viewPropertySale',array('currentPage'=>'viewProperty'.$id))->withUser($this->user)->withProperties($this->properties)->withProperty($property);
        }
    }

    public function updateProperty($id){
        $proty = Property::where('id',$id)->where('choice',0)->first();
        if($proty)
        {
            if($this->landlord->updateProperty($id,Input::all()))
            {
                return Redirect::Route('landlordViewProperty',array('id'=>$id))->with('flash_success',trans('flash.propertyUpdated'));
            }
            else
            {
                return Redirect::Route('landlordViewProperty',array('id'=>$id))->with('flash_errors',$this->landlord->errors);
            }
        }
        else
        {
            if($this->landlord->updatePropertySale($id,Input::all()))
            {
                return Redirect::Route('landlordViewProperty',array('id'=>$id))->with('flash_success',trans('flash.propertyUpdated'));
            }
            else
            {
                return Redirect::Route('landlordViewProperty',array('id'=>$id))->with('flash_errors',$this->landlord->errors);
            }
        }
    }

    public function viewPropertyApplications($id){
        $allapplications =Application::where('property_id',$id)->get();
        $applications=array();
        foreach($allapplications as $application)
        {
            $amount=Applied_tenant::where('application_id',$application->id)->sum('amount');
            $status=$application->status;
            $user=User::where('id',$application->primary_tenant_id)->first();   
            $applied_user=Applied_tenant::where('application_id',$application->id)->get()->toArray(); 
            //corenter
            $all=array();
            for($i=0;$i<count($applied_user);$i++) 
            {
             
                $userinfo=User::where('id',$applied_user[$i]['user_id'])->where('id','!=',$application->primary_tenant_id)->first();
            
                if($userinfo != Null)
                {
                array_push($all,array('first_name'=>$userinfo->first_name, 'last_name'=>$userinfo->last_name, 'email'=>$userinfo->email,'profile_picture'=>$userinfo->profile_picture,'id'=>$userinfo->id));
                }

            }
            
            array_push($applications,array('amount'=>$amount, 'status'=>$status, 'first_name'=>$user->first_name, 'last_name'=>$user->last_name, 'email'=>$user->email,'profile_picture'=>$user->profile_picture,'id'=>$user->id,'application_id'=>$application->id,'corenter'=>$all));
            
        }
        return View::Make('landlord.viewPropertyApplications',array('currentPage'=>'propertyApplications'.$id))->withUser($this->user)->withProperties($this->properties)->withApplications($applications);
    }

    public function viewPropertyPhotos($id){
        $property = $this->landlord->findProperty($id);
        return View::make('landlord.viewPropertyPhotos',array('currentPage'=>'viewPropertyPhotos'.$id))->withUser($this->user)->withProperties($this->properties)->withProperty($property);
    }

    public function deletePropertyPhoto($id){
        if($this->landlord->deletePropertyPhoto($id))
        {
            return Redirect::back()->with('flash_success',trans('flash.photoDeleted'));
        }
        else
        {
            return Redirect::Route('landlordPropertyPhotos',array('id'=>$id))->with('flash_errors',$this->landlord->errors);
        }
    }

    public function uploadPropertyPhoto($id)
    {
 if($this->landlord->uploadPropertyPhoto($id))
        {
            return Redirect::Route('landlordPropertyPhotos',array('id'=>$id))->with('flash_success',trans('flash.photoUploaded'));
        }
        else
        {
            return Redirect::Route('landlordPropertyPhotos',array('id'=>$id))->with('flash_errors',trans('flash.photoUploaded_error'));
        }
    }

    public function viewRentPayments($id)
    {

        $property   =   $this->landlord->findProperty($id);
        $application = Application::where('property_id',$id)->where('status','accepted')->first();

        if($application == Null)
        {
            $tenants = Tenant::where('property_id',$id)->with('user')->get();
            $deposit = Deposit_transaction::where('property_id',$id)->with('user')->first();
            $rents   =   Rent_transaction::where('property_id',$id)->orderBy('created_at','desc')->with('user')->get();
            return View::Make('landlord.viewRentPayments',array('currentPage'=>'propertyRents'.$id))->withUser($this->user)->withProperties($this->properties)->withProperty($property)->withTenants($tenants)->withDeposit($deposit)->withRents($rents);

        }else
        {
            $maintenants=Tenant::where('user_id',$application->primary_tenant_id)->where('property_id',$id)->first();
            $maintenants->rent=$property->rent;
            $maintenants->save();

            $tenants = Tenant::where('property_id',$id)->where('user_id',$application->primary_tenant_id)->with('user')->get();

            $deposit = Deposit_transaction::where('property_id',$id)->with('user')->first();

            $rents   =   Rent_transaction::where('property_id',$id)->orderBy('created_at','desc')->with('user')->get();

            return View::Make('landlord.viewRentPayments',array('currentPage'=>'propertyRents'.$id))->withUser($this->user)->withProperties($this->properties)->withProperty($property)->withTenants($tenants)->withDeposit($deposit)->withRents($rents); 
        }
 
    }

    public function viewPropertyAsPublic($id)
    {
        $property = Property::where('id',$id)->with('photos','landlord')->first();
        $homeLike = Property::where('city',$property->city)->with('photos','landlord')->get();
        $userdel = User::where('id',$property->user_id)->first();
        $ratings = Rating::where('property_id',$id)->paginate(4);
        $safety = Rating::where('property_id',$id)->avg('safety');
        $pet_friendly = Rating::where('property_id',$id)->avg('pet_friendly');
        $walkability = Rating::where('property_id',$id)->avg('walkability');
        $restaurants = Rating::where('property_id',$id)->avg('restaurants');
        $parks = Rating::where('property_id',$id)->avg('parks');
        $schools = Rating::where('property_id',$id)->avg('schools');
        $traffic = Rating::where('property_id',$id)->avg('traffic');
        $parking = Rating::where('property_id',$id)->avg('parking');
        $entertainment = Rating::where('property_id',$id)->avg('entertainment');
        $transportation = Rating::where('property_id',$id)->avg('transportation');
        $community = Rating::where('property_id',$id)->avg('community');
        $cleanliness = Rating::where('property_id',$id)->avg('cleanliness');
        $overall = Rating::where('property_id',$id)->avg('overall');
        //dd($safety);
        //$savesearchuser = Savesearch::where('user_id',Auth::user()->id)->where('property_id',$id)->first();
       // dd($savesearch);
        if($property->choice == 0)
        {
            if(Auth::check())
            {
                $savesearch = Savesearch::where('property_id',$id)->where('user_id',Auth::user()->id)->first();
                $user   =   Auth::user();
                if($user->role_id==2)
                {
                    $user->pets = explode('|',$user->pets);
                    $user->rental_history = json_decode($user->rental_history);
                    $user->work_history   = json_decode($user->work_history);
                    $user->additional_income   = json_decode($user->additional_income);

                    $application = Application::where('primary_tenant_id',$user->id)->where('property_id',$property->id)->first();


                    return View::Make('userviewProperty')->withProperty($property)->withUser($user)->withApplication($application)->with('userdel',$userdel)->with('savesearch',$savesearch)->with('ratings',$ratings)
                                                    ->with('safety',$safety)
                                                    ->with('pet_friendly',$pet_friendly)
                                                    ->with('walkability',$walkability)
                                                    ->with('restaurants',$restaurants)
                                                    ->with('parks',$parks)
                                                    ->with('schools',$schools)
                                                    ->with('traffic',$traffic)
                                                    ->with('parking',$parking)
                                                    ->with('entertainment',$entertainment)
                                                    ->with('transportation',$transportation)
                                                    ->with('community',$community)
                                                    ->with('cleanliness',$cleanliness)
                                                    ->with('overall',$overall)
                                                    ->with('homeLike',$homeLike);
                }
                return View::Make('userviewProperty')->withProperty($property)->with('userdel',$userdel)->with('savesearch',$savesearch)->with('ratings',$ratings)
                                                    ->with('safety',$safety)
                                                    ->with('pet_friendly',$pet_friendly)
                                                    ->with('walkability',$walkability)
                                                    ->with('restaurants',$restaurants)
                                                    ->with('parks',$parks)
                                                    ->with('schools',$schools)
                                                    ->with('traffic',$traffic)
                                                    ->with('parking',$parking)
                                                    ->with('entertainment',$entertainment)
                                                    ->with('transportation',$transportation)
                                                    ->with('community',$community)
                                                    ->with('cleanliness',$cleanliness)
                                                    ->with('overall',$overall)
                                                    ->with('homeLike',$homeLike);
            }
            else
            {
                
                return View::Make('viewProperty')->withProperty($property)->with('userdel',$userdel)->with('ratings',$ratings)
                                                    ->with('safety',$safety)
                                                    ->with('pet_friendly',$pet_friendly)
                                                    ->with('walkability',$walkability)
                                                    ->with('restaurants',$restaurants)
                                                    ->with('parks',$parks)
                                                    ->with('schools',$schools)
                                                    ->with('traffic',$traffic)
                                                    ->with('parking',$parking)
                                                    ->with('entertainment',$entertainment)
                                                    ->with('transportation',$transportation)
                                                    ->with('community',$community)
                                                    ->with('cleanliness',$cleanliness)
                                                    ->with('overall',$overall)
                                                    ->with('homeLike',$homeLike);
            }
        }
        else
        {
            if(Auth::check())
            {
                $savesearch = Savesearch::where('property_id',$id)->where('user_id',Auth::user()->id)->first();
                $user   =   Auth::user();
                if($user->role_id==2)
                {
                    $user->pets = explode('|',$user->pets);
                    $user->rental_history = json_decode($user->rental_history);
                    $user->work_history   = json_decode($user->work_history);
                    $user->additional_income   = json_decode($user->additional_income);

                    $application = Application::where('primary_tenant_id',$user->id)->where('property_id',$property->id)->first();


                    return View::Make('userviewPropertySale')->withProperty($property)->withUser($user)->withApplication($application)->with('userdel',$userdel)->with('savesearch',$savesearch)->with('ratings',$ratings)
                                                    ->with('safety',$safety)
                                                    ->with('pet_friendly',$pet_friendly)
                                                    ->with('walkability',$walkability)
                                                    ->with('restaurants',$restaurants)
                                                    ->with('parks',$parks)
                                                    ->with('schools',$schools)
                                                    ->with('traffic',$traffic)
                                                    ->with('parking',$parking)
                                                    ->with('entertainment',$entertainment)
                                                    ->with('transportation',$transportation)
                                                    ->with('community',$community)
                                                    ->with('cleanliness',$cleanliness)
                                                    ->with('overall',$overall)
                                                    ->with('homeLike',$homeLike);
                }
                return View::Make('userviewPropertySale')->withProperty($property)->with('userdel',$userdel)->with('savesearch',$savesearch)->with('ratings',$ratings)
                                                    ->with('safety',$safety)
                                                    ->with('pet_friendly',$pet_friendly)
                                                    ->with('walkability',$walkability)
                                                    ->with('restaurants',$restaurants)
                                                    ->with('parks',$parks)
                                                    ->with('schools',$schools)
                                                    ->with('traffic',$traffic)
                                                    ->with('parking',$parking)
                                                    ->with('entertainment',$entertainment)
                                                    ->with('transportation',$transportation)
                                                    ->with('community',$community)
                                                    ->with('cleanliness',$cleanliness)
                                                    ->with('overall',$overall)
                                                    ->with('homeLike',$homeLike);
            }
            else
            {
                return View::Make('viewPropertySale')->withProperty($property)->with('userdel',$userdel)->with('ratings',$ratings)
                                                    ->with('safety',$safety)
                                                    ->with('pet_friendly',$pet_friendly)
                                                    ->with('walkability',$walkability)
                                                    ->with('restaurants',$restaurants)
                                                    ->with('parks',$parks)
                                                    ->with('schools',$schools)
                                                    ->with('traffic',$traffic)
                                                    ->with('parking',$parking)
                                                    ->with('entertainment',$entertainment)
                                                    ->with('transportation',$transportation)
                                                    ->with('community',$community)
                                                    ->with('cleanliness',$cleanliness)
                                                    ->with('overall',$overall)
                                                    ->with('homeLike',$homeLike);
            }
            return View::Make('viewPropertySale')->withProperty($property)->with('userdel',$userdel)->with('savesearch',$savesearch)->with('ratings',$ratings)
                                                    ->with('safety',$safety)
                                                    ->with('pet_friendly',$pet_friendly)
                                                    ->with('walkability',$walkability)
                                                    ->with('restaurants',$restaurants)
                                                    ->with('parks',$parks)
                                                    ->with('schools',$schools)
                                                    ->with('traffic',$traffic)
                                                    ->with('parking',$parking)
                                                    ->with('entertainment',$entertainment)
                                                    ->with('transportation',$transportation)
                                                    ->with('community',$community)
                                                    ->with('cleanliness',$cleanliness)
                                                    ->with('overall',$overall)
                                                    ->with('homeLike',$homeLike);
        }


        return View::Make('viewProperty')->withProperty($property)->with('userdel',$userdel)->with('savesearch',$savesearch)->with('ratings',$ratings)
                                                    ->with('safety',$safety)
                                                    ->with('pet_friendly',$pet_friendly)
                                                    ->with('walkability',$walkability)
                                                    ->with('restaurants',$restaurants)
                                                    ->with('parks',$parks)
                                                    ->with('schools',$schools)
                                                    ->with('traffic',$traffic)
                                                    ->with('parking',$parking)
                                                    ->with('entertainment',$entertainment)
                                                    ->with('transportation',$transportation)
                                                    ->with('community',$community)
                                                    ->with('cleanliness',$cleanliness)
                                                    ->with('overall',$overall)
                                                    ->with('homeLike',$homeLike);
    }

    public function approveApplication($id)
    {
        $application    =   Application::find($id);
        $application->status    =   'approved';
        $application->save();

        $property=$application->property_id;
        $property_owner=Property::where('id',$application->property_id)->first();

        $applications= Application::where('status','sent')->where('property_id',$property)->get();
       
        if($applications != Null)
        {
            foreach ($applications as $value) 
            {
            $application    =   Application::find($value->id);
            $application->status    =   'denied';
            $application->save(); 
            }

        $coapplicants   =   Applied_tenant::where('application_id',$application->id)->get();

        $property_url = URL::route('viewProperty',$property);

        $data = array('propertyUrl'=>urlencode($property_url));

        foreach($coapplicants as $coapplicant)
        {

            $activity   =   new Activity();
            $activity->user_id  =   $coapplicant->user_id;
            $activity->activity = 'landlord_denied';
            $activity->property_id = $property;
            $activity->application_id   =   $application->id;
            $activity->save();

            if($tenant = User::find($coapplicant->user_id))
            {
                Mail::queue('emails.landlordApplicationDenied',$data, function($message) use($tenant)
                {
                    $message->to($tenant->email,$tenant->first_name)->subject('Landlord denied your application');
                });
            }

        }
        }
        

        $application    =   Application::where('id',$id)->with('property')->first();

        $coapplicants   =   Applied_tenant::where('application_id',$id)->where('user_id',$application->primary_tenant_id)->get();

        $property_url = URL::route('viewProperty',$application->property->id);

        $data = array('propertyUrl'=>urlencode($property_url));

        foreach($coapplicants as $coapplicant)
        {

            $activity   =   new Activity();
            $activity->user_id  =   $coapplicant->user_id;
            $activity->activity = 'landlord_approved';
            $activity->property_id = $application->property->id;
            $activity->application_id   =   $application->id;
            $activity->save();

            if($tenant = User::find($coapplicant->user_id))
            {
                Mail::queue('emails.landlordApplicationConfirmed',$data, function($message) use($tenant)
                {
                    $message->to($tenant->email,$tenant->first_name)->subject('Landlord Confirmed your application');
                });
            }


        }


        return Redirect::back()->with('flash_success',trans('flash.applicationApproved'));
    }

    public function denyApplication($id)
    {
        $application    =   Application::find($id);
        $application->status    =   'denied';
        $application->save();

        $coapplicants   =   Applied_tenant::where('application_id',$id)->get();

        $property_url = URL::route('viewProperty',$application->property->id);

        $data = array('propertyUrl'=>urlencode($property_url));

        foreach($coapplicants as $coapplicant)
        {

            $activity   =   new Activity();
            $activity->user_id  =   $coapplicant->user_id;
            $activity->activity = 'landlord_denied';
            $activity->application_id   =   $application->id;
            $activity->save();

            if($tenant = User::find($coapplicant->user_id))
            {
                Mail::queue('emails.landlordApplicationDenied',$data, function($message) use($tenant)
                {
                    $message->to($tenant->email,$tenant->first_name)->subject('Landlord denied your application');
                });
            }

        }

        return Redirect::back()->with('flash_error',trans('flash.applicationDenied'));
    }

    public function deleteProperty($id)
    {
        if($this->landlord->deleteProperty($id))
        {
             
            return Redirect::route('landlordDashboard')->with('flash_success',trans('flash.propertyDeleted'));
        }
        else
        {
            return Redirect::back()->with('flash_errors',$this->landlord->errors);
        }
    }

    public function deletePropertySendReqest($id)
    {
    
     if($this->landlord->deleteRequest($id))
        {
            return Redirect::route('landlordDashboard')->with('flash_success',trans('flash.propertyRequest'));
        }
        else
        {
            return Redirect::back()->with('flash_errors',$this->landlord->errors);
        }
     
    }


    public function subscribe()
    {
        return View::make('landlord.subscribe');
    }

    public function subscribed(){
        $token = Input::get('stripeToken');
        //dd(Input::all());
        //add the creadit activation code right here
        Auth::user()->subscription('monthly')->create($token);
        Auth::user()->subscription()->decrement();

        return Redirect::route('landlordDashboard')->with('flash_success','Your trial period is started');
    }

    public function requestContact($id)
    {
        $input = Input::all();
      
       
        if(Auth::check())
        {
        $this->user = Auth::user();
        $user = User::where('id',$this->user->id)->first();
        $des = Input::get('des');
        if(Input::get('phone') ==Null)
        {
            $phone = $user->phone;
        }else
        {
            $phone = Input::get('phone');   
        }
        $p_id = Input::get('property_id');
        $user_property = property::where('id',$p_id)->first();
        
        $vars['user_id'] = $user->id;
        $vars['first_name'] = $user->first_name;
        $vars['last_name'] =  $user->last_name;
        $vars['email'] =  $user->email;
        $vars['phone_number'] = $phone;
        $vars['description'] = $des;
        $vars['property_id'] = $p_id;
        $vars['choice'] = $user_property->choice;
        $event = SendRequest::create($vars);

        $act = new Activity;
        $act->user_id = $user_property->user_id;
        $act->property_id = $p_id;
        if($user_property->choice == 0)
        {
        $act->activity = "send_rent_request";
        }else {
        $act->activity = "send_sale_request";
        }
        $act->save();
        }
         
         
         Mail::queue('emails.requestContact',array('des' => $des, 'phone' => $phone, 'property_id' => $p_id), function($message) use($user)

         {
             $message->to($user->email,$user->first_name)->subject('Request Contact Information');
         });

         return Redirect::back()->with('msg',"Successfully request sent");
    }

    public function landlordHelp(){
        if(Auth::check())
        {
        $this->user = Auth::user();
        $user = User::where('id',$this->user->id)->first();
        $help = Help::where('user_id',$this->user->id)->get();
        return View::make('landlord.landlordHelp',['currentPage'=>'Help'])->withUser($this->user)->withProperties($this->properties)->withDemo($help);
        }

    }

    public function createlandlordHelp(){
        $message = Input::get('message');
        $title = Input::get('title');

        if(Auth::check())
        {
        $this->user = Auth::user();
        $user = User::where('id',$this->user->id)->first();
        $helps = Help::where('user_id',$this->user->id)->get();
        $help = new Help;
        $help->user_id = $user->id;
        $help->message = $message;
        $help->title =$title;
        $help->status ="0";
        
        $help->save();
        }
         
        return Redirect::back();
    }

    public function landlordVacate(){

        $all_tenant = Property::where('properties.user_id',$this->user->id)
            ->leftJoin('tenants', 'tenants.property_id', '=', 'properties.id')
            ->where('tenants.ask_for_vacate',1)->where('tenants.vacate_status',1)->get();

        return View::make('landlord.viewVacateNotice',['currentPage'=>'vacate_notice'])->withUser($this->user)->withProperties($this->properties)->with('tenant',$all_tenant);

    }

    public function deletelandlordHelp($id){
       if($this->landlord->deleteHelp($id))
        {
            return Redirect::back()->with('flash_success',trans('flash.landlordsHelp'));;
        }
        else
        {
            return Redirect::back()->with('flash_errors',$this->landlord->errors);
        }
    }

     public  function landlordVacateApprove($id){
        $tent = Tenant::find($id);
         $tent->vacate_status = 2;
         $tent->save();

         return Redirect::back()->with('flash_success',"Approved Successfully");
     }

    public function landlordVacateDecline($id){
        $tent = Tenant::find($id);
        $tent->vacate_status = 3;
        $tent->save();

        return Redirect::back()->with('flash_success',"Declined Successfully");
    }


}