<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 09/08/14
 * Time: 2:10 PM
 */

use Roomy\Repositories\Renter\RenterRepository as RenterRepo;

class RenterController extends BaseController{

    /**
     * @var Roomy\Repositories\Renter
     */

    private $renter;

    function __construct(RenterRepo $renter){
        $this->renter   = $renter;

        if(Auth::check())
        {
            $this->user = Auth::user();
            $this->properties = Tenant::where('user_id',$this->user->id)->with('property')->get();
        }

        $this->beforeFilter('renter',[
            'only'  =>  [
                'viewDashboard',
                'editProfile',
                'updateProfile',
                'applyForProperty',
                'acceptApplication',
                'rejectApplication',
                'viewProperty',
                'viewProperties'
            ]
        ]);


    }

    public function viewDashboard(){
        $activities = Activity::where('user_id',Auth::user()->id)->with('application')->with('property')->orderby('created_at','desc')->paginate('10');

        return View::make('renter.dashboard',array('currentPage'=>'dashboard'))->withUser($this->user)->withProperties($this->properties)->withActivities($activities);
    }


    public function editProfile(){
        $this->user->pets = explode('|',$this->user->pets);
        $this->user->rental_history = json_decode($this->user->rental_history);
        $this->user->work_history   = json_decode($this->user->work_history);
        $this->user->additional_income   = json_decode($this->user->additional_income);
        return View::Make('renter.editProfile',array('currentPage'=>'editProfile'))->withUser($this->user)->withProperties($this->properties);
    }

    public function updateProfile(){
        if($this->renter->updateProfile(Input::all()))
        {
            return Redirect::Route('renterEditProfile')->with('flash_success',trans('flash.profileUpdated'));
        }
        else
        {
            return Redirect::Route('renterEditProfile')->with('flash_error',trans('flash.error'));
        }
    }

    public function applyForProperty($id){
        if($this->renter->updateProfile(Input::all()))
        {
            $this->renter->applyForProperty($id,Input::all());

            return Redirect::Route('viewProperty',array('id'=>$id))->with('flash_success',trans('flash.appliedForProperty'));
        }
        else
        {
            return Redirect::Route('renterEditProfile')->with('flash_error',trans('flash.error'));
        }
    }

    public function acceptApplication($id){
        if(!$this->renter->isEveryApplicantRegistered($id))
        {
            return Redirect::back()->with('flash_error',trans('flash.applicantsNotRegistered'));
        }
        if($this->renter->acceptApplication($id))
        {
            return Redirect::back()->with('flash_success',trans('flash.applicationAccepted'));
        }
        else
        {
            return Redirect::back()->with('flash_error',trans('flash.error'));
        }
    }

    public function rejectApplication($id){
        if($this->renter->rejectApplication($id))
        {
            return Redirect::back()->with('flash_success',trans('flash.applicationRejected'));
        }
        else
        {
            return Redirect::back()->with('flash_error',trans('flash.error'));
        }
    }

    public function viewProfile($id){
        $user = $this->renter->find($id);

        return View::Make('renter.viewProfile')->withUser($user)->withProperties($this->properties);
    }

    public function updateDp(){
        if($this->renter->updateDp(Input::all()))
        {
            return Redirect::Route('renterEditProfile')->with('flash_success',trans('flash.profilePictureUpdated'));
        }
        else
        {
            return Redirect::Route('renterEditProfile')->with('flash_errors',trans('flash.error'));
        }
    }

    public function viewProperty($id){
        $property   =   $this->renter->findProperty($id);

        $tenant = Tenant::where('user_id',Auth::user()->id)->where('property_id',$id)->first();

        $deposit = Deposit_transaction::where('user_id',Auth::user()->id)->where('property_id',$id)->first();

        $rents   =   Rent_transaction::where('user_id',Auth::user()->id)->where('property_id',$id)->orderBy('created_at','desc')->get();

        return View::Make('renter.viewProperty',array('currentPage'=>'viewProperty'.$property->id))->withUser($this->user)->withProperty($property)->withProperties($this->properties)->withTenant($tenant)->withDeposit($deposit)->withRents($rents);
    }

    public function payDeposit($id){

        $property = Property::where('id',$id)->with('landlord')->first();

        $data = array();
        $data['payee_email'] = $property->landlord->paypal_email;
        $data['cancel_url'] = URL::route('viewPropertyAsRenter',$id);
        $data['return_url'] = URL::route('paidDeposit',$id);
        $data['ipn_url'] = '';
        $data['item_name'] = 'Deposit for'.$property->name;
        $data['amount'] = $property->deposit;

        return View::make('renter.paypalCheckout',$data);
    }

    public function paidDeposit($id){

        if(Input::get('payment_status')=="Completed" || Input::get('payment_status')=="Pending")
        {
            if($this->renter->paidDeposit($id))
            {
                return Redirect::route('viewPropertyAsRenter',$id)->with('flash_success',trans('flash.paidDeposit'));
            }
            else
            {
                return Redirect::route('viewPropertyAsRenter',$id)->with('flash_error','flash.error');
            }
        }
        else
        {
            return Redirect::route('viewPropertyAsRenter',$id)->with('flash_error','flash.error');
        }

    }

    public function payRent($id){

        $property = Property::where('id',$id)->with('landlord')->first();

        $tenant = Tenant::where('user_id',Auth::user()->id)->where('property_id',$id)->first();

        $user_id = Auth::user()->id;
        $app = DB::table('applications')->where('property_id',$id)->where('primary_tenant_id',$tenant->user_id)->where('status','accepted')->first();

        $data = array();
        if($app)
        {

            if($app->primary_tenant_id == $tenant->user_id)
            {
                 
                Log::info("testing paypal");
                $data['payee_email'] = $property->landlord->paypal_email;
                $data['amount'] = $property->rent;

            }
            else
            {

                $tenants = Tenant::where('property_id',$id)->get();
                foreach($tenants as $ten)
                {
                    $main_renter = Application::where('status','accepted')->where('primary_tenant_id',$ten->user_id)->first();
                    if($main_renter)
                    {
                        $email = User::where('id',$main_renter->primary_tenant_id)->first();
                        if($email)
                        {
                            $data['payee_email'] = $email->paypal_email;
                            $data['amount'] = $tenant->rent;
                        }

                    }
                }
            }
        }
        else
        {

            $tenants = Tenant::where('property_id',$id)->get();
            foreach($tenants as $ten)
            {
                $main_renter = Application::where('status','accepted')->where('primary_tenant_id',$ten->user_id)->first();
                if($main_renter)
                {
                    $email = User::where('id',$main_renter->primary_tenant_id)->first();
                    if($email)
                    {
                        $data['payee_email'] = $email->paypal_email;
                        $data['amount'] = $tenant->rent;
                    }

                }
            }
        }
        $data['cancel_url'] = URL::route('viewPropertyAsRenter',$id);
        $data['return_url'] = URL::route('paidRent',$id);
        $data['ipn_url'] = '';
        $data['item_name'] = 'Rent for '.$property->name;
        

        return View::make('renter.paypalCheckout',$data);
    }

    public function paidRent($id){

        if(Input::get('payment_status')=="Completed" || Input::get('payment_status')=="Pending")
        {
            if($this->renter->paidRent($id))
            {
                return Redirect::route('viewPropertyAsRenter',$id)->with('flash_success',trans('flash.paidRent'));
            }
            else
            {
                return Redirect::route('viewPropertyAsRenter',$id)->with('flash_error','flash.error');
            }
        }
        else
        {
            return Redirect::route('viewPropertyAsRenter',$id)->with('flash_error','flash.error');
        }

    }

    public function applications(){
        $applications = User::find(Auth::user()->id)->applications()->get();

        return View::Make('renter.applications',array('currentPage'=>'renterApplications'))->withUser($this->user)->withProperties($this->properties)->withApplications($applications);
    }

    public function viewProperties(){

        $properties = Property::with('photos','landlord')->orderBy('created_at','desc')->paginate(9);


        return View::make('renter.viewProperties',array('currentPage'=>'viewProperties'))->withUser($this->user)->withProperties($properties)->with('propertiesArray',$properties->toArray());

    }

   public function renterHelp(){
        if(Auth::check())
        {
        $this->user = Auth::user();
        $user = User::where('id',$this->user->id)->first();
        $help = Help::where('user_id',$this->user->id)->get();
        return View::make('renter.renterHelp',['currentPage'=>'Help'])->withUser($this->user)->withProperties($this->properties)->withDemo($help);
        }

    }

    

    public function createrenterHelp(){
        $message = Input::get('message');
        $title = Input::get('title');

        if(Auth::check())
        {
        $this->user = Auth::user();
        $user = User::where('id',$this->user->id)->first();
        $helps = Help::where('user_id',$this->user->id)->get();
        $help = new Help;
        $help->user_id = $user->id;
        $help->message = $message;
        $help->title =$title;
        $help->status ="0";
        
        $help->save();
        }
         
        return Redirect::back();
    }

    public function renterVacate($id){
        $user_id = Auth::user()->id;

        $ask_vacate = Tenant::where('property_id',$id)->where('user_id',$user_id)->first();
        $ask_vacate->ask_for_vacate = 1;
        $ask_vacate->vacate_status = 1;
        $ask_vacate->vacate_from = 1;
        //1 for renter
        //2 for landlord
        $ask_vacate->vacate_date = date('Y-m-d', strtotime("+30 days"));
        $ask_vacate->save();

        return Redirect::back()->with('flash_success',"You request for Vacate Notice Please Wait for landlord approval");

    }

    

      public function deleteRenterHelp($id){
       if($this->renter->deleteHelp($id))
        {
            return Redirect::back()->with('flash_success',trans('flash.landlordsHelp'));;
        }
        else
        {
            return Redirect::back()->with('flash_errors',$this->landlord->errors);
        }
    }

    public function renterchangepassword()
    {

        $your_email=Input::get('your_email');
        $curr_password=Input::get('current_password');
        $change_password=Input::get('change_password');
        $role_id=Input::get('role_id');
        if($your_email)
        {
            $user=User::where('email',$your_email)->where('role_id',$role_id)->first();
            if($user){
                if($curr_password){
                     
                    if(Hash::check($curr_password, $user->password))
                    {  

                        $new_password = Hash::make($change_password);
                         
                        $user = User::where('id',$user->id)->update(array('password' => $new_password));
                        return Redirect::back()->with('msg', "Updated Successfully");
                    }else{

                        return Redirect::back()->with('msg', "Please Enter the Current password correctly");
                    }

                }else{
                return Redirect::back()->with('msg', "Please Enter the password");
            }

            }else
            {
                return Redirect::back()->with('msg', "Please Enter the vaild Admin Email Id");
            }
        }
        else
        {
            return Redirect::back()->with('msg', "Please Enter the  Email Id");
        }
    }

} 