<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

    public function sendMail()
    {
        $date = date('d');
        $properties = Property::where('choice',0)->get();
        foreach($properties as $property)
        {
            Log::info($date);
            $start_date = $property->rent_start_date;
            $start_date = $start_date -1;
            if($start_date == $date)
            {
                Log::info("tesss");
                $application = Application::where('property_id',$property->id)->where('status','accepted')->first();
                if($application)
                {
                    $tenants = Tenant::where('property_id',$property->id)->get();
                    foreach($tenants as $tenant)
                    {
                        $user = User::where('id',$tenant->user_id)->first();
                        Log::info('testing');
                        $data = array('propertyUrl'=>"test");

                        Mail::send('emails.renterRemainder',$data, function($message) use($user)
                        {
                            $message->to($user->email)->subject('Rent Remainder');
                        });
                    }
                }
            }
            else
            {
                // do nothing
            }

        }
    }

    public function vacate()
    {
        $date = date('Y-m-d');
        $tenant = Tenant::where('ask_for_vacate',1)->where('vacate_status',1)->where('vacate_date',$date)->get();
        if($tenant)
        {
            foreach($tenant as $ten)
            {
                $user = User::where('id',$ten->user_id)->first();
                $data = array('propertyUrl'=>"test");

                Mail::send('emails.renterRemainder',$data, function($message) use($user)
                {
                    $message->to($user->email)->subject('Rent Remainder');
                });
            }
        }
        else
        {
            //do nothing
        }
    }


}
