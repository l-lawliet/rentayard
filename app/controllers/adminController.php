<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 09/08/14
 * Time: 2:10 PM
 */

use Roomy\Repositories\Admin\AdminRepository as AdminRepo;

class AdminController extends BaseController{

    /**
     * @var Roomy\Repositories\Admin
     */

    private $admin;

    function __construct(AdminRepo $admin){
        $this->admin   = $admin;
        if(Auth::check())
        {
            $this->user = Auth::user();
        }

        $this->beforeFilter('admin');

    }

    public function viewDashboard(){

        $total_users = User::all()->count();
        $total_deposits = Deposit_transaction::all()->sum('amount');
        $total_rent = Rent_transaction::all()->sum('amount');
        $total_properties = Property::all()->count();

        return View::make('admin.dashboard',array('currentPage'=>'dashboard'))->withUser($this->user)->with('total_users',$total_users)->with('total_deposits',$total_deposits)->with('total_rent',$total_rent)->with('total_properties',$total_properties);
    }

    public function userManagement(){

        if(isset($_GET['role'])){
            if(Input::get('role')=='landlord')
                $users = User::where('role_id',1)->paginate(10);
            elseif(Input::get('role')=='renter')
                $users = User::where('role_id',2)->paginate(10);
            else
                $users = User::where('role_id',3)->paginate(10);
        }else
        {
            $users = User::paginate(10);
        }
        return View::Make('admin.userManagement',array('currentPage'=>'userManagement'))->withUser($this->user)->withUsers($users);
    }

    public function adminAddUser()
    {
            return View::make('admin.addUser', array('currentPage'=>'userManagement'))->withUser($this->user);
    }
    public function adminAddUserProcess()
    {
        $email = Input::get('email');
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {
            return Redirect::back()->with('flash_errors',$validator->messages()->all());
        }
        else
        {
            $user = User::where('email',Input::get('email'))->first();
            if($user)
            {
                $this->errors = "User already exists";
                return Redirect::back()->with('flash_error',"Email ID already exist");
            }
            $user = new User;
            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');
            $user->email = Input::get('email');

            if(Input::get('role')=='landlord')
            {
                $user->role_id = 1;
            }elseif(Input::get('role')=='renter')
            {
                $user->role_id = 2;
            }else
            {
                $user->role_id = 3;
            }

            $user->is_activated = 0;

            $new_password = time();
            $new_password .= rand();
            $new_password = sha1($new_password);
            $new_password = substr($new_password, 0, 8);
            $user->password = Hash::make($new_password);


            $subject = "Welcome On Board";
            $email_data['name'] = $user->username;
            $email_data['password'] = $new_password;
            $email_data['email'] = $user->email;

            if($user)
            {
                Mail::send('emails.newUser', array('email_data' => $email_data), function ($message) use ($email, $subject) {
                    $message->to($email)->subject($subject);
                });
            }

            $user->activation_code = Hash::make(time().$user->email);

            $data = array('name' => Input::get('first_name'),'activationCode'=>urlencode($user->activation_code));

            Mail::queue('emails.activation',$data, function($message)
            {
                $message->to(Input::get('email'),Input::get('first_name')." ".Input::get('last_name'))->subject('Activation Mail');
            });

            $user->save();

            Applied_tenant::where('email',Input::get('email'))->update(array('user_id'=>$user->id));

            $activity   =   new Activity;
            $activity->user_id  =   $user->id;
            $activity->activity =   'joined_roomy';

             $activity->save();

            return Redirect::back()->with('flash_success',"User Added successfully");
        }
    }

    public function propertyManagement(){

        $properties = Property::paginate(10);

        return View::make('admin.propertyManagement',array('currentPage'=>'propertyManagement'))->withUser($this->user)->withProperties($properties);

    }


    public function deleteProperty($id){

        $applicaton = Application::where('property_id',$id)->count();
        

        if($applicaton != 0)
        {
         
        Applicaton::where('property_id',$id)->delete(); 
        }
        
        Activity::where('property_id',$id)->delete();

        SendRequest::where('property_id',$id)->delete();

        Savesearch::where('property_id',$id)->delete();

        Property::find($id)->delete();

        return Redirect::back()->with('flash_success',"Property Deleted Successfully");
        }
    

    public function deleteUser($id){
        if($this->admin->deleteUser($id))
        {
            return Redirect::back()->with('flash_success',trans('flash.userDeleted'));
        }
        else
        {
            return Redirect::back()->with('flash_error',trans('flash.error'));
        }
    }

    public function editUser($id){
        if($user = $this->admin->getUser($id))
        {
            $user->pets = explode('|',$user->pets);
            $user->rental_history = json_decode($user->rental_history);
            $user->work_history   = json_decode($user->work_history);
            $user->additional_income   = json_decode($user->additional_income);
            return View::make('admin.editUser',array('currentPage'=>'editUser'))->withUser($user);
        }
    }

    public function settings(){
        return View::make('admin.settings',array('currentPage'=>'settings'))->withUser($this->user);
    }

    public function updateSettings(){
        if($this->admin->updateSettings(Input::all()))
        {
            return Redirect::back()->with('flash_success',trans('flash.settingsUpdated'));
        }
        else
        {
            return Redirect::back()->with('flash_error',trans('flash.error'));
        }
    }

    public function editEmailTemplates(){
        return View::make('admin.editEmailTemplates',array('currentPage'=>'editEmailTemplates'))->withUser($this->user);
    }

    public function updateEmailTemplates(){
        if($this->admin->updateEmailTemplates(Input::all()))
        {
            return Redirect::back()->with('flash_success',trans('flash.emailTemplatesUpdated'));
        }
        else
        {
            return Redirect::back()->with('flash_error',trans('flash.error'));
        }
    }

    public function updateUser($id){
        if($this->admin->updateUser($id,Input::all()))
        {
            return Redirect::Route('userManagement')->with('flash_success',trans('flash.profileUpdated'));
        }
        else
        {
            return Redirect::Route('userManagement')->with('flash_error',trans('flash.error'));
        }
    }


    public function rentTransactions(){

        $transactions = Rent_transaction::get();
        $transactions_details=array();
        foreach($transactions as $transactions_data)
        {
            //dd($transactions_data->user_id);
            $user=User::where('id',$transactions_data->user_id)->first();
 
            $owner=Property::where('id',$transactions_data->property_id)->first(); 
         
            $owner_info=User::where('id',$owner->user_id)->first();

            $transactions_info= Rent_transaction::where('id',$transactions_data->id)->first();

            array_push($transactions_details,array('tenant'=>$user->first_name,'owner'=>$owner_info->first_name.$owner_info->last_name,'transactions_info_date'=>$transactions_info->created_at,'transactions_info_mode'=>$transactions_info->mode,'transactions_info_amount'=>$transactions_info->amount));
        }

        $transactions = Rent_transaction::with('user','property')->paginate(10)->toArray();

        return View::make('admin.rentTransactions',['currentPage'=>'rentTransactions'])->withTransactions($transactions_details)->withUser($this->user);

    }
     public function adminHelp(){
        $help = DB::table('users')->whereIn('role_id', array(1, 2))->rightJoin('help','help.user_id','=','users.id')
                    ->select('users.first_name as first_name','users.email as email','help.title as title','help.message as message','help.status as status','help.id as id')->get();

        
        return View::make('admin.help',['currentPage'=>'Help'])->withUser($this->user)->withHelp($help);

    }

     public function resolvedHelp($id){
        if($this->admin->updateHelp($id))
        {
            return Redirect::back()->with('flash_success',trans('flash.AdminHelp'));;
        }
        else
        {
            return Redirect::back()->with('flash_errors',$this->admin->errors);
        }

    }

    public function adminAddPropertyRent($id){
      return View::Make('admin.adminAddPropertyRent',array('currentPage'=>'addProperty'))->withUser($this->user)->withId($id);

    }

     public function adminAddPropertyRentProcess($id){
      if($var = $this->admin->addPropertyRent(Input::all(),$id))
        {

            return Redirect::back()->with('flash_success',trans('flash.propertyAdded'));
        }
        else
        {
            return Redirect::back()->with('flash_errors',$this->admin->errors);;
        }

    }


    public function adminAddPropertySale($id){
      return View::Make('admin.adminAddPropertySale',array('currentPage'=>'addProperty'))->withUser($this->user)->withId($id);

    }

     public function adminAddPropertySaleProcess($id){
      if($var = $this->admin->addPropertySale(Input::all(),$id))
        {

            return Redirect::back()->with('flash_success',trans('flash.propertyAdded'));
        }
        else
        {
            return Redirect::back()->with('flash_errors',$this->admin->errors);;
        }
    }

   public function editPropertyrent($id){
      $property = $this->admin->findProperty($id);
        if($property->choice == 0)
        {
            return View::make('admin.editPropertyRent',array('currentPage'=>'viewProperty'.$id))->withUser($this->user)->withProperty($property);
        }
        else
        {
            return View::make('admin.editPropertySale',array('currentPage'=>'viewProperty'.$id))->withUser($this->user)->withProperty($property);
        }
   }


   public function adminupdateProperty($id){
    $proty = Property::where('id',$id)->where('choice',0)->first();
        if($proty)
        {
            if($this->admin->updateProperty($id,Input::all()))
            {
                return Redirect::back()->with('flash_success',trans('flash.propertyUpdated'));
            }
            else
            {
               return Redirect::back()->with('flash_success',$this->admin->errors);
               
            }
        }
        else
        {
            if($this->admin->updatePropertySale($id,Input::all()))
            {
                return Redirect::back()->with('flash_success',trans('flash.propertyUpdated'));
            }
            else
            {
                 return Redirect::back()->with('flash_success',$this->admin->errors);
            }
        }
    }

  


} 