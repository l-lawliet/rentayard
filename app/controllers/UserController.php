<?php

use Roomy\Repositories\User\UserRepository as UserRepo;

class UserController extends BaseController {

    /**
     * @var Roomy\Repositories\User
     */

    private $user;

    public function __construct(UserRepo $user){
        $this->user = $user;
        $this->beforeFilter('guest',[
            'only' => [
                'signup',
                'login',
                'processSignup',
                'activate',
                'processLogin',
                'installProcess'
            ]
        ]);

        $this->beforeFilter('auth',[
            'only'  =>  [
                'saveHome',
                'saveSearch',
                'savedHome',
                'savedSearch',
                'logout'
            ]
        ]);
    }
	public function signup(){
        return View::make('signup');
    }

    public function login(){
        return View::make('login');
    }

     public function forgotPassword(){
        return View::make('forgot_password');
    }

       public function help(){
        
        return View::make('forgot_password');
    }

    public function processSignup(){

        if($this->user->register(Input::except('_token')))
        {
            return Redirect::back()->with('flash_success','Please check your email for activation mail');
        }
        else
        {
            return Redirect::back()->with('flash_errors',$this->user->errors);
        }
    }

    public function activate($code){



        if($this->user->activate($code))
        {
            return Redirect::route('login')->with('flash_success','You are authenticated successfully');
        }
        else
        {
            return Redirect::route('signup')->with('flash_errors',$this->user->errors);
        }
    }

    public function processLogin(){
        if($this->user->attemptLogin(Input::get('email'),Input::get('password')))
        {
            $user = Auth::user();
            if($user->role_id=='1')
            {
                return Redirect::route('landlordDashboard')->with('flash_success','Welcome to RentaYawd feel free to post an look around our community for the most convenient housing for rent. Also bare in mind we are working on ways to remove the hasle of transaction for a housing between guest/host');
            }
            elseif($user->role_id=='2')
            {
                return Redirect::route('renterDashboard')->with('flash_success','Logged In Successfully');
            }
            elseif($user->role_id=='3')
            {
                return Redirect::route('adminDashboard')->with('flash_success','Logged In Successfully');
            }
            else
            {
                echo "Role is not defined to this user.";
            }
        }
        else
        {
            return Redirect::route('login')->with('flash_errors',$this->user->errors);
        }

    }

     public function forgotPasswordprocess()
     {
        if($this->user->forgotPassowrd(Input::get('login_email')))
        {
           return Redirect::back()->with('flash_success',$this->user->errors);
             
        }else
        {
         return Redirect::back()->with('flash_errors',$this->user->errors);
        }
    }

    public function logout()
    {
        Auth::logout();

        return Redirect::route('login')->with('flash_success','Logged out successfully');
    }

    public function home()
    {

      try{
      DB::connection()->getDatabaseName();
       
      $properties = Property::with('photos','landlord')->orderBy('created_at','desc')->paginate(9);
        if(Auth::check())
        {
            return View::make('userHome')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
        else
        {
            return View::make('home')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }

       }
       catch(Exception $e){
       return View::make('home');
       }
        
    }

    public function search()
    {
        $pro = Property::with('photos')->get();
       // dd($pro);
        $title = Input::get('search');
        if(Input::get('type')== 'rent'){
            $type = 0;
        }else if(Input::get('type') == 'buy'){
            $type = 1;
        }

        Session::put('searchs',$title);
        $searchTerms = explode(' ', $title);

        $property = Property::with('photos','landlord')->where('choice',$type);

        if($type == 0){
            $property->where('showcase',1);
        }

        foreach($searchTerms as $term)
        {
            //lets add a where the landlord points out its available
            $property->where('address', 'LIKE', '%'. $term .'%')->orWhere('city', 'LIKE', '%'. $term .'%');
        }

        $properties = $property->orderBy('created_at','desc')->paginate(9);
      // dd($properties);
        if($title != ""){ $flag=1;}else{$flag=0;}
        if(Auth::check())
        {
            if($type == 0){
                return View::make('searchRent')->withProperties($properties)
                    // if($title != ""){ ->withFlag(1)}else{->withFlag(0)}
                    ->with('propertiesArray',$properties->toArray());
            }elseif($type==1){
                return View::make('searchBuy')->withProperties($properties)
                    // if($title != ""){ ->withFlag(1)}else{->withFlag(0)}
                    ->with('propertiesArray',$properties->toArray());
            }
//            return View::make('userSearch')->withProperties($properties)
//            ->withFlag($flag)
//            ->with('propertiesArray',$properties->toArray());
        }
        else
        {
            if($type == 0){
                return View::make('searchRent')->withProperties($properties)
                    // if($title != ""){ ->withFlag(1)}else{->withFlag(0)}
                    ->with('propertiesArray',$properties->toArray());
            }elseif($type==1){
                return View::make('searchBuy')->withProperties($properties)
                    // if($title != ""){ ->withFlag(1)}else{->withFlag(0)}
                    ->with('propertiesArray',$properties->toArray());
            }
        }

    }

    public function saveHome($id)
    {
        $user_id = Auth::user()->id;
        
        $savesearchs = new Savesearch;
        $savesearchs->user_id = $user_id;
        $savesearchs->property_id = $id;
        $savesearchs->option = 1;
        $savesearchs->save();

        if($savesearchs)
        {
            return Redirect::back()->with('msg',"Home Saved Successfully");
        }
        else
        {
            return Redirect::back()->with('msg',"Something Went wrong");
        }
    }

    public function saveSearch()
    {
        $id = Auth::user()->id;
        $key = Session::get('searchs');
        $savesearchs = new Savesearch;
        $savesearchs->user_id = $id;
        $savesearchs->option = 0;
        $savesearchs->keyword = $key;
        $savesearchs->save();

        if($savesearchs)
        {
            return Redirect::back()->with('msg',"Search Saved Successfully");
        }
        else
        {
            return Redirect::back()->with('msg',"Something went Wrong");
        }
    }

    public function savedHome()
    {
        $savedHome = Savesearch::where('user_id',Auth::user()->id)->get();
        //$properti = Property::with('photos','landlord');
        $i = 0;
        $propertie = null;
        $photo = null;
        $landlord = null;
        $properties = Property::where('user_id',Auth::user()->id)->with('photos')->get();
        foreach ($savedHome as $home)
            {
            
               // $properties = Property::with('photos','landlord')->where('id',$home->property_id)->paginate(9);
                if($home->property_id != null)
                {
                    $propertie[] = Property::where('id',$home->property_id)->first();
                    $photo[] = DB::table('property_photos')->where('property_id',$home->property_id)->where('deleted_at',NULL)->first();
                    $landlord[] = DB::table('users')->where('id',$propertie[$i]->user_id)->first();
                    $i++;
                }
                else
                {
                    //do nothing    
                }
            }
            // dd($landlord);
            // dd($photo);
            // dd($properties);
            //$properties = $properti->paginate(9);
        if($propertie)
        {
            

             

            if(Auth::user()->role_id == 2)
            {
                return View::make('renter.savedHome',array('currentPage'=>'dashboard'))->withProperties($properties)->with('count',$i)->with('property',$propertie)->with('photo',$photo)->with('landlord',$landlord);
            }
            else
            {

                return View::make('landlord.savedHome',array('currentPage'=>'dashboard'))->withProperties($properties)->with('count',$i)->with('property',$propertie)->with('photo',$photo)->with('landlord',$landlord);
            }
        }
        else
        {
            if(Auth::user()->role_id == 2)
            {
                return View::make('renter.savedHome',array('currentPage'=>'dashboard'))->withProperties($properties)->with('count',$i)->with('msg',"No homes are saved");
            }
            else
            {
                return View::make('landlord.savedHome',array('currentPage'=>'dashboard'))->withProperties($properties)->with('count',$i)->with('msg',"No homes are saved");
            }
        }
    }

    public function deleteSavedHome($id)
    {
        $del = Savesearch::where('property_id',$id)->where('user_id',Auth::user()->id)->delete();
        if($del)
        {
            return Redirect::back()->with('msg',"Saved home deleted Successfully");
        }
        else
        {
            return Redirect::back()->with('msg',"Something went wrong");
        }
    }

    public function savedSearch()
    {
        $saveSearch = Savesearch::where('user_id',Auth::user()->id)->where('option',0)->get();
        $properties = Property::where('user_id',Auth::user()->id)->with('photos')->get();
        if(Auth::user()->role_id == 2)
        {
            return View::make('renter.savedSearch', array('currentPage'=>'dashboard'))->withProperties($properties)->with('saveSearch',$saveSearch);
        }
        else
        {
            return View::make('landlord.savedSearch', array('currentPage'=>'dashboard'))->withProperties($properties)->with('saveSearch',$saveSearch);
        }
    }

    public function userSearch($key)
    {
        $pro = Property::with('photos')->get();
       // dd($pro);
        $title = $key;
       
        $searchTerms = explode(' ', $title);

        $property = Property::with('photos','landlord');

        foreach($searchTerms as $term)
        {
            $property->where('address', 'LIKE', '%'. $term .'%');
        }

        $properties = $property->orderBy('created_at','desc')->paginate(9);
      // dd($properties);
        if(Auth::check())
        {

            return View::make('userSearch')->withProperties($properties)->with('propertiesArray',$properties->toArray())->with('flag',0);

        }
        else
        {
            return View::make('search')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
    }

    public function deleteSearch($key)
    {
        $saveSearch = Savesearch::where('user_id',Auth::user()->id)->where('keyword',$key)->delete();
        if($saveSearch)
        {
            return Redirect::back()->with('msg',"Saved search page has been deleted");
        }
        else
        {
            return Redirect::back()->with('msg',"Something went wrong");
        }
        
    }

    public function buy()
    {
        $properties = Property::with('photos','landlord')->where('choice',1)->where('showcase',1)->orderBy('id', 'DESC')->where('showcase',1)->paginate(9);
        if(Auth::check())
        {
            return View::make('userBuy')->withProperties($properties)->with('propertiesArray',$properties->toArray());    
        }
        else
        {
            return View::make('buy')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
    }

    public function rent()
    {
        $properties = Property::with('photos','landlord')->where('showcase',1)->where('choice',0)->where('showcase',1)->orderBy('id', 'DESC')->paginate(9);
        if(Auth::check())
        {
            return View::make('userSell')->withProperties($properties)->with('propertiesArray',$properties->toArray());    
        }
        else
        {
            return View::make('sell')->withProperties($properties)->with('propertiesArray',$properties->toArray());    
        } 
    }

    public function buySearch()
    {
        $val = Session::get('searchs');
        $searchTerms = explode(' ', $val);

        $property = Property::with('photos','landlord');

        foreach($searchTerms as $term)
        {
            $property->where('address', 'LIKE', '%'. $term .'%');
        }
        $property->where('choice', 1)->where('showcase',1);

        $properties = $property->orderBy('created_at','desc')->paginate(9);


      // dd($properties);
        if(Auth::check())
        {
            return View::make('userSearchBuy')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
        else
        {
            return View::make('searchBuy')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
    }

    public function rentSearch()
    {
        $val = Session::get('searchs');
        $searchTerms = explode(' ', $val);

        $property = Property::with('photos','landlord');

        foreach($searchTerms as $term)
        {
            $property->where('address', 'LIKE', '%'. $term .'%');
        }
        $property->where('choice', 0)->where('showcase',1);

        $properties = $property->orderBy('created_at','desc')->paginate(9);


      // dd($properties);
        if(Auth::check())
        {
            return View::make('userSearchRent')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
        else
        {
            return View::make('searchRent')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
    }

    public function filterSearch()
    {
        return "hello";
    }

    public function filterSearchBuy()
    {
        $val = Input::get('search');
        $min = Input::get('min_price');
        $max = Input::get('max_price');
        $bed = Input::get('bed');
        $bath = Input::get('bath');
        $searchTerms = explode(' ', $val);
        $car = Input::get('car');

        // $properties = DB::table('properties')->where('choice', 1);

        // $property->whereBetween('price', array($min, $max));

        $property = Property::with('photos','landlord');

         foreach($searchTerms as $term)
         {
             $property->where('address', 'LIKE', '%'. $term .'%');
         }
        $property->where('choice', 1);

        if($min != "" && $max != ""){
            $property->whereBetween('rent', array($min, $max));
        }

        if($car != "")
        {
            $property->where('parking','>',$car);
        }

        if($bed != ""){
            $property->where('bedrooms','>',$bed);
        }

        if($bath != ""){
            $property->where('bathrooms','>',$bath);
        }

        //$property->where('price','>',$min)->where('price','<',$max);

        $properties = $property->orderBy('created_at','desc')->paginate(9);


      // dd($properties);
        if(Auth::check())
        {
            return View::make('userSearchBuy')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
        else
        {
            return View::make('searchBuy')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
    }

    public function filterSearchRent()
    {
        $val = Input::get('search');
        $min = Input::get('min_price');
        $max = Input::get('max_price');
        $bed = Input::get('bed');
        $bath = Input::get('bath');
        $car = Input::get('car');
        $searchTerms = explode(' ', $val);

        // $properties = DB::table('properties')->where('choice', 1);

        // $property->whereBetween('price', array($min, $max));

        $property = Property::with('photos','landlord')->where('showcase',1);

         foreach($searchTerms as $term)
         {
             $property->where('address', 'LIKE', '%'. $term .'%');
         }

        $property->where('choice', 0);

        if($min != "" && $max != ""){
        $property->whereBetween('rent', array($min, $max));
        }

        if($car != "")
        {
            $property->where('parking','>=',$car);
        }

        if($bed != ""){
        $property->where('bedrooms','>=',$bed);
            //return $property->toArray();
        }

        if($bath != ""){
            $property->where('bathrooms','>=',$bath);
        }

        //$property->where('price','>',$min)->where('price','<',$max);

        $properties = $property->orderBy('created_at','desc')->paginate(9);


      // dd($properties);
        if(Auth::check())
        {
            return View::make('userSearchRent')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
        else
        {
            return View::make('searchRent')->withProperties($properties)->with('propertiesArray',$properties->toArray());
        }
    }

    public function ratings($id)
    {
        $rate = Input::get('rate');
        $val= Input::get('val');
        $property_id = $id;
        if(Auth::check())
        {
            $userRate = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->first();
            switch ($val) {
            case '0':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('overall'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'overall' => $rate));
                }
                break;
            case '1':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('safety'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'safety' => $rate));
                }
                break;
            case '2':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('pet_friendly'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'pet_friendly' => $rate));
                }
                break;
            case '3':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('walkability'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'walkability' => $rate));
                }
                break;
            case '4':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('restaurants'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'restaurants' => $rate));
                }
                break;
            case '5':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('parks'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'parks' => $rate));
                }
                break;
            case '6':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('schools'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'schools' => $rate));
                }
                break;
            case '7':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('traffic'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'traffic' => $rate));
                }
                break;
            case '8':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('parking'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'parking' => $rate));
                }
                break;
            case '9':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('entertainment'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'entertainment' => $rate));
                }
                break;
            case '10':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('transportation'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'transportation' => $rate));
                }
                break;
            case '11':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('community'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'community' => $rate));
                }
                break;
            case '12':
                if($userRate)
                {
                    $update = Rating::where('user_id',Auth::user()->id)->where('property_id',$property_id)->update(array('cleanliness'=>$rate));
                }
                else
                {
                    $insert = Rating::create(array('user_id' => Auth::user()->id,
                                                    'property_id' => $property_id,
                                                    'cleanliness' => $rate));
                }
                break;
            
            default:
                # code...
                break;
        }
        }

        return Response::json(array('success' => true, 'msg' => "$id is $rate and $val"));
    }

    public function review($id)
    {
        if(Auth::check())
        {
            $userReview = Rating::where('user_id',Auth::user()->id)->where('property_id',$id)->first();
            if($userReview)
            {
                $review = Rating::where('user_id',Auth::user()->id)->where('property_id',$id)->update(array('review_title' => Input::get('review_title'),
                                                                                                    'description' => Input::get('description'),
                                                                                                    'know_by' => Input::get('place')));
                return Redirect::back()->with('msg',"Review Added Successfully");
            }
            else
            {
                $review = Rating::create(array('review_title' => Input::get('review_title'),
                                                'description' => Input::get('description'),
                                                'know_by' => Input::get('place')));
                return Redirect::back()->with('msg',"Review Added Successfully");
            }
        }
        else
        {
            return Redirect::back()->with('msg',"Please login to do this");
        }
    }

    public function install()
    {
        return View::make('install');
    }

      public function installationProcess()
    {
        $admin_username = Input::get('username');
        $admin_email = Input::get('email');
        $admin_password = Input::get('password');
        $mandrill_username = Input::get('mandrill_username');
        $mandrill_password = Input::get('mandrill_password');
        $database_name = Input::get('database_name');
        $database_user_name = Input::get('database_user_name');
        $database_password = Input::get('database_password');
    

        $validator = Validator::make(
            array(

                'username' => $admin_username,
                'password' => $admin_password,
                'email' => $admin_email,
                'mandrill_username' => $mandrill_username,
                'mandrill_password' => $mandrill_password,
                'database_name' => $database_name,
                'database_username' => $database_user_name,
                'database_password' => $database_password
                
                
            ), array(
                'username' => 'required',
                'password' => 'required',
                'email' => 'required',
                'mandrill_username' => 'required',
                'mandrill_password' => 'required',
                'database_name' => 'required',
                'database_username' => 'required',
                'database_password' => 'required'
            
            )
        );

        if ($validator->fails())
        {
            $error_messages = $validator->messages()->all();
            return Redirect::back()->with('flash_errors',$error_messages);
        }
        else
        {
            
            $mysql_username=$database_user_name;
            $mysql_password=$database_password;
            $mysql_host='localhost';
            $mysql_database=$database_name;

    $filename = public_path().'/db/roomy-new.sql';
    

    // Connect to MySQL server
    $db_conn = mysqli_connect($mysql_host, $mysql_username, $mysql_password,$mysql_database) or die('Error connecting to MySQL server: ' . mysql_error());
    // Select database
    //mysql_select_db($mysql_database) or die('Error selecting MySQL database: ' . mysql_error());

    // Temporary variable, used to store current query
    $templine = '';
    // Read in entire file
    $lines = file($filename);
    // Loop through each line
    foreach ($lines as $line)
    {
    // Skip it if it's a comment
    if (substr($line, 0, 2) == '--' || $line == '')
        continue;

    // Add this line to the current segment
    $templine .= $line;
    // If it has a semicolon at the end, it's the end of the query
    if (substr(trim($line), -1, 1) == ';')
    {
        // Perform the query
        mysqli_query($db_conn,$templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
        // Reset temp variable to empty
        $templine = '';
    }
    }

    //insert setting JSON

     
    Setting::set('mandrill_username',$mandrill_username);
    Setting::set('mandrill_password',$mandrill_password);
    Setting::set('database_name',$database_name);
    Setting::set('database_username',$database_user_name);
    Setting::set('database_password',$database_password);

    //insert admin
    
                 
    $user = new User;
    $user->first_name=$admin_username;
    $user->email =$admin_email;
    $user->password =Hash::make(Input::get('password'));
    $user->role_id = 3;
    $user->is_activated =1;
    $user->save();
    }

        return Redirect::to('/');
    }


}