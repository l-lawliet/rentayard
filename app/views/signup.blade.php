@extends('layout')

@section('title','Sign Up')

@section('content')


<!-- row-app -->
<div class="row row-app">

    <!-- col -->


    <!-- col-separator.box -->
    <div class="col-separator col-unscrollable box">

        <!-- col-table -->
        <div class="col-table">



            <!-- col-table-row -->
            <div class="col-table-row">

                <!-- col-app -->
                <div class="col-app col-unscrollable">

                    <!-- col-app -->
                    <div class="col-app">



                        <div class="login">

                            <div class="placeholder text-center"><i class="fa fa-home"></i></div>

                            <div class="panel-default col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                                <div class="panel-heading">
                                    Create New Account
                                </div>
                                <div class="panel-body">
                                    @include('partials.notifications')
                                    <form role="form" action="{{URL::route('processSignup')}}" method="post">
                                        <div class="form-group">
                                            <label>You are a ...</label>
                                            <select name="role" class="form-control">

                                                <?php

                                                    $count = User::where('role_id',3)->count();


                                                ?>

                                                @if(!$count)
                                                    <option value="admin">Admin ( one time ) </option>
                                                @endif
                                                <option value="landlord">Host (Free)</option>
                                                <option value="renter">Guest (Free)</option>


                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label> Your Name </label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="First Name" name="first_name">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" placeholder="Enter email" name="email">
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" placeholder="Password" name="password">
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block">Create Account</button>
                                        <a href="{{URL::Route('login')}}">{{trans('user.alreadyRegistered')}}</a>
                                    </form>
                                </div>

                            </div>
                            <div class="clearfix"></div>

                        </div>

                    </div>
                    <!-- // END col-app -->

                </div>
                <!-- // END col-app.col-unscrollable -->

            </div>
            <!-- // END col-table-row -->

        </div>
        <!-- // END col-table -->

    </div>
    <!-- // END col-separator.box -->


</div>
<!-- // END row-app -->



@stop
