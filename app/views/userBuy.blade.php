@extends('layoutUser')

@section('content')

<div class="row home-search-bg">
    <div class="container">
<br>
        <br>
        <h2 class="text-center">Roomy,Your Home for Real Estate.</h2>   
        <br> 
        <form class="home-search" action="{{URL::route('search')}}" method="get">
            <div class="col-md-2 col-sm-2">
                <select class="form-control" style="padding: 14px 12px;" name="type">
                    <option value="buy" selected>Buy</option>
                    <option value="rent">Rent</option>
                </select>
            </div>
                <div class="col-md-8 col-sm-8">
                <input class="form-control" type="text" id="search-address" required autocomplete="off" name="search" placeholder="search using address" />
                </div>
                <div class="col-md-2 col-sm-2">
                <button class="form-control" type="submit">Search</button>
                </div>
        </form>
        <br><br><br><br><br>
    </div>
</div>
<br><br>

<div class="container">

    <div class="row">

    @foreach($propertiesArray['data'] as $property)
 
        <div class="col-md-4">
            <a href="{{URL::route('viewProperty',$property['id'])}}">
            <div class="widget">
                <div class="timeline-cover">
                    <div class="cover image ">
                        <div class="top">
                            @if(count($property['photos']))
                                <img src="{{ asset('/uploads/property_photos/'.$property['photos'][0]['filename'])}}" class="img-responsive">
                            @else
                                <img src="{{ asset('/uploads/default_property_photo.jpg')}}" class="img-responsive">
                            @endif
                        </div>
                    </div>
                    <div class="widget cover image">
                        <div class="widget-body padding-none margin-none">
                            <div class="photo">
                                @if($property['landlord']['profile_picture'])
                                <img src="{{asset('/uploads/'.$property['landlord']['profile_picture'])}}" style="width:100px;height:100px;" class="img-circle">
                                @else
                                <img src="{{asset('uploads/default.jpg')}}" style="width:100px;height:100px;" class="img-circle">
                                @endif 
                            </div>
                            <div class="innerAll pull-left">
                                <p class="lead margin-none "> <i class="fa fa-home text-muted fa-fw"></i> {{ucfirst($property['name'])}} </p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="text-center innerAll">
                    <p class="lead margin-none "> <i class="fa fa-location-arrow text-muted fa-fw"></i> {{ucfirst($property['city'])}}, {{ucfirst($property['state'])}}, {{$property['zip']}} </p>
                    @if($property['rent'] > 0)
                    <p class="lead margin-none price">  Rent : $ {{$property['rent']}} </p>
                    <p>For rent</p>
                    @else
                    <p class="lead margin-none price">  Price : $ {{$property['price']}} </p>
                    <p>For sale</p>
                    @endif
                </div>


            </div>
            </a>
        </div>
    @endforeach

    </div>

    {{$properties->links()}}
</div>
<br>
<br>
<div class="row footer">
    <div class="container">
        <center>
            @include('footer')
        </center>
    </div>
</div>
@stop