@extends('layout')

@section('title','Roomy')
 
@section('content')

<div class="row home-search-bg">
    <div class="container">
<br>
        <br>
        <h2 class="text-center">&nbspRentaYawd locally across the island.</h2>   
        <br> 
        <form class="home-search" action="{{URL::route('search')}}" method="get">
            <div class="col-md-2 col-sm-2">
                <select class="form-control" style="display: none; padding: 14px 12px;" name="type">
                    <!-- <option value="buy" selected>Buy</option> -->
                    <option value="rent">Rent</option>
                </select>
            </div>
                <div class="col-md-8 col-sm-8">
                <input class="form-control" type="text" required autocomplete="off" id="search-address" name="search" placeholder="search using address" />
                </div>
                <div class="col-md-2 col-sm-2">
                <button class="form-control" type="submit">Search</button>
                </div>
        </form>
        <br><br>

        <div class="text-center">
        <a class="btn btn-success btn-lg" href="{{URL::to('signup')}}"> Join Now </a>
        </div>
        
        <br><br><br>
    </div>
</div>
<br><br>

<div class="row home-service text-center">
    <div class="container">

    <div class="col-md-4">
        <h3>What will you get?</h3>
        <br>
        <img class="roundPic" src="{{asset('assets/images/ser1.gif')}}">
        <br>  <br>  <br> 
        
        <p>A chance to interact with the best host for guest across the island.</p>

    </div>

    <div class="col-md-4">
        <h3>Moving?</h3>
        <br>
        <img class="roundPic" src="{{asset('assets/images/ser2.gif')}}">
        <br> <br>  <br> 
        <p>We make relocating easier. We provide the best and most suitable home rents from our listed hosts</p>
        
    </div>

    <div class="col-md-4">
        <h3>What’s your home worth?</h3>
        <br>
        <img class="roundPic" src="{{asset('assets/images/ser3.gif')}}">
        <br> <br>  <br> 
        <p>Instantly find out your home’s value and connect with guest from accross the island who in need of your service.</p>
        
    </div>


    </div>
    <br><br><br>
</div>

<!--
<div class="row home-find">
    <div class="container">

        <div class="col-md-6">
        <br><br><br>
        <h1>Find homes on the go</h1>
        <br>
        <p>Roomly's top-rated mobile app shows homes for sale near you—anytime, anywhere. With hi-res photo galleries and detailed neighborhood maps, your future home will be at your fingertips.</p>
        <br>
        <p>Exploring homes! find right choice for right people. Ultimater destination for home seekers, the most convenient, efficent and faster means than ever. </p>
        <br>
        <p>Searching property within your budget was never so easy unless without the best solution provider for home seekers. Buying a home is now a hazel free with roomy.  Your future dream home is just at your fingertips.</p>
        <br>
        </div>
        <div class="col-md-6">
            <br><br><br>
        <img src="{{asset('assets/images/mobile_devices_v4.png')}}" style="width:80%;">
        </div>

    </div>

<br><br><br>
</div>
-->

<!-- <div class="row home-afford text-center">
    <div class="container">
    <br><br>
        <h1>How much can you afford?</h1>
        <h3><strong>Renting a home is a big deal. We can help you figure out how much money you have to work with and bring clarity to the process.</strong></h3>
        <br><br>
        <img src="{{asset('assets/images/mortgage_illustration_v3.png')}}" >
        <br><br><br><br>
<!-- <a class="button" href="#first-step">TAKE THE FIRST STEP</a> 
        <br><br><br><br>
    </div>
</div> -->

<div class="row bef-footer">
    <div class="container">

    <div class="col-md-6">
    <br><br>
        <h2>Get the best local prices</h2>
        <p>From neighborhood insights when you start your searching for rent, our website is here to provide the gateway for many of you who think that finding the
            right place locally is hard. Check out these convenient places on RentaYawd.</p>
    <br><br>
    </div>
    <div class="col-md-6">
    <br><br>
        <h2>Want to host your home for rent?</h2>
        <p>There's a lot to think about when hosting, and we're here to help. Check out these tips and tricks for host, courtesy of RentaYawd community.<p>
    <br><br>
    </div>

    </div>
</div>

<div class="row footer">
    <div class="container">
@include('footer')
    </div>
</div>



@stop
