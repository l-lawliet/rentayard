@extends('layout.userSearchLayout')

@section('content')

<div class="row home-search-bg">
    <div class="container">
<br>
        <br>
        <h2 class="text-center">Roomy,Your Home for Real Estate.</h2>   
        <br> 
        <form class="home-search" action="{{URL::route('search')}}" method="get">
            <div class="col-md-2 col-sm-2">
                <select class="form-control" style="padding: 14px 12px;" name="type">
                    <option value="buy" selected>Buy</option>
                    <option value="rent">Rent</option>
                </select>
            </div>
                <div class="col-md-8 col-sm-8">
                <input class="form-control" type="text" required id="search-address" autocomplete="off" name="search" placeholder="search using address" />
                </div>
                <div class="col-md-2 col-sm-2">
                <button class="form-control" type="submit">Search</button>
                </div>
        </form>
        <br><br>
        <div class="text-center">
        <a class="btn btn-success btn-lg" href="{{URL::Route('saveSearch')}}"> Save Search </a>
        </div>
        <br><br><br>
    </div>
</div>
<br><br>

<div class="container">

    <br>

     @if(Session::has('msg'))
 <div class="alert alert-info alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('msg')}}
</div>
  @endif
 

    <div class ="row">
        <div class="widget widget-gallery" data-toggle="collapse-widget">
            <div class="widget-head"><h4 class="heading">Filtered search</h4></div>
            <div class="widget-body">
                <form action="{{URL::Route('filterSearchBuy')}}" method="get">
                    <input type="hidden" value="<?php if(isset($_GET['search'])){ echo $_GET['search'];} ?>" name="search"/>
                    <select name="min_price" id="min_price">
                        <option value="" selected="selected">Min Price</option>
                        <option value="100">$100</option>
                        <option value="200">$200</option>
                        <option value="300">$300</option>
                        <option value="400">$400</option>
                        <option value="500">$500</option>
                        <option value="600">$600</option>
                        <option value="700">$700</option>
                    </select>
                    <select name="max_price">
                        <option value="" selected="selected">Max Price</option>
                        <option value="1000">$1000</option>
                        <option value="1500">$1500</option>
                        <option value="2000">$2000</option>
                        <option value="2500">$2500</option>
                        <option value="3000">$3000</option>
                        <option value="3500">$3500</option>
                        <option value="4000">$4000</option>
                    </select>
                    <select name="bath" id="bath">
                        <option value="" selected="selected">All Baths</option>
                        <option value="1">0+</option>
                        <option value="2">1+</option>
                        <option value="3">2+</option>
                        <option value="4">3+</option>
                        <option value="5">4+</option>
                        <option value="6">5+</option>
                        <option value="7">6+</option>
                    </select>
                    <select name="bed" id="bed">
                        <option value="" selected="selected">All Beds</option>
                        <option value="1">0+</option>
                        <option value="2">1+</option>
                        <option value="3">2+</option>
                        <option value="4">3+</option>
                        <option value="5">4+</option>
                        <option value="6">5+</option>
                        <option value="7">6+</option>
                    </select>
                    <select name="car" id="car">
                        <option value="" selected="selected">All Car Parkings</option>
                        <option value="1">0+</option>
                        <option value="2">1+</option>
                        <option value="3">2+</option>
                        <option value="4">3+</option>
                        <option value="5">4+</option>
                        <option value="6">5+</option>
                        <option value="7">6+</option>
                    </select>
                    <button type="submit" class="btn btn-success pull-right" style="height: 41px; position: relative;
  top: -2px;"> Filter </button>
                </form>
            </div>
        </div>
    </div>
    <div class="row">

    @foreach($propertiesArray['data'] as $property)
 
        <div class="col-md-4">
            <a href="{{URL::route('viewProperty',$property['id'])}}">
            <div class="widget">
                <div class="timeline-cover">
                    <div class="cover image ">
                        <div class="top">
                            @if(count($property['photos']))
                                <img src="{{ asset('/uploads/property_photos/'.$property['photos'][0]['filename'])}}" class="img-responsive">
                            @else
                                <img src="{{ asset('/uploads/default_property_photo.jpg')}}" class="img-responsive">
                            @endif
                        </div>
                    </div>
                    <div class="widget cover image">
                        <div class="widget-body padding-none margin-none">
                            <div class="photo">
                                @if($property['landlord']['profile_picture'])
                                <img src="{{asset('/uploads/'.$property['landlord']['profile_picture'])}}" style="width:100px;height:100px;" class="img-circle">
                                @else
                                <img src="{{asset('uploads/default.jpg')}}" style="width:100px;height:100px;" class="img-circle">
                                @endif 
                        </div>
                            <div class="innerAll pull-left">
                                <p class="lead margin-none "> <i class="fa fa-home text-muted fa-fw"></i> {{ucfirst($property['name'])}} </p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="text-center innerAll">
                    <p class="lead margin-none "> <i class="fa fa-location-arrow text-muted fa-fw"></i> {{ucfirst($property['city'])}}, {{ucfirst($property['state'])}}, {{$property['zip']}} </p>
                    @if($property['rent'] > 0)
                    <p class="lead margin-none price">  Rent : $ {{$property['rent']}} </p>
                    <p>For rent</p>
                    @else
                    <p class="lead margin-none price">  Price : $ {{$property['price']}} </p>
                    <p>For sale</p>
                    @endif
                </div>


            </div>
            </a>
        </div>
    @endforeach

    </div>

    {{$properties->links()}}
</div>
<br>
<br>
<div class="row footer">
    <div class="container">
        <center>
            {{Setting::get('footer')}}
        </center>
    </div>
</div>
@stop