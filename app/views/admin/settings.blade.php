@extends('admin.layout')

@section('content')

<div class="col-md-8 col-md-offset-2">
<div class="widget">
    <div class="widget-head">
        <h4 class="heading">{{trans('admin.settings')}}</h4>
    </div>
    <div class="widget-body innerAll">
        <form class="form-horizontal" action="{{URL::Route('updateSettings')}}" method="post" enctype="multipart/form-data">
        <div class="row innerLR">
            <div class="col-md-8 col-md-offset-1">
                <div class="form-group">
                    <label for="sitename" class="control-label col-md-4">{{trans('admin.sitename')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="sitename" id="sitename" value="{{Setting::get('sitename')}}"/>
                        </div>
                </div>
                <div class="form-group">
                    <label for="footer" class="control-label col-md-4">{{trans('admin.footer')}}</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" name="footer" value="{{Setting::get('footer')}}"/>
                    </div>
                </div>
                <div class="form-group">
                <label for="footer" class="control-label col-md-4">Choose Logo</label>
                    <div class="col-md-8" >
                        <input value="{{Setting::get('logo')}}" class="form-control" type="file" name="logo" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="subscription_amount" class="control-label col-md-4">{{trans('admin.subscription_amount')}}</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" name="subscription_amount" id="subscription_amount" value="{{Setting::get('subscription_amount')}}"/>
                    </div>
                </div>
                <div class="col-md-6 col-md-offset-4">
                    <button class="btn btn-success" type="Submit">{{trans('user.save')}}</button>
                    <a href="{{URL::previous()}}" class="btn btn-danger btn-stroke">{{trans('user.cancel')}}</a>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
@stop