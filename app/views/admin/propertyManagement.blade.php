@extends('admin.layout')

@section('content')

<div class="widget">
	<div class="widget-head">
		<h4 class="heading">{{trans('admin.propertyManagement')}}</h4>
	</div>
	<div class="widget-body innerAll inner-2x">

		<!-- Table -->
		<table class="footable table table-striped table-primary tablet breakpoint footable-loaded">

			<!-- Table heading -->
			<thead>
				<tr>
					<th data-class="expand" class="footable-first-column">Name</th>
					<th data-hide="phone">City</th>
					<th data-hide="phone">Rent/Price</th>
					<th class="footable-last-column">Action</th>
				</tr>
			</thead>
			<!-- // Table heading END -->

			<!-- Table body -->
			<tbody>
            @foreach($properties as $property)
				<!-- Table row -->
				<tr class="gradeX">
					@if($property->rent > 0)
					<td class="expand footable-first-column">{{$property->name}}(for rent)</td>
					@else
					<td class="expand footable-first-column">{{$property->name}}(for sale)</td>
					@endif
					<td>{{$property->city}}</td>
					<td>
						@if($property->rent > 0)
                        $ {{$property->rent}}
                        @else
                        $ {{$property->price}}
                        @endif
					</td>
					<td class="footable-last-column">
						<a class="btn btn-danger btn-sm" href="{{URL::Route('deleteProperty',array('id'=>$property->id))}}" onclick="return confirm('Are you sure to delete?')"> Delete Property </a>
						<a class="btn btn-primary btn-sm" href="{{URL::Route('viewProperty',array('id'=>$property->id))}}"> View Property </a>

						@if($property->rent > 0)
					<a class="btn btn-primary btn-sm" href="{{URL::Route('adminAddPropertyRent',array('id'=>$property->user_id))}}"> Add Property Rent </a>
					@else
					<a class="btn btn-primary btn-sm" href="{{URL::Route('adminAddPropertySale',array('id'=>$property->user_id))}}"> Add Property Sale </a>
					@endif
					<a class="btn btn-primary btn-sm" href="{{URL::route('AdminViewProperty',array('id'=>$property->id))}}"> Edit Property </a>
					</td>
				</tr>
				<!-- // Table row END -->
            @endforeach


			</tbody>
			<!-- // Table body END -->

		</table>
		<!-- // Table END -->

		<center>
			{{$properties->links()}}
		</center>

	</div>
</div>

@stop