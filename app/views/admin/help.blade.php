@extends('admin.layout')

@section('content')

<div class="widget">
	<div class="widget-head">
		<h4 class="heading">{{trans('admin.help')}}</h4>
	</div>
	<div class="widget-body innerAll inner-2x">

	  <div class="col-md-12">
            <table class="table table-striped table-responsive swipe-horizontal table-primary">

                <!-- Table heading -->
                <thead>
                    <tr>
                        <th> Name </th>
                        <th> Email</th>
                        <th> Title </th>
                        <th> message</th>
                        <th> Status</th>
                        <th> Action</th>
                    </tr>
                </thead>
                <!-- // Table heading END -->

                <!-- Table body -->
                <tbody>
                @foreach($help as $help_all)
                <tr class="gradeX">
                     
 					<td class="expand footable-first-column" style="width:10%;"><?php echo $help_all->first_name;?></td>
                    <td class="expand footable-first-column" style="width:10%;"><?php echo $help_all->email?></td>
                    <td class="expand footable-first-column" style="width:20%;"><?php echo $help_all->title?></td>
                    <td class="expand footable-first-column" style="width:25%;"><?php echo $help_all->message?></td>
                   <td> @if($help_all->status == 0)
                         <label class="btn btn-warning btn-sm" >Pending</label>
                    @else
                        <label class="btn btn-success btn-sm" >Resolved</label>
                     @endif
                    
                    </td>

                    <td> @if($help_all->status == 0)
                         <a href="{{URL::route('resolvedHelp',array('id'=>$help_all->id))}}" class="btn btn-info btn-sm" >Make Resolved</a>
                    @else
                         <label class="btn btn-success btn-sm" >Already solved</label>
                     @endif
                    
                    </td>
                </tr>   
               @endforeach
               </tbody>
               </table>
               
                </div>
        </div>
		
	</div>
</div>

@stop