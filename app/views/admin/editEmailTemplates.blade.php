@extends('admin.layout')

@section('content')

<div class="col-md-12">
<div class="widget">
    <div class="widget-head">
        <h4 class="heading">{{trans('admin.editEmailTemplates')}}</h4>
    </div>
    <div class="widget-body innerAll">
        <form class="form-horizontal" action="{{URL::Route('updateEmailTemplates')}}" method="post">
        <div class="row innerLR">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="renterApplicationRequest" class="control-label col-md-3">{{trans('admin.renterApplicationRequest')}}</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="renterApplicationRequest" id="renterApplicationRequest">{{(Setting::has('email.renterApplicationRequest'))?Setting::get('email.renterApplicationRequest'):''}}</textarea>
                        </div>
                </div>
                <div class="form-group">
                    <label for="landlordApplicationConfirmation" class="control-label col-md-3">{{trans('admin.landlordApplicationConfirmation')}}</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="landlordApplicationConfirmation" id="landlordApplicationConfirmation">{{(Setting::has('email.landlordApplicationConfirmation'))?Setting::get('email.landlordApplicationConfirmation'):''}}</textarea>
                        </div>
                </div>
                <div class="form-group">
                    <label for="coapplicantInvite" class="control-label col-md-3">{{trans('admin.coapplicantInvite')}}</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="coapplicantInvite" id="coapplicantInvite">{{(Setting::has('email.coapplicantInvite'))?Setting::get('email.coapplicantInvite'):''}}</textarea>
                        </div>
                </div>
                <div class="form-group">
                    <label for="rentReminder" class="control-label col-md-3">{{trans('admin.rentReminder')}}</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="rentReminder" id="rentReminder">{{(Setting::has('email.rentReminder'))?Setting::get('email.rentReminder'):''}}</textarea>
                        </div>
                </div>
                <div class="form-group">
                    <label for="activationMail" class="control-label col-md-3">{{trans('admin.activationMail')}}</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="activationMail" id="activationMail">{{(Setting::has('email.activationMail'))?Setting::get('email.activationMail'):''}}</textarea>
                        </div>
                </div>
                <div class="form-group">
                    <label for="applicationRejection" class="control-label col-md-3">{{trans('admin.applicationRejection')}}</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="applicationRejection" id="applicationRejection">{{(Setting::has('email.applicationRejection'))?Setting::get('email.applicationRejection'):''}}</textarea>
                        </div>
                </div>
                <div class="col-md-6 col-md-offset-4">
                    <button class="btn btn-success" type="Submit">{{trans('user.save')}}</button>
                    <a href="{{URL::previous()}}" class="btn btn-danger btn-stroke">{{trans('user.cancel')}}</a>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
@stop