@extends('admin.layout')

@section('title',trans('user.editProfile'))

@section('content')

<div class="widget" id="editProfile">
    <div class="widget-head">
        <h4 class="heading">{{trans('user.editProfile')}}</h4>
    </div>
    <div class="widget-body innerAll">

        <form class="form-horizontal" method="post" enctype="multipart/form-data">
            <div class="row innerLR">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name" class="control-label col-md-4">{{trans('user.first_name')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="first_name" id="first_name" value="{{$user->first_name}}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="last_name" class="control-label col-md-4">{{trans('user.last_name')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="last_name" id="last_name" value="{{$user->last_name}}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone" class="control-label col-md-4">{{trans('user.phone')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="phone" id="phone" value="{{$user->phone}}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="salary" class="control-label col-md-4">{{trans('user.monthly_salary')}}</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" type="text" name="salary" id="salary" value="{{$user->salary}}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="designation" class="control-label col-md-4">{{trans('user.designation')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="designation" id="designation" value="{{$user->designation}}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="pets" class="control-label col-md-4">{{trans('user.pets')}}</label>
                        <div class="col-md-8">
                            <select multiple="multiple" name="pets[]" style="width: 100%;" id="pets">
                                <option value="cat" {{(in_array('cat',$user->pets))?'selected':''}} >Cat</option>
                                <option value="dog" {{(in_array('dog',$user->pets))?'selected':''}} >Dog</option>
                                <option value="fish" {{(in_array('fish',$user->pets))?'selected':''}} >Fish</option>
                                <option value="reptile" {{(in_array('reptile',$user->pets))?'selected':''}} >Reptile</option>
                                <option value="other" {{(in_array('other',$user->pets))?'selected':''}} >Other</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="about" class="control-label col-md-4">{{trans('user.about_yourself')}}</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="about" id="about">{{$user->about}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="address" class="control-label col-md-4">{{trans('user.address')}}</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="address" id="address">{{$user->address}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <h5 class="innerT"> {{trans('user.rentalHistory')}} </h5>
            <div id="rentalHistory">
            @if($user->rental_history!=NULL)
                @foreach($user->rental_history as $rental)
                <div class="widget bg-gray innerAll margin-none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="houseAddress[]" class="control-label col-md-2">
                                    {{trans('user.address')}}
                                </label>
                                <div class="col-md-10">
                                    <input class="form-control" name="houseAddress[]" id="houseAddress[]" value="{{$rental->address}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseRent[]" class="control-label col-md-4">
                                    {{trans('user.rent')}}
                                </label>
                                <div class="input-group">
                                      <span class="input-group-addon">
                                        $
                                      </span>
                                    <input class="form-control" type="text" name="houseRent[]" id="houseRent[]" value="{{$rental->rent}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseTenancyLength[]" class="control-label col-md-4">
                                    {{trans('user.lengthOfTenancy')}}
                                </label>
                                <div class="input-group">
                                    <input class="form-control" type="text" name="houseTenancyLength[]" id="houseTenancyLength[]" value="{{$rental->tenancyLength}}"/>
                                      <span class="input-group-addon">
                                        years
                                      </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseLandlordFirstName[]" class="control-label col-md-4">
                                    {{trans('user.landlordFirstName')}}
                                </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="houseLandlordFirstName[]" id="houseLandlordFirstName[]" value="{{$rental->landlordFirstName}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseLandlordLastName[]" class="control-label col-md-4">
                                    {{trans('user.landlordLastName')}}
                                </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="houseLandlordLastName[]" id="houseLandlordLastName[]" value="{{$rental->landlordLastName}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseLandlordPhone[]" class="control-label col-md-4">
                                    {{trans('user.landlordPhone')}}
                                </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="houseLandlordPhone[]" id="houseLandlordPhone[]" value="{{$rental->landlordPhone}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseLandlordEmail[]" class="control-label col-md-4">
                                    {{trans('user.landlordEmail')}}
                                </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="email" name="houseLandlordEmail[]" id="houseLandlordEmail[]" value="{{$rental->landlordEmail}}"/>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-danger btn-block" type="button" onclick="deleteRental(this)">
                            {{trans('user.deleteThisRental')}}
                        </button>
                    </div>
                </div>
                @endforeach
            @endif
            </div>
            <button class="btn btn-success btn-sm addRental" type="button">{{trans('user.addRental')}}</button>

            <h5 class="innerT"> {{trans('user.workHistory')}} </h5>
            <div id="workHistory">
            @if($user->work_history!=NULL)
                @foreach($user->work_history as $work)
                <div class="widget bg-gray innerAll margin-none">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="workDesignation[]" class="control-label col-md-4">{{trans('user.designation')}}</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="workDesignation[]" id="workDesignation[]" value="{{$work->designation}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="workCompany[]" class="control-label col-md-4">{{trans('user.company')}}</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="workCompany[]" id="workCompany[]" value="{{$work->company}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="workSalary[]" class="control-label col-md-4">{{trans('user.monthly_salary')}}</label>
                                <div class="input-group"><span class="input-group-addon">$</span>
                                    <input class="form-control" type="text" name="workSalary[]" id="workSalary[]" value="{{$work->salary}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="workEmploymentLength[]" class="control-label col-md-4">{{trans('user.lengthOfEmployement')}}</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" name="workEmploymentLength[]" id="workEmploymentLength[]" value="{{$work->employmentLength}}" /><span class="input-group-addon">years</span>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-danger btn-block" type="button" onclick="deleteWork(this)">{{trans('user.deleteThisWork')}}</button>
                    </div>
                </div>
                @endforeach
            @endif
            </div>
            <button class="btn btn-success btn-sm addWork" type="button">{{trans('user.addWork')}}</button>


            <h5 class="innerT"> {{trans('user.incomeSources')}} </h5>
            <div id="incomeSources">
                @if($user->additional_income!=NULL)
                @foreach($user->additional_income as $income)
                <div class="widget bg-gray innerAll margin-none">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="incomeSource[]" class="control-label col-md-4">{{trans('user.incomeSource')}}</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="incomeSource[]" id="incomeSource[]" value="{{$income->source}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="incomeAmount[]" class="control-label col-md-4">{{trans('user.amount')}}</label>
                                <div class="input-group"><span class="input-group-addon">$</span>
                                    <input class="form-control" type="text" name="incomeAmount[]" id="incomeAmount[]" value="{{$income->amount}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 pull-right col-md-offset-1">
                            <button class="btn btn-danger" type="button" onclick="deleteIncome(this)">{{trans('user.delete')}}</button>
                        </div>

                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <button class="btn btn-success btn-sm addIncome" type="button">{{trans('user.addIncomeSource')}}</button>


            <div class="row">
                <div class="center">
                    <button class="btn btn-success" type="Submit">{{trans('user.updateProfile')}}</button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('script')

    <script src="{{asset('assets/plugins/forms_elements_select2/js/select2.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script>
        $("#pets").select2({
            placeholder: "Select the Pets",
            allowClear: true
        });


        var rentalHistory = '<div class="widget bg-gray innerAll margin-none"><div class="row"><div class="col-md-12"><div class="form-group"><label for="houseAddress[]" class="control-label col-md-2">{{trans('user.address')}}</label><div class="col-md-10"><input class="form-control" name="houseAddress[]" id="houseAddress[]"/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseRent[]" class="control-label col-md-4">{{trans('user.rent')}}</label><div class="input-group"><span class="input-group-addon">$</span><input class="form-control" type="text" name="houseRent[]" id="houseRent[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseTenancyLength[]" class="control-label col-md-4">{{trans('user.lengthOfTenancy')}}</label><div class="input-group"><input class="form-control" type="text" name="houseTenancyLength[]" id="houseTenancyLength[]" value=""/><span class="input-group-addon">years</span></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordFirstName[]" class="control-label col-md-4">{{trans('user.landlordFirstName')}}</label><div class="col-md-8"><input class="form-control" type="text" name="houseLandlordFirstName[]" id="houseLandlordFirstName[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordLastName[]" class="control-label col-md-4">{{trans('user.landlordLastName')}}</label><div class="col-md-8"><input class="form-control" type="text" name="houseLandlordLastName[]" id="houseLandlordLastName[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordPhone[]" class="control-label col-md-4">{{trans('user.landlordPhone')}}</label><div class="col-md-8"><input class="form-control" type="text" name="houseLandlordPhone[]" id="houseLandlordPhone[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordEmail[]" class="control-label col-md-4">{{trans('user.landlordEmail')}}</label><div class="col-md-8"><input class="form-control" type="email" name="houseLandlordEmail[]" id="houseLandlordEmail[]" value=""/></div></div></div><button class="btn btn-danger btn-block" type="button" onclick="deleteRental(this)"> {{trans('user.deleteThisRental')}} </button></div></div>';

        function deleteRental(e){
            $(e).parent().parent().remove();
        }

        $('.addRental').on('click',function(){
            $('#rentalHistory').append(rentalHistory);
        });

        var workHistory = '<div class="widget bg-gray innerAll margin-none"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label for="workDesignation[]" class="control-label col-md-4">{{trans('user.designation')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="workDesignation[]" id="workDesignation[]" value="" /> </div> </div> </div> <div class="col-md-6"> <div class="form-group"> <label for="workCompany[]" class="control-label col-md-4">{{trans('user.company')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="workCompany[]" id="workCompany[]" value="" /> </div> </div> </div> <div class="col-md-6"> <div class="form-group"> <label for="workSalary[]" class="control-label col-md-4">{{trans('user.monthly_salary')}}</label> <div class="input-group"><span class="input-group-addon">$</span> <input class="form-control" type="text" name="workSalary[]" id="workSalary[]" value="" /> </div> </div> </div> <div class="col-md-6"> <div class="form-group"> <label for="workEmploymentLength[]" class="control-label col-md-4">{{trans('user.lengthOfEmployement')}}</label> <div class="input-group"> <input class="form-control" type="text" name="workEmploymentLength[]" id="workEmploymentLength[]" value="" /><span class="input-group-addon">years</span> </div> </div> </div> <button class="btn btn-danger btn-block" type="button" onclick="deleteWork(this)">{{trans('user.deleteThisWork')}}</button> </div> </div>';

        function deleteWork(e){
            $(e).parent().parent().remove();
        }

        $('.addWork').on('click',function(){
            $('#workHistory').append(workHistory);
        });

        var income = '<div class="widget bg-gray innerAll margin-none"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label for="incomeSource[]" class="control-label col-md-4">{{trans('user.incomeSource')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="incomeSource[]" id="incomeSource[]" value="" /> </div> </div> </div> <div class="col-md-3"> <div class="form-group"> <label for="incomeAmount[]" class="control-label col-md-4">{{trans('user.amount')}}</label> <div class="input-group"><span class="input-group-addon">$</span> <input class="form-control" type="text" name="incomeAmount[]" id="incomeAmount[]" value="" /> </div> </div> </div> <div class="col-md-2 pull-right col-md-offset-1"> <button class="btn btn-danger" type="button" onclick="deleteIncome(this)">{{trans('user.delete')}}</button> </div> </div> </div>';

        function deleteIncome(e){
            $(e).parent().parent().remove();
        }

        $('.addIncome').on('click',function(){
            $('#incomeSources').append(income);
        });

    </script>

@stop