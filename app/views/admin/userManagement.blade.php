@extends('admin.layout')

@section('content')

<div class="widget">
	<div class="widget-head">
		<h4 class="heading">{{trans('admin.userManagement')}}</h4>
	</div>
	<div class="widget-body innerAll inner-2x">

        <div style="padding: 10px;float: right"><span>Register a new user here: </span><a class="btn btn-info btn-sm" href="{{URL::Route('adminAddUser')}}"> Add New Renter / Landlord ! </a></div>

		<!-- Table -->
		<table class="footable table table-striped table-primary tablet breakpoint footable-loaded">

			<!-- Table heading -->
			<thead>
				<tr>
					<th data-class="expand" class="footable-first-column">Name</th>
					<th data-hide="phone">Email</th>
					<th data-hide="phone">Role</th>
					<th class="footable-last-column">Action</th>
				</tr>
			</thead>
			<!-- // Table heading END -->

			<!-- Table body -->
			<tbody>
            @foreach($users as $user_)
				<!-- Table row -->
				<tr class="gradeX">
					<td class="expand footable-first-column">{{$user_->first_name}} {{$user_->last_name}}</td>
					<td>{{$user_->email}}</td>
					<td>
					    @if($user_->role_id==1)
					        <a href="?role=landlord"><span class="label label-success"> Landlord </span></a>
					    @elseif($user_->role_id==2)
					        <a href="?role=renter"><span class="label label-default"> Renter </span></a>
					    @elseif($user_->role_id==3)
					        <a href="?role=admin"><span class="label label-warning"> Admin </span></a>
					    @endif
					</td>
					<td class="footable-last-column">
						<a class="btn btn-danger btn-sm" href="{{URL::Route('deleteUser',array('id'=>$user_->id))}}" onclick="return confirm('Are you sure to delete?')"> Delete User </a>
						<a class="btn btn-primary btn-sm" href="{{URL::Route('editUser',array('id'=>$user_->id))}}"> Edit </a>
					</td>
				</tr>
				<!-- // Table row END -->
            @endforeach


			</tbody>
			<!-- // Table body END -->

		</table>
		<!-- // Table END -->



		<center>
			{{$users->links()}}
		</center>
	</div>
</div>

@stop