@extends('admin.layout')

@section('content')

<div class="widget">
	<div class="widget-head">
		<h4 class="heading">{{trans('admin.rentTransactions')}}</h4>
	</div>
	<div class="widget-body innerAll inner-2x">

		<!-- Table -->
		<table class="footable table table-striped table-primary tablet breakpoint footable-loaded">

			<!-- Table heading -->
			<thead>
				<tr>
					<th data-class="expand" class="footable-first-column">Date</th>
					<th data-hide="phone">Renter</th>
					<th data-hide="phone">Landlord Name</th>
					<th>Paypal</th>
					<th class="footable-last-column">amount</th>
				</tr>
			</thead>
			<!-- // Table heading END -->

			<!-- Table body -->
			<tbody>
            <?php 
			for($i=0; $i<count($transactions); $i++)
			{    
			?>
				<!-- Table row -->
				<tr class="gradeX">
					<td class="expand footable-first-column">{{$transactions[$i]['transactions_info_date']}}</td>
					<td>{{$transactions[$i]['tenant']}}</td>
					<td>
					    {{$transactions[$i]['owner']}}
					</td>
					<td>{{$transactions[$i]['transactions_info_mode']}}</td>
					<td class="footable-last-column">
						{{$transactions[$i]['transactions_info_amount']}}
                    </td>
				</tr>
				<!-- // Table row END -->
            <?php
        	}
        ?>


			</tbody>
			<!-- // Table body END -->

		</table>
		<!-- // Table END -->

	</div>
</div>

@stop