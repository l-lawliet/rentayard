
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if gt IE 8]> <html class="ie paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if !IE]><!--><html class="paceCounter paceSocial sidebar sidebar-social footer-sticky"><!-- <![endif]-->
<head>
    <title>Rentayawd</title>

    <!-- Meta -->

    <meta charset="utf-8">
    <link rel="icon" href="{{Setting::get('logo')}}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

    <!--[if lt IE 9]><link rel="stylesheet" href="../assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->


    <link href="{{asset('assets/css/skins/module.admin.stylesheet-complete.skin.green.min.css')}}" rel="stylesheet" />

          <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('assets/images/apple-touch-icon-144-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('assets/images/apple-touch-icon-114-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('assets/images/apple-touch-icon-72-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" href="{{asset('assets/images/apple-touch-icon-57-precomposed.png')}}">
  <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}">
  <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
</head>
<body>

<div class="container-fluid" style="visibility: visible">

<!-- Main Sidebar Menu -->
<div id="menu" class="hidden-print sidebar-default sidebar-brand-primary">


<div id="sidebar-social-wrapper">
<div id="brandWrapper">
    <a href="?page=index&amp;lang="><span class="text">{{(Setting::has('sitename'))?Setting::get('sitename'):''}} Admin</span></a>
</div>
<ul class="menu list-unstyled">
    <li class="{{($currentPage=='dashboard')?'active':''}}">
        <a href="{{URL::Route('adminDashboard')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('admin.dashboard')}}</span>
        </a>
    </li>
    <li class="{{($currentPage=='userManagement')?'active':''}}">
        <a href="{{URL::Route('userManagement')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('admin.userManagement')}}</span>
        </a>
    </li>

    <li class="{{($currentPage=='settings')?'active':''}}">
        <a href="{{URL::Route('settings')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('admin.settings')}}</span>
        </a>
    </li>

    <li class="{{($currentPage=='editEmailTemplates')?'active':''}}">
        <a href="{{URL::Route('editEmailTemplates')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('admin.editEmailTemplates')}}</span>
        </a>
    </li>

    <li class="{{($currentPage=='propertyManagement')?'active':''}}">
        <a href="{{URL::Route('propertyManagement')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('admin.propertyManagement')}}</span>
        </a>
    </li>

    <li class="{{($currentPage=='rentTransactions')?'active':''}}">
        <a href="{{URL::Route('rentTransactions')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('admin.rentTransactions')}}</span>
        </a>
    </li>

    <li class="{{($currentPage=='rentTransactions')?'active':''}}">
        <a href="{{URL::Route('adminHelp')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('admin.help')}}</span>
        </a>
    </li>

</ul>

</div>
</div>
<div id="content">
    <div class="navbar hidden-print navbar-default box main" role="navigation">

        <div class="user-action pull-left menu-right-hidden-xs menu-left-hidden-xs border-left">
            <div class="dropdown username pull-left">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span class="media margin-none">
                        <span class="pull-left">{{trans('user.hi')}},</span>
                        <span class="media-body"> {{$user->first_name}} <span class="caret"></span></span>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::Route('logout')}}">{{trans('user.logout')}}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="innerAll">
        <div class="row">
            @include('partials.notifications')
            @yield('content')
        </div>
    </div>


</div>

</div>
<script src="{{asset('assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<!-- <script src="{{asset('assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/core_preload/preload.pace.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script> -->
<script src="{{asset('assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/components/core/core.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/media_gridalicious/jquery.gridalicious.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/media_gridalicious/gridalicious.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/menus/sidebar.main.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/components/menus/sidebar.collapse.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/plugins/other_mixitup/jquery.mixitup.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/other_mixitup/mixitup.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>

@yield('script')

</body>
</html>