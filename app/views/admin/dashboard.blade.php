@extends('admin.layout')

@section('content')

<div class="row">
    <div class="widget-head height-auto ">
        <div class="row row-merge ">
            <div class="col-md-3">
                <a href="" class="widget widget-icon success innerAll inner-2x text-center text-regular">
                    <i class="display-block icon-user-1 text-xlarge"></i>
                    <span class="lead">{{$total_users}} Users</span>
                </a>
            </div>
            <div class="col-md-3">
                <a href="" class="widget widget-icon inverse innerAll inner-2x text-center text-regular">
                	<i class="display-block icon-dollar text-xlarge"> {{$total_deposits}}</i>
                	<span class="lead">Deposit transacted</span>
                </a>
            </div>
            <div class="col-md-3">
                <a href="" class="widget widget-icon primary innerAll inner-2x text-center text-regular">
                    <i class="display-block icon-dollar text-xlarge"> {{$total_rent}}</i>
                    <span class="lead">Rent transacted</span>
                </a>
            </div>
            <div class="col-md-3">
                <a href="" class="widget widget-icon inverse innerAll inner-2x text-center text-regular">
                    <i class="display-block icon-building text-xlarge"></i>
                    <span class="lead">{{$total_properties}} Properties</span>
                </a>
            </div>
        </div>
    </div>
</div>

@stop

