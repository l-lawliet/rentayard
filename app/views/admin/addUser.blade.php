<?php
/**
 * Created by PhpStorm.
 * User: David White
 * Date: 29/7/15
 * Time: 2:39 PM
 */
?>

@extends('admin.layout')

@section('title','Add New User'))

@section('content')

<div class="widget" id="editProfile">
    <div class="widget-head">
        <h4 class="heading">Add New User</h4>
    </div>
    <div class="widget-body innerAll">

<form class="form-horizontal" action = "{{route('adminAddUserProcess')}}" method="post" enctype="multipart/form-data">

<div class="row innerLR">

    <div class="col-md-6">
        <div class="form-group">
            <label for="first_name" class="control-label col-md-4">Select a Role:</label>
            <div class="col-md-8">
                <select name="role" class="form-control">

                    <?php

                    $count = User::where('role_id',3)->count();


                    ?>

                    @if(!$count)
                    <option value="admin">Admin ( one time ) </option>
                    @endif
                    <option value="landlord">Landlord (30 Days Free Trial)</option>
                    <option value="renter">Renter (Free)</option>


                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="first_name" class="control-label col-md-4">{{trans('user.first_name')}}</label>
            <div class="col-md-8">
                <input class="form-control" type="text" name="first_name" id="first_name" />
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="last_name" class="control-label col-md-4">{{trans('user.last_name')}}</label>
            <div class="col-md-8">
                <input class="form-control" type="text" name="last_name" id="last_name" />
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="phone" class="control-label col-md-4">{{trans('user.email')}}</label>
            <div class="col-md-8">
                <input class="form-control" type="text" name="email" id="email" />
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block">Create Account</button>
</div>
    </form>

@stop