@extends('landlord.layout')

@section('content')
<?php 
for($i=0; $i<count($applications); $i++)
{
    if($applications[$i]['status']!='denied')
    {
?>
    <div class="col-md-3">
        <div class="widget">
            <h5 class="innerAll margin-none border-bottom bg-gray">{{trans('landlord.application')}} #{{$applications[$i]['application_id']}}</h5>
            <div class="widget-body padding-none">
            <h4><center>Renter</center></h4>
                <div class="media border-bottom innerAll margin-none">
                    <?php $image = ($applications[$i]['profile_picture'])?$applications[$i]['profile_picture']:Config::get('app.default_dp'); ?>
                    <!-- <img src="{{ Image::path('/uploads/'.$image, 'resizeCrop', 32, 32) }}" class="pull-left media-object"> -->
 
                    <img src="{{asset(Image::path('/uploads/'.$image, 'resizeCrop', 32, 32))}}" class="pull-left media-object" style="width:35%;height:35%">
 
                    <div class="media-body">
                        <h5 class="margin-none">
                        @if($applications[$i]['first_name'])
                            <a href="{{URL::route('viewRenter',$applications[$i]['id'])}}" target="_blank">{{$applications[$i]['first_name']}} {{$applications[$i]['last_name']}}</a>
                        @else
                            {{$applications[$i]['email']}}
                        @endif
                        </h5>
                        <small> $ {{$applications[$i]['amount']}} </small>
                    </div>
                </div>
             <h4><center>Co Renter</center></h4>
             <?php 
             for($j=0;$j<count($applications[$i]['corenter']);$j++)
             {
                 $applications[$i]['corenter'][$j]['first_name'];
             ?>
                <div class="media border-bottom innerAll margin-none">
                    <?php $image = ($applications[$i]['corenter'][$j]['profile_picture'])?$applications[$i]['corenter'][$j]['profile_picture']:Config::get('app.default_dp'); ?>
                    <!-- <img src="{{ Image::path('/uploads/'.$image, 'resizeCrop', 32, 32) }}" class="pull-left media-object"> -->
 
                    <img src="{{asset('uploads/'.$image)}}" class="pull-left media-object" style="width:35%;height:35%">
 
                    <div class="media-body">
                        <h5 class="margin-none">
                        @if($applications[$i]['corenter'][$j]['first_name'])
                            <a href="{{URL::route('viewRenter',$applications[$i]['corenter'][$j]['id'])}}" target="_blank">{{$applications[$i]['corenter'][$j]['first_name']}} {{$applications[$i]['corenter'][$j]['last_name']}}</a>
                        @else
                            {{$applications[$i]['corenter'][$j]['email']}}
                        @endif
                        </h5>
                         
                    </div>
                </div>
                <?php
                }
         
         ?>
                <div class="row">
                        @if($applications[$i]['status']=="sent")
                        <div class="col-sm-6">
                            <a class="btn btn-success btn-block" href="{{URL::Route('approveApplication',array('id'=>$applications[$i]['application_id']))}}"> {{trans('landlord.approve')}} </a>
                        </div>
                        <div class="col-sm-6">
                            <a class="btn btn-danger btn-block" href="{{URL::Route('denyApplication',array('id'=>$applications[$i]['application_id']))}}"> {{trans('landlord.deny')}} </a>
                        </div>
                        @elseif($applications[$i]['status']=="approved")
                            <div class="ribbon-wrapper"><div class="ribbon primary"> {{trans('landlord.approved')}} </div></div>
                            <div class="col-sm-12">
                                <a class="btn btn-danger btn-block" href="{{URL::Route('denyApplication',array('id'=>$applications[$i]['application_id']))}}"> {{trans('landlord.deny')}} </a>
                            </div>
                        @elseif($applications[$i]['status']=="accepted")
                            <div class="ribbon-wrapper"><div class="ribbon primary"> {{trans('landlord.accepted')}} </div></div>
                        @elseif($applications[$i]['status']=="rejected")
                            <div class="ribbon-wrapper"><div class="ribbon danger"> {{trans('landlord.rejected')}} </div></div>
                        @endif
                </div>
            </div>
        </div>
    </div>
<?php 
   }
}
?>

 

@stop