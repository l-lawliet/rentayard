 
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if gt IE 8]> <html class="ie paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if !IE]><!--><html class="paceCounter paceSocial sidebar sidebar-social footer-sticky"><!-- <![endif]-->
<head>
    <title>RentaYawd</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <link rel="icon" href="{{Setting::get('logo')}}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

    <!--[if lt IE 9]><link rel="stylesheet" href="../assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->


    <link href="{{asset('assets/css/skins/module.admin.stylesheet-complete.skin.green.min.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/css/custom-style.css')}}" rel="stylesheet" />

          <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('assets/images/apple-touch-icon-144-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('assets/images/apple-touch-icon-114-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('assets/images/apple-touch-icon-72-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" href="{{asset('assets/images/apple-touch-icon-57-precomposed.png')}}">
  <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}">
  <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
</head>
<body class="bg-blk">
            <!-- Static navbar -->
      <nav class="navbar navbar-default">
       
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{{route('home')}}}"><img src="{{asset('assets/images/roomly-logo.png')}}">{{Setting::get('sitename')}}</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
             <!--  <li><a href="{{{route('buy')}}}">Buy</a></li> -->
              <li><a href="{{{route('rent')}}}">Rent</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li ><a href="{{URL::route('savedSearch')}}">Saved search <!-- <span class="sr-only">(current)</span> --></a></li>
              <!-- <li><a href="{{URL::route('savedHome')}}">Saved Home</a></li> -->
              <li><a href="{{URL::route('landlordDashboard')}}">{{Auth::user()->first_name}}</a></li>
            </ul>
          </div><!--/.nav-collapse -->
       
      </nav>

<div class="container-fluid dash" style="visibility: visible">

<!-- Main Sidebar Menu -->
<div id="menu" class="hidden-print sidebar-default sidebar-brand-primary">


<div id="sidebar-social-wrapper">
<div id="brandWrapper">
    <a href="?page=index&amp;lang="><span class="text">{{(Setting::has('sitename'))?Setting::get('sitename'):''}} Landlord</span></a>
</div>
<ul class="menu list-unstyled">
    <li class="{{($currentPage=='dashboard')?'active':''}}">
        <a href="{{URL::Route('landlordDashboard')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('landlord.dashboard')}}</span>
        </a>
    </li>

    <li class="hasSubmenu {{($currentPage=='addProperty')?'active':''}}">
            <a href="#menu-6dbebf84f11222" data-toggle="collapse" class="collapsed">
                <i class="icon-add-symbol"></i>
                <span>Add Property</span>
            </a>
            <ul class="collapse" id="menu-6dbebf84f11222" style="height: 1px;">
                <li>
                    <a href="{{URL::route('addPropertyRent')}}" class="no-ajaxify">
                        <i class="icon-add-symbol"></i>
                        <span>Rent</span>
                    </a>
                </li>
<!--
                <li>
                    <a href="{{URL::route('addPropertySale')}}" class="no-ajaxify">
                        <i class="icon-add-symbol"></i>
                        <span>Sale</span>
                    </a>
                </li>
-->
                    
            </ul>
        </li>
        <li class="hasSubmenu {{($currentPage=='addProperty')?'active':''}}">
            <a href="#menu-properties" data-toggle="collapse" class="collapsed">
                <i class="icon-add-symbol"></i>
                <span>Properties Request</span>
            </a>
            <ul class="collapse" id="menu-properties" style="height: 1px;">
                <li>
                    <a href="{{URL::route('PropertyRentRequest')}}" class="no-ajaxify">
                        <i class="icon-add-symbol"></i>
                        <span>Rent</span>
                    </a>
                </li>
<!--
                <li>
                    <a href="{{URL::route('PropertySaleRequest')}}" class="no-ajaxify">
                        <i class="icon-add-symbol"></i>
                        <span>Sale</span>
                    </a>
                </li>
-->
                    
            </ul>
        </li>
<!-- 

    <li class="{{($currentPage=='addProperty')?'active':''}}">
        <a href="{{URL::Route('addProperty')}}">
            <i class="icon-add-symbol"></i>
            <span>{{trans('landlord.addProperty')}}</span>
        </a>
    </li> -->

    @foreach($properties->all() as $property)
        <li class="hasSubmenu {{($currentPage=='viewProperty'.$property->id||$currentPage=='propertyApplications'.$property->id||$currentPage=='propertyRents'.$property->id||$currentPage=='viewPropertyPhotos'.$property->id)?'active':''}}">
            <a href="#menu-6dbebf84f{{$property->id}}" data-toggle="collapse">
                <i class="icon-home-1"></i>
                <span>{{ucfirst($property->name)}}</span>
            </a>
            <ul class="collapse {{($currentPage=='viewProperty'.$property->id||$currentPage=='propertyApplications'.$property->id||$currentPage=='propertyRents'.$property->id||$currentPage=='viewPropertyPhotos'.$property->id)?'in':''}}" id="menu-6dbebf84f{{$property->id}}">
                <li class="{{($currentPage=='viewProperty'.$property->id)?'active':''}}">
                    <a href="{{URL::route('landlordViewProperty',array('id'=>$property->id))}}" class="no-ajaxify">
                        <i class="fa fa-edit"></i>
                        <span>{{trans('landlord.viewOrEdit')}}</span>
                    </a>
                </li>
                <li class="{{($currentPage=='viewPropertyPhotos'.$property->id)?'active':''}}">
                    <a href="{{URL::route('landlordPropertyPhotos',array('id'=>$property->id))}}" class="no-ajaxify">
                        <i class="fa fa-camera"></i>
                        <span>{{trans('landlord.photos')}}</span>
                    </a>
                </li>
                @if($property->choice == 0)
                    @if($property->accept_application)
                        <li class="{{($currentPage=='propertyApplications'.$property->id)?'active':''}}">
                            <a href="{{URL::Route('landlordPropertyApplications',array('id'=>$property->id))}}" class="no-ajaxify">
                                <i class="icon-user-1"></i>
                                <span>{{trans('landlord.applications')}}</span>
                            </a>
                        </li>
                    @endif
<!--
                    <li class="{{($currentPage=='propertyRents'.$property->id)?'active':''}}">
                        <a href="{{URL::Route('landlordRentPayments',array('id'=>$property->id))}}" class="no-ajaxify">
                            <i class="icon-dollar"></i>
                            <span>{{trans('landlord.rentPayments')}}</span>
                        </a>
                    </li>
-->
                @endif
                <li class="">

                    <a href="{{URL::route('landlordDeleteProperty',array('id'=>$property->id))}}" class="no-ajaxify" onclick="return confirm('Are you sure to delete?')">
 
                        <i class="fa fa-times"></i>
                        <span>{{trans('landlord.deleteProperty')}}</span>
                    </a>
                </li>
            </ul>
        </li>
    @endforeach

    <li class="{{($currentPage=='vacate_notice')?'active':''}}">
        <a href="{{URL::Route('landlordVacate')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('landlord.landlordvacate')}}</span>
        </a>
    </li>

     <li class="{{($currentPage=='lanlordhelp')?'active':''}}">
        <a href="{{URL::Route('landlordHelp')}}">
            <i class="icon-clipboard"></i>
            <span>{{trans('landlord.landlordhelp')}}</span>
        </a>
    </li>

</ul>

</div>
</div>
<div id="content">
    <div class="navbar hidden-print navbar-default box main" role="navigation">

        <div class="user-action pull-left menu-right-hidden-xs menu-left-hidden-xs border-left">
            <div class="dropdown username pull-left">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span class="media margin-none">
                        <span class="pull-left">{{trans('user.hi')}},</span>
                        <span class="media-body"> {{Auth::user()->first_name}} <span class="caret"></span></span>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::Route('landlordEditProfile')}}">{{trans('user.editProfile')}}</a></li>
                    <li><a href="{{URL::Route('logout')}}">{{trans('user.logout')}}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="innerAll">
        <div class="row">
            @include('partials.notifications')
            @yield('content')
        </div>
    </div>


</div>

</div>
<script src="{{asset('assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<!-- <script src="{{asset('assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/core_preload/preload.pace.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script> -->
<script src="{{asset('assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/components/core/core.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/media_gridalicious/jquery.gridalicious.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/media_gridalicious/gridalicious.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/menus/sidebar.main.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/components/menus/sidebar.collapse.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/plugins/other_mixitup/jquery.mixitup.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/other_mixitup/mixitup.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>

@yield('script')

</body>
</html>