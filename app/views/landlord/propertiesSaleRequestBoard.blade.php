@extends('landlord.layout')

@section('title',trans('property.SalePropertyRequest'))

@section('content')

<div class="col-md-12">
    <div class="widget">
        <div class="widget-head">
            <h4 class="heading">Sale Properties Request</h4>
        </div>
        <div class="widget-body ">

       

           <!-- Table -->
            <table class="table table-striped table-responsive swipe-horizontal table-primary">

                <!-- Table heading -->
                <thead>
                    <tr>
                        <th> First Name </th>
                        <th> Last Name</th>
                        <th> Email</th>
                        <th> Phone Number </th>
                        <th> Description</th>
                    </tr>
                </thead>
                <!-- // Table heading END -->

                <!-- Table body -->
                <tbody>

                <?php
                $i=0;
                function chop_string($string,$x) {

  $string = strip_tags(stripslashes($string)); // convert to plaintext
  return substr($string, 0, strpos(wordwrap($string, $x), "\n"));
} 


                ?>
                     
               @foreach($demo as $property)
               <?php 
               $i=$i+1;
               $par=chop_string($property->description,30); ?>
                <!-- Table row -->
                <tr class="gradeX">
                     
                    <td class="expand footable-first-column">{{$property->first_name}}</td>
                    <td class="expand footable-first-column">{{$property->last_name}}</td>
                    <td class="expand footable-first-column">{{$property->email}}</td>
                    <td class="expand footable-first-column">{{$property->phone_number}}</td>
                    <td class="expand footable-first-column" style="width:25%;">

                    <span class="allsmallview smallview{{$i}}">{{$par}}...
                    <br>
                    <a  onclick="view({{$i}});">Click here</a>
                     </span>
                    <span class="allfullview fullview{{$i}}" style="display:none">{{$property->description}}</span>
                    <div>
                    
                     <a class="btn btn-danger btn-sm" href="{{URL::route('landlordDeletePropertySendRequest',array('id'=>$property->id))}}" style="float:right;margin-right:6%;" onclick="return confirm('Are you sure to delete?')" >Delete</a>
                    </div>
                        
                    </td>
                     
                </tr>
                <!-- // Table row END -->
            @endforeach
          
                    
                </tbody>
                <!-- // Table body END -->

            </table>
            <!-- // Table END -->






        </div>
    </div>
</div>
@stop


@section('script')
  <script type="text/javascript">
            function view(id)
            {
                $('.allsmallview').show();
                $('.allfullview').hide();
                $('.smallview'+id).hide();
                $('.fullview'+id).show();
            }
            </script>
@stop