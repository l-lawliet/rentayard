@extends('landlord.layout')

@section('title',trans('property.addProperty'))

@section('content')

<div class="col-md-10 col-md-offset-1">
    <div class="widget">
        <div class="widget-head">
            <h4 class="heading">{{trans('property.selectProperty')}}</h4>
        </div>
        <div class="widget-body innerAll">
            <div class="row innerLR">
                <div class="col-md-8 col-md-offset-1">
                    <a href="{{URL::route('addPropertyRent')}}" class="btn btn-success">Add property for Rent</a>
<!--                    <a href="{{URL::route('addPropertySale')}}" class="btn btn-success">Add property for Sale</a>-->
                </div>
            </div>
        </div>
    </div>
</div>
@stop


@section('script')

<script src="{{asset('assets/plugins/forms_elements_bootstrap-switch/js/bootstrap-switch.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/forms_elements_bootstrap-switch/bootstrap-switch.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>

@stop