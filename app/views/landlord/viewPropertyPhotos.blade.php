@extends('landlord.layout')


@section('content')

<div class="widget widget-gallery" data-toggle="collapse-widget">
    <div class="widget-head"><h4 class="heading"> {{ucfirst($property->name)}}{{trans('landlord.propertysPhotoGallery')}}</h4></div>
    <div class="widget-body">

        <!-- Gallery Layout -->
        <div class="gallery gallery-2">
            
                @if(count($property->photos))
                <div class="col-md-12">
                   
                    @foreach($property->photos as $photo)

                    <div class="col-md-3 hidden-phone">
                        <a class="thumb no-ajaxify" data-gallery="gallery-2" href="{{asset('uploads/property_photos/'.$photo->filename)}}"><img src="{{asset(Image::path('/uploads/property_photos/'.$photo->filename, 'resizeCrop', 400, 200))}}" alt="photo" class="img-responsive" /></a>

                        <a class="btn btn-danger btn-block" href="{{URL::route('landlordPropertyPhotoDelete',array('id'=>$photo->id))}}" onclick="return confirm('Are you sure to delete?')"><i class="icon-delete-symbol"></i> &nbsp; &nbsp; Delete </a>
 
                    </div>

                    @endforeach
                    
                </div>
                @else
                    <div class="well text-center">
                        <div class="center text-large innerAll">
                            <i class="fa fa-5x fa-eye-slash  "></i>
                        </div>
                        <h1 class="strong innerTB">Oups!</h1>
                        <div class="well bg-white text-danger strong ">
                            No Image Found
                        </div>
                    </div>
                @endif

                <div style="height: 50px;"></div>

                <form action="{{URL::Route('uploadPropertyPhoto',array('id'=>$property->id))}}" method="post" enctype="multipart/form-data">
                    <div class="input-group">
                        <br>
                        <input class="form-control" type="file" name="photos" />
                        <div class="input-group-btn">
                         <br>
                            <button class="btn btn-success">{{trans('landlord.upload')}}</button>
                        </div>
                    </div>
                </form>

        </div>
        <!-- // Gallery Layout END -->

    </div>
</div>








<!-- Blueimp Gallery -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev no-ajaxify">‹</a>
    <a class="next no-ajaxify">›</a>
    <a class="close no-ajaxify">×</a>
    <a class="play-pause no-ajaxify"></a>
    <ol class="indicator"></ol>
</div>
<!-- // Blueimp Gallery END -->

@stop


@section('script')
<script src="{{asset('assets/plugins/media_blueimp/js/blueimp-gallery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/media_blueimp/js/jquery.blueimp-gallery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
@stop