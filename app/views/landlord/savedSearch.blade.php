@extends('landlord.layout')

@section('content')

<div class="widget" id="editProfile">
    <div class="widget-head">
        <h4 class="heading">Saved Search</h4>
    </div>
    <div class="widget-body innerAll">
        <div class="row">
        	<?php $i = 1; ?>
            <div class="col-md-10">
            	<table class="table">
				  <thead>
					  <tr>
						  <th class="text-center">Id</th>
						  <th class="text-center">Click</th>                                       
						  <th class="text-center">Action</th>                                          
					  </tr>
				  </thead>   
				  <tbody>
				  	@foreach($saveSearch as $savesear)
					<tr class="tr-btm">
						<td class="text-center">{{$i++}}</td>
						
						<td class="center"><a class="btn btn-success" href="{{{route('userSearch',array('key' => $savesear->keyword))}}}">{{$savesear->keyword}}</a></td>
 
					    <td class="center"><a class="btn btn-danger" href="{{{route('deleteSearch',array('key' => $savesear->keyword))}}}" onclick="return confirm('Are you sure to delete?')">Delete</a></td>                             
 
					
					</tr>

					@endforeach
				  </tbody>
				</table>
            </div>
        </div>
    </div>
</div>

@stop