@extends('landlord.layout')


@section('content')

<div class="col-md-3">
    <div class="widget">
        <h5 class="innerAll margin-none border-bottom bg-gray">{{trans('landlord.tenants')}} </h5>
        <div class="widget-body padding-none">
        @foreach($tenants as $tenant)
            <div class="media border-bottom innerAll margin-none">
                <?php $image = ($tenant->user['profile_picture'])?$tenant->user['profile_picture']:Config::get('app.default_dp'); ?>
                <!-- <img src="{{ Image::path('/uploads/'.$image, 'resizeCrop', 32, 32) }}" class="pull-left media-object"> -->
                    <img src="{{asset('uploads/'.$image)}}" class="pull-left media-object" style="width:30%;height:30%">

                <div class="media-body">
                    <h5 class="margin-none">
                        <a href="{{URL::route('viewRenter',$tenant->user['id'])}}" target="_blank">{{$tenant->user['first_name']}} {{$tenant->user['last_name']}}</a>
                        @if(!$tenant->rentPaid)
                            <div class="pull-right">
                                <span class="label label-danger"> Due </span>
                            </div>
                        @endif
                    </h5>
                    <small> $ {{$tenant->rent}} </small>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="widget">
    	<div class="widget-head">
    		<h4 class="heading">{{trans('user.transactions')}}</h4>
    	</div>
    	<div class="widget-body innerAll inner-2x">

    		<!-- Table -->
    		<table class="table table-striped table-responsive swipe-horizontal table-primary">

    			<!-- Table heading -->
    			<thead>
    				<tr>
    				    <th> {{trans('landlord.tenants')}} </th>
    					<th> {{trans('user.amount')}} </th>
    					<th> {{trans('user.as')}} </th>
    					<th> {{trans('user.mode')}} </th>
    					<th> {{trans('user.timestamp')}} </th>
    				</tr>
    			</thead>
    			<!-- // Table heading END -->

    			<!-- Table body -->
    			<tbody>


                    @foreach($rents as $rent)

                    <!-- Table row -->
                    <tr class="gradeA">
                        <td> {{$rent->user->first_name}} {{$rent->user->last_name}} </td>
                        <td> $ {{$rent->amount}} </td>
                        <td> {{trans('user.rent')}} </td>
                        <td> {{$rent->mode}} </td>
                        <td> {{$rent->created_at}} </td>
                    </tr>
                    <!-- // Table row END -->

                    @endforeach

                    @if(isset($deposit))

                    <!-- Table row -->
                    <tr class="gradeA">
                        <td> {{$deposit->user->first_name}} {{$deposit->user->last_name}} </td>
                        <td> $ {{$deposit->amount}} </td>
                        <td> {{trans('user.deposit')}} </td>
                        <td> {{$deposit->mode}} </td>
                        <td> {{$deposit->created_at}} </td>
                    </tr>
                    <!-- // Table row END -->

                    @endif

    			</tbody>
    			<!-- // Table body END -->

    		</table>
    		<!-- // Table END -->

    	</div>
    </div>

</div>


@stop
