@extends('landlord.layout')

@section('content')

<div class="widget" id="editProfile">
    <div class="widget-head">
        <h4 class="heading">{{trans('user.save_home')}}</h4>
    </div>
    <div class="widget-body innerAll">
        	<div class="row">
        	 @if(Session::has('msg'))
 <div class="alert alert-info alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('msg')}}
</div>
  @endif
            	<div class="col-md-10">
            		   @for($i = 0; $i < $count; $i++)
			           <div class="col-md-4">
			            <a href="{{URL::route('viewProperty',$property[$i]['id'])}}">
			            <div class="widget">
			                <div class="timeline-cover das">
			                    <div class="cover image ">
			                        <div class="top">
			                        	@if($photo[$i]->filename != NULL)
			                                <img src="{{ asset('/uploads/property_photos/'.$photo[$i]->filename)}}" class="img-responsive">
			                            @else
			                                <img src="{{ asset('/uploads/default_property_photo.jpg')}}" class="img-responsive">
			                            @endif
			                        </div>
			                    </div>
			                    <div class="widget cover image">
			                        <div class="widget-body padding-none margin-none">
			                          <div class="photo">
                                @if($landlord[$i]->profile_picture)
                                <img src="{{asset('/uploads/'.$landlord[$i]->profile_picture)}}" style="width:70px;height:70px;" class="img-circle">
                                @else
                                <img src="{{asset('uploads/default.jpg')}}" style="width:70px;height:70px;" class="img-circle">
                                @endif 
                       			 </div>
			                      
			                            <div class="innerAll pull-left">
			                                <p class="lead margin-none "> <i class="fa fa-home text-muted fa-fw"></i> {{ucfirst($property[$i]['name'])}} </p>
			                            </div>
			                        </div>
			                        <div class="clearfix"></div>
			                    </div>
			                </div>
			                <div class="text-center innerAll">
			                    <p class="lead margin-none "> <i class="fa fa-location-arrow text-muted fa-fw"></i> {{ucfirst($property[$i]['city'])}}, {{ucfirst($property[$i]['state'])}}, {{$property[$i]['zip']}} </p>
			                    @if($property[$i]['rent'] > 0)
			                    <p class="lead margin-none ">  Rent : $ {{$property[$i]['rent']}} </p>
			                    <p>For rent</p>
			                    @else
			                    <p class="lead margin-none ">  Price : $ {{$property[$i]['price']}} </p>
			                    <p>For sale</p>
			                    @endif
 
<a class = "btn btn-danger btn-block" href="{{{route('deleteSavedHome', array('id' => $property[$i]['id']))}}}" onclick="return confirm('Are you sure to delete?')">Delete</a>
 

			                </div>


			            </div>
			            </a>
			        </div>
			        
 				    @endfor
				</div>
			</div>
    </div>
</div>

            

@stop