@extends('landlord.layout')

@section('content')


<div class="col-md-3">
    <?php foreach($tenant as $ten) {?>
    <div class="widget">

        <div class="widget-body padding-none">

            <div class="media border-bottom innerAll margin-none">
                <div class="media-body">
                    <h5 class="margin-none">
                        {{trans('landlord.landlordvacate')}} from the property on <br> {{$ten->vacate_date}} <a href="{{URL::Route('viewProperty',$ten->property_id)}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> </a>
                    </h5>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <a class="btn btn-success btn-block" href="{{URL::Route('landlordVacateApprove',$ten->id)}}"> {{trans('landlord.approve')}} </a>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-danger btn-block" href="{{URL::Route('landlordVacateDecline',$ten->id)}}"> {{trans('landlord.deny')}} </a>
                </div>

            </div>
        </div>


    </div>
    <?php } ?>
</div>

@stop