@extends('landlord.layout')

@section('title',trans('property.RentPropertyRequest'))

@section('content')

<div class="col-md-12">
    <div class="widget">
        <div class="widget-head">
            <h4 class="heading">Rent Properties Request</h4>
        </div>
        <div class="widget-body ">

       

           <!-- Table -->
            <table class="table table-striped table-responsive swipe-horizontal table-primary">

                <!-- Table heading -->
                <thead>
                    <tr>
                        <th> First Name </th>
                        <th> Last Name</th>
                        <th> Email</th>
                        <th> Phone Number </th>
                        <th> Description</th>
                    </tr>
                </thead>
                <!-- // Table heading END -->

                <!-- Table body -->
                <tbody>

                <?php
                $i=0;
                function chop_string($string,$x) {

  $string = strip_tags(stripslashes($string)); // convert to plaintext
  return substr($string, 0, strpos(wordwrap($string, $x), "\n"));
} 


                ?>
                     
               @foreach($demo as $property)

               <?php 
               $i=$i+1;
               $v=chop_string($property->description,30); ?>
                <!-- Table row -->
                <tr class="gradeX">
                     
                    <td class="expand footable-first-column">{{$property->first_name}}</td>
                    <td class="expand footable-first-column">{{$property->last_name}}</td>
                    <td class="expand footable-first-column">{{$property->email}}</td>
                    <td class="expand footable-first-column">{{$property->phone_number}}</td>
                    <td class="expand footable-first-column" style="width:25%;">

                    <span class="allsmallview smallview{{$i}}">{{$v}}...
                    <br>
                    <a  onclick="view({{$i}});">Click here</a></div>
                    </span>
                    <span class="allfullview fullview{{$i}}" style="display:none">{{$property->description}}</span>
                    <div>
                    
                         <a class="btn btn-danger btn-sm" href="{{URL::route('landlordDeletePropertySendRequest',array('id'=>$property->id))}}" style="float:right;margin-right:6%;" onclick="return confirm('Are you sure to delete?')" >Delete</a>
                    </td>
                     
                </tr>
                <!-- // Table row END -->
            @endforeach

          
                    
                </tbody>
                <!-- // Table body END -->

            </table>
            <!-- // Table END -->






        </div>
    </div>
</div>
@stop
 






 




@section('script')

 <script type="text/javascript">
            function view(id)
            {
                $('.allsmallview').show();
                $('.allfullview').hide();
                $('.smallview'+id).hide();
                $('.fullview'+id).show();
            }
            </script>

<script src="{{asset('assets/plugins/forms_elements_bootstrap-switch/js/bootstrap-switch.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/forms_elements_bootstrap-switch/bootstrap-switch.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
    


@stop
