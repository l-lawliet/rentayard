@extends('landlord.layout')

@section('content')

<div class="col-md-8 col-md-offset-2">
<div class="widget">
    <div class="widget-head">
        <h4 class="heading">{{trans('user.editProfile')}}</h4>
    </div>
    <div class="widget-body innerAll">
        <div class="row">
            <?php $image = ($user->profile_picture)?$user->profile_picture:Config::get('app.default_dp'); ?>
            <img src="{{asset('uploads/'.$image)}}" class="img-circle col-md-2" style="weidth=30%;height=30%">

            <form action="{{URL::Route('landlordUpdateDp')}}" method="post" enctype="multipart/form-data">
                <div class="col-md-10">
                    <label for="dp">Profile Picture :</label>
                    <div class="input-group">
                        <input class="form-control" type="file" name="profile_picture"/>
                        <div class="input-group-btn">
                            <button class="btn btn-success">Upload</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


        <form class="form-horizontal" action="{{URL::Route('landlordUpdateProfile')}}" method="post">
        <div class="row innerLR">
            <div class="col-md-8 col-md-offset-1">


                <div class="form-group">
                    <label for="first_name" class="control-label col-md-4">{{trans('user.first_name')}}</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" name="first_name" id="first_name" value="{{$user->first_name}}"/>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" type="text" name="last_name" id="last_name" value="{{$user->last_name}}"/>
                        </div>
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-md-4">{{trans('user.email')}}</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" name="email" id="email" value="{{$user->email}}" disabled="true"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="paypal_email" class="control-label col-md-4">{{trans('user.paypal_email')}}</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" name="paypal_email" id="paypal_email" value="{{$user->paypal_email}}"/>
                    </div>
                </div>
                    <div class="col-md-6 col-md-offset-4">
                        <button class="btn btn-success" type="Submit">{{trans('user.save')}}</button>
                        <a href="{{URL::previous()}}" class="btn btn-danger btn-stroke">{{trans('user.cancel')}}</a>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
@stop