@extends('landlord.layout')

@section('content')


<ul class="timeline-activity list-unstyled">


	@foreach($activities as $activity)
        @if($activity->activity=='joined_roomy')
        <li class="active">
            <div class="block block-inline">
            <span class="marker"></span>
                <div class="caret"></div>
                <div class="box-generic">
                    <div class="timeline-top-info">
                        <i class="fa fa-user"></i>
                        <a href="" class="text-inverse">{{trans('user.you')}}</a> {{trans('user.joined')}}
                        <a href="" class="text-info"><i class="fa fa-location-arrow"></i> {{(Setting::has('sitename'))?Setting::get('sitename'):''}}</a>
                    </div>
                </div>
            </div>
        </li>
        @elseif($activity->activity=='send_rent_request')
       <li class="active">
               <div class="block block-inline">
               <span class="marker"></span>
                   <div class="caret"></div>
                   <div class="box-generic">
                       <div class="timeline-top-info">
                           <i class="fa fa-user"></i>
                           <span class="text-inverse"> {{trans('landlord.tenantsendrentRequest')}} </span>
                           @if(isset($activity->property))
                           <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" title="view Property" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                           &nbsp;<a class="btn btn-info btn-sm" href="{{URL::route('PropertyRentRequestBoard',array('id'=>$activity->property->id))}}"> click here </a>
                           @else
                           &lt;Property Removed&gt;
                           @endif
                       </div>
                   </div>
               </div>
           </li>
        @elseif($activity->activity=='applied')

            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse"><a href="{{URL::Route('viewRenter',array('id'=>$activity->application->primary_tenant->id))}}">{{$activity->application->primary_tenant->first_name}}</a> {{trans('landlord.renterapplied')}} </span>
                            @if(isset($activity->application->property_id))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->application->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->application->property->name}} </a>
                            &nbsp;<a class="btn btn-info btn-sm" href="{{URL::Route('landlordPropertyApplications',array('id'=>$activity->application->property->id))}}"> click here </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif

                        </div>
                    </div>
                </div>
            </li>
      @elseif($activity->activity=='send_sale_request')
       <li class="active">
               <div class="block block-inline">
               <span class="marker"></span>
                   <div class="caret"></div>
                   <div class="box-generic">
                       <div class="timeline-top-info">
                           <i class="fa fa-user"></i>
                           <span class="text-inverse"> {{trans('landlord.tenantsendsaleRequest')}} </span>
                           @if(isset($activity->property))
                           <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" title="view Property" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                           &nbsp;<a class="btn btn-info btn-sm" href="{{URL::route('PropertySaleRequestBoard',array('id'=>$activity->property->id))}}"> click here </a>
                           @else
                           &lt;Property Removed&gt;
                           @endif
                       </div>
                   </div>
               </div>
           </li>
        @elseif($activity->activity=='accepted')

            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse"><a href="{{URL::Route('viewRenter',array('id'=>$activity->application->primary_tenant->id))}}">{{$activity->application->primary_tenant->first_name}}</a> {{trans('landlord.applicationAccepted')}} </span>
                            @if(isset($activity->application->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->application->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->application->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif

                        </div>
                    </div>
                </div>
            </li>
       @elseif($activity->activity=='rejected')

           <li class="active">
               <div class="block block-inline">
               <span class="marker"></span>
                   <div class="caret"></div>
                   <div class="box-generic">
                       <div class="timeline-top-info">
                           <i class="fa fa-user"></i>
                           <span class="text-inverse"><a href="{{URL::Route('viewRenter',array('id'=>$activity->application->primary_tenant->id))}}">{{$activity->application->primary_tenant->first_name}}</a> {{trans('landlord.applicationRejected')}} </span>
                           @if(isset($activity->application->property))
                           <a href="{{URL::Route('viewProperty',array('id'=>$activity->application->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->application->property->name}} </a>
                           @else
                           &lt;Property Removed&gt;
                           @endif
                       </div>
                   </div>
               </div>
           </li>
       @elseif($activity->activity=='paid_rent')

           <li class="active">
               <div class="block block-inline">
               <span class="marker"></span>
                   <div class="caret"></div>
                   <div class="box-generic">
                       <div class="timeline-top-info">
                           <i class="fa fa-user"></i>
                           <span class="text-inverse"> {{trans('landlord.tenantPaidRent')}} </span>
                           @if(isset($activity->property))
                           <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                           @else
                           &lt;Property Removed&gt;
                           @endif
                       </div>
                   </div>
               </div>
           </li>
       @elseif($activity->activity=='paid_deposit')

           <li class="active">
               <div class="block block-inline">
               <span class="marker"></span>
                   <div class="caret"></div>
                   <div class="box-generic">
                       <div class="timeline-top-info">
                           <i class="fa fa-user"></i>
                           <span class="text-inverse"> {{trans('landlord.tenantPaidDeposit')}} </span>
                           @if(isset($activity->property))
                           <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                           @else
                           &lt;Property Removed&gt;
                           @endif
                       </div>
                   </div>
               </div>
           </li>
       @endif
	@endforeach

</ul>

{{$activities->links()}}

@stop

