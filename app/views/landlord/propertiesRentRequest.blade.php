@extends('landlord.layout')

@section('title',trans('property.RentProperty'))

@section('content')

<div class="col-md-12">
    <div class="widget">
        <div class="widget-head">
            <h4 class="heading">Rent Properties</h4>
        </div>
        <div class="widget-body ">

            <!-- Table -->
            <table class="table table-striped table-responsive swipe-horizontal table-primary">

                <!-- Table heading -->
                <thead>
                    <tr>
                        <th> Property Name </th>
                        <th> City </th>
                        <th> price </th>
                        <th> Action </th>
                        <th> Contact Request</th>
                    </tr>
                </thead>
                <!-- // Table heading END -->

                <!-- Table body -->
                <tbody>
                    @foreach($demo as $property)
                <!-- Table row -->
                <tr class="gradeX">
                   
                    <td class="expand footable-first-column">{{$property->name}}</td>
                     
                   
                    <td>{{$property->city}}</td>
                    <td>
                        @if($property->rent > 0)
                        $ {{$property->rent}}

                        @endif
                    </td>
                    <td class="footable-last-column">
                        <a class="btn btn-primary btn-sm" href="{{URL::route('landlordViewProperty',array('id'=>$property->id))}}"> View / Edit Property </a>
                        <a class="btn btn-danger btn-sm" href="{{URL::route('landlordDeleteProperty',array('id'=>$property->id))}}" onclick="return confirm('Are you sure to delete?')" > Delete Property </a>
                    </td>
                    <td class="footable-last-column">
                        <a class="btn btn-info btn-sm" href="{{URL::route('PropertyRentRequestBoard',array('id'=>$property->id))}}"> click here </a>
                    </td>
                </tr>
                <!-- // Table row END -->
            @endforeach
                    
                </tbody>
                <!-- // Table body END -->

            </table>
            <!-- // Table END -->





        </div>
    </div>
</div>
@stop


@section('script')

<script src="{{asset('assets/plugins/forms_elements_bootstrap-switch/js/bootstrap-switch.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/forms_elements_bootstrap-switch/bootstrap-switch.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
    


@stop
