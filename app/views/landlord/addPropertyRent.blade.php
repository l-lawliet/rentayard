@extends('landlord.layout')

@section('title',trans('property.addProperty'))

@section('content')

<div class="col-md-10 col-md-offset-1">
    <div class="widget">
        <div class="widget-head">
            <h4 class="heading">{{trans('landlord.addPropertyRent')}}</h4>
        </div>
        <div class="widget-body innerAll">

            <form class="form-horizontal" action="{{URL::Route('addPropertyRent')}}" method="post" enctype="multipart/form-data" files="true">
                <div class="row innerLR">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="form-group">
                            <label for="name" class="control-label col-md-4">{{trans('property.name')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="name" id="name" placeholder="Enter your property name" minlength="25" maxlength="30"/>
                            </div>
                        </div>
                        <div id="locationField" class="form-group">
                            <label for="address" class="control-label col-md-4">Enter your address</label>

                            <div class="col-md-8">
                            <input class="form-control" name="address" id="autocomplete" placeholder="Enter your property address"
                                   onFocus="geolocate()" type="text" />
                                   <p style="padding-top: 10px;">Note: Use this input box to search the place in map</p>
                            </div>
                        </div>
                        <div class="col-md-8 col-md-offset-4">
                          <div id="map-canvas" style="width: 104%; height: 200px;"></div>
                          <br>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="street" >street number</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="street_num" id="street_number" placeholder="Enter your property street number or 0 for none"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="street">street name</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="route" name="street_name" placeholder="Enter your property street name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="city">{{trans('property.city')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="locality" name="city" placeholder="Enter your city" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="state">{{trans('property.state')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="administrative_area_level_1"  name="state" placeholder="Enter your property state or town"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="country">country</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="country" id="country" placeholder="Enter your property country"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="zip">{{trans('property.zip')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="zip"  id="zip" placeholder="Enter your property zip code"/>
                            </div>
                        </div>
                      <input type="text" id="latitude" name="lat" hidden="true" required>
                      <input type="text" id="longitude" name="lon" hidden="true" required>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="rent">{{trans('property.rent')}}</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input class="form-control" type="text" name="rent" id="rent" placeholder="Enter your property rent amount"/>
                                    <span class="input-group-addon">.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="deposit">{{trans('property.deposit')}}</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input class="form-control" type="text" name="deposit" id="deposit" placeholder="Enter your property deposit amount"/>
                                    <span class="input-group-addon">.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="bedrooms">{{trans('property.bedrooms')}}</label>
                            <div class="col-md-8">
                                <select class="form-control" name="bedrooms" id="bedrooms">
                                    <option value="0">None</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10+</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="bathrooms">{{trans('property.bathrooms')}}</label>
                            <div class="col-md-8">
                                <select class="form-control" name="bathrooms" id="bathrooms">
                                    <option value="0">None</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10+</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="sqft">{{trans('property.squareFootage')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="sqft" id="sqft" placeholder="Shared or Not Shared"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="parking">{{trans('property.parking')}}</label>
                            <div class="col-md-8">
                                <select class="form-control" name="parking" id="parking">
                                    <option value="0">None</option>
                                    <option value="1">1 {{trans('property.car')}}</option>
                                    <option value="2">2 {{trans('property.cars')}}</option>
                                    <option value="3">3 {{trans('property.cars')}}</option>
                                    <option value="4">4 {{trans('property.cars')}}</option>
                                    <option value="5">5 {{trans('property.cars')}}</option>
                                    <option value="6">6 {{trans('property.cars')}}</option>
                                    <option value="7">7 {{trans('property.cars')}}</option>
                                    <option value="8">8 {{trans('property.cars')}}</option>
                                    <option value="9">9 {{trans('property.cars')}}</option>
                                    <option value="10">10+ {{trans('property.cars')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="zip">Description</label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="des"  id="des" placeholder="Enter your property description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="acceptApplication">{{trans('property.acceptApplications')}}</label>
                            <div class="col-md-8">
                                <div class="make-switch" data-on="success" data-off="danger"><input type="checkbox" name="acceptApplication" checked></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="acceptApplication">{{trans('property.showcase')}}</label>
                            <div class="col-md-8">
                                <div class="make-switch" data-on="success" data-off="danger"><input type="checkbox" name="showcase" checked></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="rentStartDate">{{trans('property.rentStartDate')}}</label>
                            <div class="col-md-8">
                                <select class="form-control" name="rentStartDate" id="rentStartDate">
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="photos">{{trans('property.uploadPhotos')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="file" name="photos" id="photos" />
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-4">
                            <button class="btn btn-success" id="addpropertyrent" type="Submit">{{trans('property.addProperty')}}</button>
                            <a href="{{URL::previous()}}" class="btn btn-danger btn-stroke">{{trans('user.cancel')}}</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop


@section('script')

<script src="{{asset('assets/plugins/forms_elements_bootstrap-switch/js/bootstrap-switch.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/forms_elements_bootstrap-switch/bootstrap-switch.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};


$(window).on('beforeunload', function(){
        return "If you want refresh , All the data will be lost";
    });

    // Form Submit
    $(document).on("submit", "form", function(event){
        // disable warning
        $(window).off('beforeunload');
    });


$(document).ready(function(){




  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });

  var mapOptions = {
    zoom: 6
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);


      map.setCenter(pos);
    }, function() {
      handleNoGeolocation(true);
    });
  }

});



// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

console.log(place.geometry.location.lat());

console.log(place.geometry.location.lng());

var newLatLng = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());

  var marker = new google.maps.Marker({
      position: newLatLng,
      map: map,
      draggable: true,
      title: 'Hello World!'
  });

  $('#latitude').val(marker.getPosition().lat());
  $('#longitude').val(marker.getPosition().lng());

  google.maps.event.addListener(marker, 'dragend', function(ev){
    $('#latitude').val(marker.getPosition().lat());
    $('#longitude').val(marker.getPosition().lng());
});

  map.setCenter(newLatLng);

  map.setZoom(18);

//console.log(place.geometry.location.toString());
console.log(componentForm);

  for (var component in componentForm) {
  //  document.getElementById(component).value = '';
   // document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
    });
  }
}

jQuery('#addpropertyrent').click(function(e) {
    var isValid = true;
    $('#sqft, #zip, #des, #bedrooms,#bathrooms, #autocomplete, #rentStartDate, #name, #street_number, #route, #locality, #administrative_area_level_1,#country, #rent, #deposit').each(function() {
      if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0' ) {
        isValid = false;
        $(this).css({
          "border": "1px solid red",
          "background-color": "#FFCECE"
        });
      } else {
        $(this).css({
          "border": "",
          "background-color": ""
        });
      }
    });

    $('#rent, #deposit').each(function() {
        var input_value=$.trim($(this).val());
        console.log(input_value);

      if (isNaN(input_value)) {
        alert("Rent and Deposit should be in numbers only")
        isValid = false;
        $(this).val('');
         $(this).css({
          "border": "1px solid red",
          "background-color": "#FFCECE"
        });

      } else {




      }
    });
    if (isValid == false) {
 alert("Please fill all highlighted fields.");
      e.preventDefault();
    }
});

    </script>


@stop
