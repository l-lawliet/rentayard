@extends('landlord.layout')

@section('title',ucfirst($property->name))

@section('content')

<div class="col-md-10 col-md-offset-1">
    <div class="widget">
        <div class="widget-head">
            <h4 class="heading">{{ucfirst($property->name)}}</h4> ViewProperty
            <div class="pull-right">
                <a class="btn btn-success btn-sm" href="{{URL::Route('viewProperty',array('id'=>$property->id))}}" target="_blank"> <i class="fa fa-eye"></i> {{trans('landlord.viewAsPublic')}} </a>
            </div>
        </div>
        <div class="widget-body innerAll">
            <form class="form-horizontal" action="{{URL::Route('updateProperty',array('id'=>$property->id))}}" method="post" enctype="multipart/form-data">
                <div class="row innerLR">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="form-group">
                            <label for="name" class="control-label col-md-4">{{trans('property.name')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="name" id="name" value="{{$property->name}}" minlength="25" maxlength="30"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="city">{{trans('property.city')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="city" id="city" value="{{$property->city}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="state">{{trans('property.state')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="state" id="state" value="{{$property->state}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="zip">{{trans('property.zip')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="zip"  id="zip" value="{{$property->zip}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="rent">{{trans('property.rent')}}</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input class="form-control" type="text" name="rent" id="rent" value="{{$property->rent}}"/>
                                    <span class="input-group-addon">.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="deposit">{{trans('property.deposit')}}</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input class="form-control" type="text" name="deposit" id="deposit" value="{{$property->deposit}}"/>
                                    <span class="input-group-addon">.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="bedrooms">{{trans('property.bedrooms')}}</label>
                            <div class="col-md-8">
                                <select class="form-control" name="bedrooms" id="bedrooms">
                                    <option value="0" {{($property->bedrooms=='0')?'selected':''}}>None</option>
                                    <option value="1" {{($property->bedrooms=='1')?'selected':''}}>1</option>
                                    <option value="2" {{($property->bedrooms=='2')?'selected':''}}>2</option>
                                    <option value="3" {{($property->bedrooms=='3')?'selected':''}}>3</option>
                                    <option value="4" {{($property->bedrooms=='4')?'selected':''}}>4</option>
                                    <option value="5" {{($property->bedrooms=='5')?'selected':''}}>5</option>
                                    <option value="6" {{($property->bedrooms=='6')?'selected':''}}>6</option>
                                    <option value="7" {{($property->bedrooms=='7')?'selected':''}}>7</option>
                                    <option value="8" {{($property->bedrooms=='8')?'selected':''}} >8</option>
                                    <option value="9" {{($property->bedrooms=='9')?'selected':''}}>9</option>
                                    <option value="10" {{($property->bedrooms=='10')?'selected':''}}>10+</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="bathrooms">{{trans('property.bathrooms')}}</label>
                            <div class="col-md-8">
                                <select class="form-control" name="bathrooms" id="bathrooms">
                                    <option value="0" {{($property->bathrooms=='0')?'selected':''}}>None</option>
                                    <option value="1" {{($property->bathrooms=='1')?'selected':''}}>1</option>
                                    <option value="2" {{($property->bathrooms=='2')?'selected':''}}>2</option>
                                    <option value="3" {{($property->bathrooms=='3')?'selected':''}}>3</option>
                                    <option value="4" {{($property->bathrooms=='4')?'selected':''}}>4</option>
                                    <option value="5" {{($property->bathrooms=='5')?'selected':''}}>5</option>
                                    <option value="6" {{($property->bathrooms=='6')?'selected':''}}>6</option>
                                    <option value="7" {{($property->bathrooms=='7')?'selected':''}}>7</option>
                                    <option value="8" {{($property->bathrooms=='8')?'selected':''}} >8</option>
                                    <option value="9" {{($property->bathrooms=='9')?'selected':''}}>9</option>
                                    <option value="10" {{($property->bathrooms=='10')?'selected':''}}>10+</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="sqft">{{trans('property.squareFootage')}}</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="sqft" id="sqft" value="{{$property->sqft}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="parking">{{trans('property.parking')}}</label>
                            <div class="col-md-8">
                                <select class="form-control" name="parking" id="parking">
                                    <option value="0" {{($property->parking=='0')?'selected':''}}>None</option>
                                    <option value="1" {{($property->parking=='1')?'selected':''}}>1 {{trans('property.cars')}}</option>
                                    <option value="2" {{($property->parking=='2')?'selected':''}}>2 {{trans('property.cars')}}</option>
                                    <option value="3" {{($property->parking=='3')?'selected':''}}>3 {{trans('property.cars')}}</option>
                                    <option value="4" {{($property->parking=='4')?'selected':''}}>4 {{trans('property.cars')}}</option>
                                    <option value="5" {{($property->parking=='5')?'selected':''}}>5 {{trans('property.cars')}}</option>
                                    <option value="6" {{($property->parking=='6')?'selected':''}}>6 {{trans('property.cars')}}</option>
                                    <option value="7" {{($property->parking=='7')?'selected':''}}>7 {{trans('property.cars')}}</option>
                                    <option value="8" {{($property->parking=='8')?'selected':''}} >8 {{trans('property.cars')}}</option>
                                    <option value="9" {{($property->parking=='9')?'selected':''}}>9 {{trans('property.cars')}}</option>
                                    <option value="10" {{($property->parking=='10')?'selected':''}}>10+ {{trans('property.cars')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="acceptApplication">{{trans('property.acceptApplications')}}</label>
                            <div class="col-md-8">
                                <div class="make-switch" data-on="success" data-off="danger"><input type="checkbox" name="acceptApplication" {{($property->accept_application==1)?'checked':''}}></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="rentStartDate">{{trans('property.rentStartDate')}}</label>
                            <div class="col-md-8">
                                <select class="form-control" name="rentStartDate" id="rentStartDate">
                                    @for($i=5;$i<29;$i++)
                                        <option value="{{$i}}" {{($property->rent_start_date==$i)?'selected':''}} >{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="des">Description</label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="des" id="des">{{$property->des}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-4">
                            <button class="btn btn-success" type="Submit">{{trans('property.updateProperty')}}</button>

                            <a href="{{URL::route('deleteProperty',array('id'=>$property->id))}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-danger btn-stroke">{{trans('user.delete')}}</a>
 
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop


@section('script')

<script src="{{asset('assets/plugins/forms_elements_bootstrap-switch/js/bootstrap-switch.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/forms_elements_bootstrap-switch/bootstrap-switch.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>

<script type="text/javascript">
  
$(window).on('beforeunload', function(){
        return "If you want refresh , All the data will be lost";
    });

    // Form Submit
    $(document).on("submit", "form", function(event){
        // disable warning
        $(window).off('beforeunload');
    });
</script>
 
@stop