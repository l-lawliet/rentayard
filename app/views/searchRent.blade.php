@extends('layout.searchlayout')

@section('content')

<div class="row home-search-bg">
    <div class="container">
<br>
        <br>
        <h2 class="text-center">&nbsp&nbsp&nbspRentaYawd locally across the island.</h2>   
        <br> 
        <form class="home-search" action="{{URL::route('search')}}" method="get">
            <div class="col-md-2 col-sm-2">
                <select class="form-control" style="display: none; padding: 14px 12px;" name="type">
                    <!-- <option value="buy">Buy</option> -->
                    <option value="rent" selected>Rent</option>
                </select>
            </div>
                <div class="col-md-8 col-sm-8">
                <input class="form-control" type="text" id="search-address" required autocomplete="off" name="search" placeholder="search using address" />
                </div>
                <div class="col-md-2 col-sm-2">
                <button class="form-control" type="submit">Search</button>
                </div>
        </form>
        <br><br>
        <div class="text-center">
        <a class="btn btn-success btn-lg" href="{{URL::Route('saveSearch')}}"> Save Search </a>
        </div>

        <br><br><br>
    </div>
</div>

<div class="container">

    <br>
    
    <div class ="row">
        <div class="widget widget-gallery" data-toggle="collapse-widget">
            <div class="widget-head"><h4 class="heading">Filtered search</h4></div>
            <div class="widget-body"> 
                <form action="{{URL::Route('filterSearchRent')}}" method="get">
                    <input type="hidden" value="<?php if(isset($_GET['search'])){ echo $_GET['search'];} ?>" name="search"/>
                <select name="min_price" id="min_price">
                    <option value="" selected="selected">Min Price</option>
                    <option value="10000">$10K</option>
                    <option value="20000">$20K</option>
                    <option value="30000">$30K</option>
                    <option value="40000">$40K</option>
                    <option value="50000">$50K</option>
                    <option value="60000">$60K</option>
                    <option value="70000">$70K</option>
                </select>
                <select name="max_price">
                    <option value="" selected="selected">Max Price</option>
                    <option value="70000">$70K</option>
                    <option value="80000">$80K</option>
                    <option value="90000">$90K</option>
                    <option value="100000">$100K</option>
                    <option value="110000">$110K</option>
                    <option value="120000">$120K</option>
                    <option value="130000">$130K</option>
                </select>
                <select name="bath" id="bath">
                    <option value="" selected="selected">All Baths</option>
                    <option value="0">0+</option>
                    <option value="1">1+</option>
                    <option value="2">2+</option>
                    <option value="3">3+</option>
                    <option value="4">4+</option>
                    <option value="5">5+</option>
                    <option value="6">6+</option>
                </select>
                <select name="bed" id="bed">
                    <option value="" selected="selected">All Beds</option>
                  <option value="0">0+</option>
                    <option value="1">1+</option>
                    <option value="2">2+</option>
                    <option value="3">3+</option>
                    <option value="4">4+</option>
                    <option value="5">5+</option>
                    <option value="6">6+</option>
                </select>
                    <select name="car" id="car">
                        <option value="" selected="selected">All Car Parkings</option>
                    <option value="0">0+</option>
                    <option value="1">1+</option>
                    <option value="2">2+</option>
                    <option value="3">3+</option>
                    <option value="4">4+</option>
                    <option value="5">5+</option>
                    <option value="6">6+</option>>
                    </select>
                <button type="submit" class="btn btn-success pull-right" style="height: 30px; position: relative;
  top: -1px;" > Filter </button>
                </form>
            </div>
        </div>
    </div>
  <div class="row">

    @foreach($propertiesArray['data'] as $property)
 
        <div class="col-md-4">
            <a href="{{URL::route('viewProperty',$property['id'])}}">
            <div class="widget">
                <div class="timeline-cover">
                    <div class="cover image ">
                        <div class="top">
                            @if(count($property['photos']))

                                <img src="{{ asset(Image::path('/uploads/property_photos/'.$property['photos'][0]['filename'], 'resizeCrop', 400, 200))}}" class="img-responsive">
                            @else
                                <img src="{{ asset(Image::path('/uploads/default_property_photo.jpg', 'resizeCrop', 400, 200))}}" class="img-responsive">
                            @endif
                        </div>
                    </div>
                    <div class="widget cover image">
                        <div class="widget-body padding-none margin-none">
                            <div class="photo">
                                   @if($property['landlord']['profile_picture'])
                                                    <img src="{{asset(Image::path('/uploads/'.$property['landlord']['profile_picture'], 'resizeCrop', 55, 55))}}" style="width:55px;height:55px;" class="img-circle">
                                                    @else
                                                    <img src="{{asset(Image::path('/uploads/default.jpg', 'resizeCrop', 55, 55))}}" style="width:55px;height:55px;" class="img-circle">
                                    @endif 
                        </div>
                            <div class="innerAll pull-left">
                                <p class="lead margin-none "> <i class="fa fa-home text-muted fa-fw"></i> {{ucfirst($property['name'])}} </p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="text-center innerAll">
                    <p class="lead margin-none "> <i class="fa fa-location-arrow text-muted fa-fw"></i> {{ucfirst($property['city'])}}, {{ucfirst($property['state'])}}, {{$property['zip']}} </p>
                    @if($property['rent'] > 0)
                    <p class="lead margin-none price">  Rent : $ {{$property['rent']}} </p>
                    <p>For rent</p>
                    @else
                    <p class="lead margin-none price ">  Price : $ {{$property['price']}} </p>
                    <p>For sale</p>
                    @endif
                </div>


            </div>
            </a>
        </div>
    @endforeach

    </div>

    {{$properties->links()}}
</div>
<br>
<br>
<div class="row footer">
    <div class="container">
    
            @include('footer')
       
    </div>
</div
@stop