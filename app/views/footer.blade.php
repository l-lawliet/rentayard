
        <footer class="clearfix">
            <div class="col-md-12">
                <div class="col-xs-12 col-md-3">
                    <h3><a href="#bout">About RentaYawd</a></h3>
                    RentaYawd is as explicit as its name state. RentaYawd allows anyone across the Caribbean to
                    host their home on the internet for rent. We are providing a medium that allows hosts and guests                                   to find each other. We do not want to only become a medium but our aim is to also provide a more                                   convenient and safe way for guests and hosts to conduct rental businesses. 
                </div>
                <div class="col-xs-12 col-md-3">
                    
                    <ul>
                    <h3><a>Quick links</a></h3>
                    <li><a href="#">Help</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Why Host</a></li>
                     <li><a href="#">Terms of Services</a></li>    
                    </ul>
                </div>
                <div class="social-icons col-xs-12 col-md-3">
                    <center>
                    <h3><a>Stay Connected</a></h3>
                    <a href="https://www.facebook.com/" title="RentaYawd on Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                    <a href="https://instagram.com/"><i class="fa fa-instagram"></i></a>
<!--                    <a href="https://www.linkedin.com/company/choice-business-solutions-ltd"><i class="fa fa-linkedin"></i></a>-->
                        </center>
                    
                </div>
                <div class="col-xs-12 col-md-3">
                    <h3><a>Contact Information</a></h3>
                    <address>
                        <strong>RentaYawd</strong>
                        <br>
                        <i class="glyphicon glyphicon-phone"></i>
                        We do not accept phone calls.
                        <br>
                        <i class="glyphicon glyphicon-envelope"></i>
                        contact@rentayawd.com
                        <br>
                         <i class="glyphicon glyphicon-eye-open"></i>
                        We are always available 24/7.
                        
                    </address>
                </div>
            </div>
        </footer>
