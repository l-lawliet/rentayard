@extends('layout.userSearchLayout')

@section('content')
<div class="row home-search-bg">
    <div class="container">
<br>
        <br>
        <h2 class="text-center">RentaYawd locally across the island.</h2>   
        <br> 
        <form class="home-search" action="{{URL::route('search')}}" method="get">
            <div class="col-md-2 col-sm-2">
                <select class="form-control" style="display: none; padding: 14px 12px;" name="type">
                   <!--  <option value="buy">Buy</option> -->
                    <option value="rent">Rent</option>
                </select>
            </div>
                <div class="col-md-8 col-sm-8">
                <input class="form-control" type="text" autocomplete="off" required id="search-address" name="search" placeholder="search using address" />
                </div>
                <div class="col-md-2 col-sm-2">
                <button class="form-control" type="submit">Search</button>
                </div>
        </form>
        <br><br>
        @if($flag==1)
        <div class="text-center">
        <a class="btn btn-success btn-lg" href="{{URL::Route('saveSearch')}}"> Save Search </a>
        </div>
        @endif
        <br><br><br>
    </div>
</div>
<br><br>

<div class="container">

 @if(Session::has('msg'))
 <div class="alert alert-info alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('msg')}}
</div>
  @endif


    <div class="clearfix"></div>
    <br>
 
    <div class="row">

    @foreach($propertiesArray['data'] as $property)
 
        <div class="col-md-4">
            <a href="{{URL::route('viewProperty',$property['id'])}}">
            <div class="widget">
                <div class="timeline-cover">
                    <div class="cover image ">
                        <div class="top">
                            @if(count($property['photos']))

                                <img src="{{ asset(Image::path('/uploads/property_photos/'.$hlike['photos'][0]['filename'], 'resizeCrop', 55, 55))}}" class="img-responsive">
                            @else
                                <img src="{{ asset(Image::path('/uploads/default_property_photo.jpg', 'resizeCrop', 55, 55))}}" class="img-responsive">
                            @endif
                        </div>
                    </div>
                    <div class="widget cover image">
                        <div class="widget-body padding-none margin-none">
                            <div class="photo">
                                   @if($property['landlord']['profile_picture'])
                                                    <img src="{{asset(Image::path('/uploads/'.$property['landlord']['profile_picture'], 'resizeCrop', 55, 55))}}" style="width:55px;height:55px;" class="img-circle">
                                                    @else
                                                    <img src="{{ asset(Image::path('/uploads/default.jpg', 'resizeCrop', 55, 55))}}" style="width:55px;height:55px;" class="img-circle">
                                    @endif 
                        </div>
                            <div class="innerAll pull-left">
                                <p class="lead margin-none "> <i class="fa fa-home text-muted fa-fw"></i> {{ucfirst($property['name'])}} </p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="text-center innerAll">
                    <p class="lead margin-none "> <i class="fa fa-location-arrow text-muted fa-fw"></i> {{ucfirst($property['city'])}}, {{ucfirst($property['state'])}}, {{$property['zip']}} </p>
                    @if($property['rent'] > 0)
                    <p class="lead margin-none price">  Rent : $ {{$property['rent']}} </p>
                    <p>For rent</p>
                    @else
                    <p class="lead margin-none price ">  Price : $ {{$property['price']}} </p>
                    <p>For sale</p>
                    @endif
                </div>


            </div>
            </a>
        </div>
    @endforeach

    </div>

    {{$properties->links()}}
</div>
<br>
<br>
<div class="row footer">
    <div class="container">
        <center>
           @include('footer')
        </center>
    </div>
</div>
@stop