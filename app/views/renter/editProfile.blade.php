@extends('renter.layout')

@section('title',trans('user.editProfile'))

@section('content')
 
@if(Session::has('msg'))
    <div class="alert alert-danger alert-dismissable">
   <button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
   {{Session::get('msg')}}
</div>
@endif

<div class="widget" id="editProfile">
    <div class="widget-head">
        <h4 class="heading">{{trans('user.editProfile')}}</h4> <a class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#myModelchange">Change Password</a>
    </div>
   
<div class="modal fade" id="myModelchange" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="background-color: white">
     <form role="form" action="{{{route('Renterchangepassword')}}}" method="post">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
      </div>
      <div class="modal-body">
       <label>Your Email Id</label>
         <input class="form-control" name="your_email" type="text">

      <label>Current password</label>
         <input class="form-control" name="current_password" type="password">
      
      <label>Change password</label>
         <input class="form-control" name="change_password" type="password" value="">

      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Save</button>
        <input name="role_id" type="hidden" value='2'> 

      </div>
    </div>
  </div>
</div>

</form>
    <div class="widget-body innerAll">
        <div class="row">
                    <?php $image = ($user->profile_picture)?$user->profile_picture:Config::get('app.default_dp'); ?>
                    <img src="{{asset(Image::path('/uploads/'.$image, 'resizeCrop', 32, 32))}}" class="pull-left media-object" style="width:35%;height:35%">

                    <form action="{{URL::Route('renterUpdateDp')}}" method="post" enctype="multipart/form-data">
                        <div class="col-md-10">
                            <label for="dp">Profile Picture :</label>
                            <div class="input-group">
                                <input class="form-control" type="file" name="profile_picture"/>
                                <div class="input-group-btn">
                                    <button class="btn btn-success">Upload</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
        <form class="form-horizontal" action="{{URL::Route('renterUpdateProfile')}}" method="post" enctype="multipart/form-data">
            <div class="row innerLR">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name" class="control-label col-md-4">{{trans('user.first_name')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="first_name" id="first_name" value="{{$user->first_name}}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="last_name" class="control-label col-md-4">{{trans('user.last_name')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="last_name" id="last_name" value="{{$user->last_name}}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone" class="control-label col-md-4">{{trans('user.phone')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="phone" id="phone" value="{{$user->phone}}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="salary" class="control-label col-md-4">{{trans('user.monthly_salary')}}</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" type="text" name="salary" id="salary" value="{{$user->salary}}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="designation" class="control-label col-md-4">{{trans('user.designation')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="designation" id="designation" value="{{$user->designation}}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="pets" class="control-label col-md-4">{{trans('user.pets')}}</label>
                        <div class="col-md-8">
                            <select multiple="multiple" name="pets[]" style="width: 100%;" id="pets">
                                <option value="cat" {{(in_array('cat',$user->pets))?'selected':''}} >Cat</option>
                                <option value="dog" {{(in_array('dog',$user->pets))?'selected':''}} >Dog</option>
                                <option value="fish" {{(in_array('fish',$user->pets))?'selected':''}} >Fish</option>
                                <option value="reptile" {{(in_array('reptile',$user->pets))?'selected':''}} >Reptile</option>
                                <option value="other" {{(in_array('other',$user->pets))?'selected':''}} >Other</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="about" class="control-label col-md-4">{{trans('user.about_yourself')}}</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="about" id="about">{{$user->about}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="address" class="control-label col-md-4">{{trans('user.address')}}</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="address" id="address">{{$user->address}}</textarea>
                        </div>
                    </div>
                </div>
<!--
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="address" class="control-label col-md-4">{{trans('user.paypal_email')}}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="paypal_email" id="paypal_email" value="{{$user->paypal_email}}"/>
                        </div>
                    </div>
                </div>
-->
            </div>
            <hr/>
            <h5 class="innerT"> {{trans('user.rentalHistory')}} </h5>
            <div id="rentalHistory">

            <?php 
            $i=0;
            $rh=array();
                if($user->rental_history!=NULL)
                {
                    foreach($user->rental_history as $rental)
                    { 
                    $i++;
                    array_push($rh,$i);
            ?>
                <div class="widget bg-gray innerAll margin-none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="houseAddress[]" class="control-label col-md-2">
                                    {{trans('user.address')}}
                                </label>
                                <div class="col-md-10">
                                    <input class="form-control" name="houseAddress[]" id="houseAddress[]" value="{{$rental->address}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseRent[]" class="control-label col-md-4">
                                    {{trans('user.rent')}}
                                </label>
                                <div class="input-group">
                                      <span class="input-group-addon">
                                        
                                      </span>
                                    <input class="form-control pre_rent{{$i}}" type="text" name="houseRent[]" id="houseRent[]" value="{{$rental->rent}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseTenancyLength[]" class="control-label col-md-4">
                                    {{trans('user.lengthOfTenancy')}}
                                </label>
                                <div class="input-group">
                                    <input class="form-control pre_rent_year{{$i}}" type="text" name="houseTenancyLength[]" id="houseTenancyLength[]" value="{{$rental->tenancyLength}}"/>
                                      <span class="input-group-addon">
                                        years
                                      </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseLandlordFirstName[]" class="control-label col-md-4">
                                    {{trans('user.landlordFirstName')}}
                                </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="houseLandlordFirstName[]" id="houseLandlordFirstName[]" value="{{$rental->landlordFirstName}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseLandlordLastName[]" class="control-label col-md-4">
                                    {{trans('user.landlordLastName')}}
                                </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="houseLandlordLastName[]" id="houseLandlordLastName[]" value="{{$rental->landlordLastName}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseLandlordPhone[]" class="control-label col-md-4">
                                    {{trans('user.landlordPhone')}}
                                </label>
                                <div class="col-md-8">
                                    <input class="form-control pre_rent_phone{{$i}}" type="text" name="houseLandlordPhone[]" id="houseLandlordPhone[]" value="{{$rental->landlordPhone}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="houseLandlordEmail[]" class="control-label col-md-4">
                                    {{trans('user.landlordEmail')}}
                                </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="email" name="houseLandlordEmail[]" id="houseLandlordEmail[]" value="{{$rental->landlordEmail}}"/>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-danger btn-block" type="button" onclick="deleteRental(this,{{$i}})">
                            {{trans('user.deleteThisRental')}}
                        </button>
                    </div>
                </div>
            <?php 
                    } 
                }
            ?>
             
            </div>
            <button class="btn btn-success btn-sm addRental" type="button">{{trans('user.addRental')}}</button>

            <h5 class="innerT"> {{trans('user.workHistory')}} </h5>
            <div id="workHistory">
            <?php 
            $w=0;
            $wh=array();
                if($user->work_history!=NULL)
                {
                    foreach($user->work_history as $work) 
                    { 
                    $w++;
                    array_push($wh,$w);
            ?>    
                <div class="widget bg-gray innerAll margin-none">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="workDesignation[]" class="control-label col-md-4">{{trans('user.designation')}}</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="workDesignation[]" id="workDesignation[]" value="{{$work->designation}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="workCompany[]" class="control-label col-md-4">{{trans('user.company')}}</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="workCompany[]" id="workCompany[]" value="{{$work->company}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="workSalary[]" class="control-label col-md-4">{{trans('user.monthly_salary')}}</label>
                                <div class="input-group"><span class="input-group-addon">$</span>
                                    <input class="form-control  pre_work_salary{{$w}}" type="text" name="workSalary[]" id="workSalary[]" value="{{$work->salary}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="workEmploymentLength[]" class="control-label col-md-4">{{trans('user.lengthOfEmployement')}}</label>
                                <div class="input-group">
                                    <input class="form-control pre_work_exper{{$w}}" type="text" name="workEmploymentLength[]" id="workEmploymentLength[]" value="{{$work->employmentLength}}" /><span class="input-group-addon">years</span>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-danger btn-block" type="button" onclick="deleteWork(this,{{$w}})">{{trans('user.deleteThisWork')}}</button>
                    </div>
                </div>
            <?php 
                    } 
                }
            ?>

            </div>
            <button class="btn btn-success btn-sm addWork" type="button">{{trans('user.addWork')}}</button>


            <h5 class="innerT"> {{trans('user.incomeSources')}} </h5>
            <div id="incomeSources">

            <?php 
            $t=0;
            $income_h=array();
                 if($user->additional_income!=NULL)
                 { 
                    foreach($user->additional_income as $income)
                    {
                    $t++;          
                    array_push($income_h,$t);
            ?>
                    
                <div class="widget bg-gray innerAll margin-none">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="incomeSource[]" class="control-label col-md-4">{{trans('user.incomeSource')}}</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="incomeSource[]" id="incomeSource[]" value="{{$income->source}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="incomeAmount[]" class="control-label col-md-4">{{trans('user.amount')}}</label>
                                <div class="input-group"><span class="input-group-addon">$</span>
                                    <input class="form-control pre_income{{$t}}" type="text" name="incomeAmount[]" id="incomeAmount[]" value="{{$income->amount}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 pull-right col-md-offset-1">
                            <button class="btn btn-danger" type="button" onclick="deleteIncome(this,{{$t}})">{{trans('user.delete')}}</button>
                        </div>

                    </div>
                </div>
            <?php 
                    }
                }
            ?>
            </div>
            <button class="btn btn-success btn-sm addIncome" type="button">{{trans('user.addIncomeSource')}}</button>


            <div class="row">
                <div class="center">
                    <button class="btn btn-success" id="rendereditprofile" type="Submit">{{trans('user.updateProfile')}}</button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('script')

    <script src="{{asset('assets/plugins/forms_elements_select2/js/select2.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script>

        $("#pets").select2({
            placeholder: "Select the Pets",
            allowClear: true
        });
    //Rental History add and delete
        rental="<?php echo $i; ?>";
        rental=Number(rental);
        if(rental >0)
        {
            var rh_array = [<?php echo '"'.implode('","', $rh).'"' ?>];
        }else{
            var rh_array=[];
        }

        rental=rental+1;
         
        var rentalHistory = '<div class="widget bg-gray innerAll margin-none"><div class="row"><div class="col-md-12"><div class="form-group"><label for="houseAddress[]" class="control-label col-md-2">{{trans('user.address')}}</label><div class="col-md-10"><input class="form-control" name="houseAddress[]" id="houseAddress[]"/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseRent[]" class="control-label col-md-4">{{trans('user.rent')}}</label><div class="input-group"><span class="input-group-addon">$</span><input class="form-control pre_rent'+rental+'" type="text" name="houseRent[]" id="houseRent[]"  value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseTenancyLength[]" class="control-label col-md-4">{{trans('user.lengthOfTenancy')}}</label><div class="input-group"><input class="form-control pre_rent_year'+rental+'" type="text" name="houseTenancyLength[]" id="houseTenancyLength[]" value=""/><span class="input-group-addon">years</span></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordFirstName[]" class="control-label col-md-4">{{trans('user.landlordFirstName')}}</label><div class="col-md-8"><input class="form-control" type="text" name="houseLandlordFirstName[]" id="houseLandlordFirstName[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordLastName[]" class="control-label col-md-4">{{trans('user.landlordLastName')}}</label><div class="col-md-8"><input class="form-control" type="text" name="houseLandlordLastName[]" id="houseLandlordLastName[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordPhone[]" class="control-label col-md-4">{{trans('user.landlordPhone')}}</label><div class="col-md-8"><input class="form-control pre_rent_phone'+rental+'" type="text" name="houseLandlordPhone[]" id="houseLandlordPhone[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordEmail[]" class="control-label col-md-4">{{trans('user.landlordEmail')}}</label><div class="col-md-8"><input class="form-control" type="email" name="houseLandlordEmail[]" id="houseLandlordEmail[]" value=""/></div></div></div><button class="btn btn-danger btn-block" type="button"   onclick="deleteRental(this,'+rental+')"> {{trans('user.deleteThisRental')}} </button></div></div>';

        function deleteRental(e,id)
        {
            var indexs = rh_array.indexOf(''+id);
            removeByIndex(rh_array,indexs);
            $(e).parent().parent().remove();
        }

        $('.addRental').on('click',function()
        {
            rh_array.push(rental);
            $('#rentalHistory').append(rentalHistory);
            rental=rental+1;
        });

    //working History and delete
        work_h="<?php echo $w; ?>";
        work_h=Number(work_h);
        if(work_h >0)
        {
            var wh_array = [<?php echo '"'.implode('","', $wh).'"' ?>];
        }else
        {
            var wh_array=[];
        }
        work_h=work_h+1;


        var workHistory = '<div class="widget bg-gray innerAll margin-none"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label for="workDesignation[]" class="control-label col-md-4">{{trans('user.designation')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="workDesignation[]" id="workDesignation[]" value="" /> </div> </div> </div> <div class="col-md-6"> <div class="form-group"> <label for="workCompany[]" class="control-label col-md-4">{{trans('user.company')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="workCompany[]" id="workCompany[]" value="" /> </div> </div> </div> <div class="col-md-6"> <div class="form-group"> <label for="workSalary[]" class="control-label col-md-4">{{trans('user.monthly_salary')}}</label> <div class="input-group"><span class="input-group-addon">$</span> <input class="form-control pre_work_salary'+work_h+'" type="text" name="workSalary[]" id="workSalary[]" value="" /> </div> </div> </div> <div class="col-md-6"> <div class="form-group"> <label for="workEmploymentLength[]" class="control-label col-md-4">{{trans('user.lengthOfEmployement')}}</label> <div class="input-group"> <input class="form-control pre_work_exper'+work_h+'" type="text" name="workEmploymentLength[]" id="workEmploymentLength[]" value="" /><span class="input-group-addon">years</span> </div> </div> </div> <button class="btn btn-danger btn-block" type="button" onclick="deleteWork(this,'+work_h+')">{{trans('user.deleteThisWork')}}</button> </div> </div>';

        function removeByIndex(arr, index)
        {
            arr.splice(index, 1);
        }


        function deleteWork(e,id)
        {
            var indexs = wh_array.indexOf(''+id);    
            removeByIndex(wh_array,indexs);
            $(e).parent().parent().remove();
        }

        $('.addWork').on('click',function()
        {
            wh_array.push(work_h);
            $('#workHistory').append(workHistory);
            work_h=work_h+1;
        });


        inc_h="<?php echo $t; ?>";
        inc_h=Number(inc_h);
        if(inc_h >0)
        {
            var income_array = [<?php echo '"'.implode('","', $income_h).'"' ?>];
        }else
        {
            var income_array=[];
        }
        inc_h=inc_h+1;

        var income = '<div class="widget bg-gray innerAll margin-none"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label for="incomeSource[]" class="control-label col-md-4">{{trans('user.incomeSource')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="incomeSource[]" id="incomeSource[]" value="" /> </div> </div> </div> <div class="col-md-3"> <div class="form-group"> <label for="incomeAmount[]" class="control-label col-md-4">{{trans('user.amount')}}</label> <div class="input-group"><span class="input-group-addon">$</span> <input class="form-control pre_income'+inc_h+'" type="text" name="incomeAmount[]" id="incomeAmount[]" value="" /> </div> </div> </div> <div class="col-md-2 pull-right col-md-offset-1"> <button class="btn btn-danger" type="button" onclick="deleteIncome(this,'+inc_h+')">{{trans('user.delete')}}</button> </div> </div> </div>';

        function deleteIncome(e,id)
        {
            var indexs = income_array.indexOf(''+id);  
            removeByIndex(income_array,indexs);
            $(e).parent().parent().remove();
        }

        $('.addIncome').on('click',function()
        {
            income_array.push(inc_h);
            $('#incomeSources').append(income);
            inc_h=inc_h+1;
        });


jQuery('#rendereditprofile').click(function(e)
{

        var isValid = true;

for(var j=0;j<rh_array.length;j++)
{
        
        var prerental_value_rent=$('.pre_rent'+rh_array[j]).val();
        var prerental_value_year=$('.pre_rent_year'+rh_array[j]).val();
        var prerental_value_phone=$('.pre_rent_phone'+rh_array[j]).val();

        if (isNaN(prerental_value_rent)) 
        {
            alert("Rent should be in numbers only")
            isValid = false;
            $('.pre_rent'+rh_array[j]).val('');
            $('.pre_rent'+rh_array[j]).css({
              "border": "1px solid red",
              "background-color": "#FFCECE"
            });
         
        }  
        
        if (isNaN(prerental_value_year)) 
        {
            alert("Tenancy year should be in numbers only")
            isValid = false;
            $('.pre_rent_year'+rh_array[j]).val('');
            $('.pre_rent_year'+rh_array[j]).css({
              "border": "1px solid red",
              "background-color": "#FFCECE"
            });
         
        } 

        if (isNaN(prerental_value_phone))
        {
            alert("Phone number should be in numbers only")
            isValid = false;
            $('.pre_rent_phone'+rh_array[j]).val('');
            $('.pre_rent_phone'+rh_array[j]).css({
              "border": "1px solid red",
              "background-color": "#FFCECE"
            });
         
        } 
    
}

   //pre work History
for(var j=0;j<wh_array.length;j++)
{
   
        var prework_salary=$('.pre_work_salary'+wh_array[j]).val();
        var prework_exp=$('.pre_work_exper'+wh_array[j]).val();
    
        if (isNaN(prework_salary)) 
        {
            alert("Salary Amount should be in numbers only")
            isValid = false;
            $('.pre_work_salary'+wh_array[j]).val('');
            $('.pre_work_salary'+wh_array[j]).css({
              "border": "1px solid red",
              "background-color": "#FFCECE"
            });
         
       }  

        if (isNaN(prework_exp))
        {
            alert("Length of Employement should be in numbers only")
            isValid = false;
            $('.pre_work_exper'+wh_array[j]).val('');
            $('.pre_work_exper'+wh_array[j]).css({
              "border": "1px solid red",
              "background-color": "#FFCECE"
        });
         
        }  
       
}


//income Source
//pre work History
for(var j=0;j< income_array.length;j++)
{
   
    var prework_source_salary=$('.pre_income'+income_array[j]).val();

            if (isNaN(prework_source_salary)) 
            {
            alert("Salary Amount should be in numbers only")
            isValid = false;
            $('.pre_income'+income_array[j]).val('');
            $('.pre_income'+income_array[j]).css({
                "border": "1px solid red",
                "background-color": "#FFCECE"
            });
                 
            }  

         
}

    $('#first_name, #last_name, #phone, #salary, #designation, #about, #address').each(function() 
    {
            if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0' )
            {
            isValid = false;
            $(this).css({
                "border": "1px solid red",
                "background-color": "#FFCECE"
            });
            } else {
                $(this).css({
                  "border": "",
                  "background-color": ""

            });
            }
    });


    $('#phone').each(function() 
    {
            var input_value=$.trim($(this).val());
                

            if (isNaN(input_value))
            {
            alert("Phone Number should be in numbers only");
            isValid = false;
            $(this).val('');
            $(this).css({
                "border": "1px solid red",
                "background-color": "#FFCECE"
            });
                 
            }  
    });

    $('#salary').each(function() 
    {

        var input_value=$.trim($(this).val());
            
        if (isNaN(input_value)) 
        {
        alert("Phone Number should be in numbers only");
        isValid = false;
        $(this).val('');
        $(this).css({
            "border": "1px solid red",
            "background-color": "#FFCECE"
        });

        }  
    });

   
    if (isValid == false)
    {
        alert("Please fill all highlighted fields.");
        e.preventDefault();
    } 
});

</script>

@stop