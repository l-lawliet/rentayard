<!DOCTYPE HTML> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Googlebot" content="noindex" />
	<meta http-equiv="Robots" content="noindex, nofollow" />
	<meta name="title" content="" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<title></title>

	<link rel="shortcut icon" href="/favicon.ico" />

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
</head>
<body>

	<script type="text/javascript">
		var autosubmit = function(){$('#autoform').submit();}
		$('document').ready(function(){$('a').click(autosubmit);autosubmit();})
	</script>
	<form id="autoform" target="_top" method="post" action="https://sandbox.paypal.com/cgi-bin/webscr">  
	<input type="hidden" name="cmd" id="cmd" value="_xclick" />  
	<input type="hidden" name="bn" id="bn" value="OrderDragon_BuyNow_WPS_IN" />  
	<input type="hidden" name="business" id="business" value="{{{ $payee_email }}}" />
	<input type="hidden" name="notify_url" id="notify_url" value="{{{ $ipn_url }}}" />
	<input type="hidden" name="return" id="return" value="{{{ $return_url }}}" />
	<input type="hidden" name="no_note" id="no_note" value="1" />
	<input type="hidden" name="currency_code" id="currency_code" value="USD" />
	<input type="hidden" name="charset" id="charset" value="utf-8" />
	<input type="hidden" name="rm" id="rm" value="2" />
	<input type="hidden" name="no_shipping" id="no_shipping" value="0" />
	<input type="hidden" name="shopping_url" id="shopping_url" value="{{{ $cancel_url }}}" />
	<input type="hidden" name="cancel_return" id="cancel_return" value="{{{ $cancel_url }}}" />
	<input type="hidden" name="item_name" id="item_name" value="{{{ $item_name }}}" />
	<input type="hidden" name="amount" id="amount" value="{{{ $amount }}}" />
	<input type="hidden" name="quantity" id="quantity" value="1" />
	<input type="hidden" name="cbt" id="cbt" value="Complete Purchase" />
		<style type="text/css" media="screen">
			<!--
			body {
				height: 100%;
				padding: 0;
				margin: 0;
				font-family: Verdana, Arial, Helvetica, sans-serif;
			} 
			#centered {
				margin: 75px auto;
				width: 740px;
				border: thin solid #333;
				padding: 10px;
				text-align: center;
			} 
		-->
	</style>

	<div id="centered">
		<p>Your are being redirected to paypal. If you are not redirected shortly, please click the button below.</p>
		<p><button type="submit">Continue</button></p>
	</div>

</form>
</body>
</html>