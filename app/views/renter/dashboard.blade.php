@extends('renter.layout')

@section('content')


<ul class="timeline-activity list-unstyled">
	@foreach($activities as $activity)
        @if($activity->activity=='joined_roomy')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <a href="" class="text-inverse">{{trans('user.you')}}</a> {{trans('user.joined')}}
                            <a href="" class="text-info"><i class="fa fa-location-arrow"></i> {{(Setting::has('sitename'))?Setting::get('sitename'):''}}</a>
                        </div>
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='applied')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <a href="{{URL::Route('renterEditProfile')}}" class="text-inverse">{{trans('user.you')}}</a> {{trans('user.applied')}}
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='friend_applied')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <a href="{{URL::Route('renterEditProfile')}}" class="text-inverse">{{trans('user.yourfriend')}}</a> {{trans('user.applied')}}
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='landlord_approved')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse">{{trans('user.landlordApproved')}}</span>
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                        @if($activity->application->status=='approved')
                        <div class="bg-gray innerAll border-top">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a class="btn btn-success btn-block accept" href="{{URL::Route('acceptApplication',array('id'=>$activity->application->id))}}" onClick="accept()"> {{trans('user.accept')}} </a>
                                </div>
                                <div class="col-sm-6">
                                    <a class="btn btn-danger btn-block reject" href="{{URL::Route('rejectApplication',array('id'=>$activity->application->id))}}" onClick="reject()"> {{trans('user.reject')}} </a>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='landlord_denied')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse">{{trans('user.landlordDenied')}}</span>
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='accepted')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse">{{trans('user.accepted')}}</span>
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='friend_accepted')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse">{{trans('user.friendAccepted',array('name'=>$activity->application->primary_tenant->first_name,'url'=>URL::Route('viewRenter',array('id'=>$activity->application->primary_tenant->id))))}}</span>
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='rejected')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse">{{trans('user.rejected')}}</span>
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='friend_rejected')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse">{{trans('user.friendRejected',array('name'=>$activity->application->primary_tenant->first_name))}}</span>
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='paid_rent')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse">{{trans('user.youPaidRent')}}</span>
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->application->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @elseif($activity->activity=='paid_deposit')
            <li class="active">
                <div class="block block-inline">
                <span class="marker"></span>
                    <div class="caret"></div>
                    <div class="box-generic">
                        <div class="timeline-top-info">
                            <i class="fa fa-user"></i>
                            <span class="text-inverse">{{trans('user.youPaidDeposit')}} </span>
                            @if(isset($activity->property))
                            <a href="{{URL::Route('viewProperty',array('id'=>$activity->property->id))}}" target="_blank" class="text-info"><i class="fa fa-building-o"></i> {{$activity->property->name}} </a>
                            @else
                            &lt;Property Removed&gt;
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @endif
	@endforeach


</ul>

{{$activities->links()}}



<div class="overlay" style="display:none;z-index:999;top:0;left:0;background-color:rgba(0,0,0,0.8);position:absolute;width:100%;height:100%;">

</div>
@stop

@section('script')
<script type="text/javascript">

    function accept()
    {
        document.getElementsByClassName("overlay").style.display = 'block';
    }

    function reject()
    {
        document.getElementsByClassName("overlay").style.display = 'block';
    }
    
</script>

@stop
