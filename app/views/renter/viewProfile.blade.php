@extends('layout')


@section('content')
<div class="container">

@include('partials.notifications')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="timeline-cover">
                <div class="widget border-bottom">

                    <div class="widget-body border-bottom">
                        <div class="media">
                            <div class="pull-left innerAll">
                                <?php $image = ($user->profile_picture)?$user->profile_picture:Config::get('app.default_dp'); ?>
                                <!-- <img src="{{ Image::path('/uploads/'.$image, 'resizeCrop', 32, 32) }}" class="img-circle"> -->
                               
                                <!-- <img src="http://localhost/truliashared/uploads/default.jpg" class="img-circle"> -->

                                <img src="{{asset(Image::path('uploads/'.$image, 'resizeCrop', 400, 200))}}" class="img-circle" style="width:30%;height:30%">
                            </div>
                            <div class="media-body">
                                <br>
                                <h4><a href="{{URL::route('viewRenter',$user->id)}}">{{$user->first_name}} {{$user->last_name}}</a> <a href="" class="text-muted"></a></h4>
                                <div class="clearfix"></div>
                                <p>{{$user->designation}}</p>
                                <p>{{$user->about}}</p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="col-md-6 col-md-offset-1">
       <div class="widget">
            <div class="widget-head border-bottom bg-gray">
                <h5 class="innerAll pull-left margin-none">{{trans('user.rentalHistory')}}</h5>

            </div>
            <div class="widget-body padding-none">
                @if(is_array($user->rental_history))
                    @foreach($user->rental_history as $rental)
                    <div class="innerAll">
                        <p class=" margin-none"><i class="fa fa-home fa-fw text-muted"></i> {{$rental->address}} </p>
                        <br>
                        <p> <span class="label label-default"> {{trans('user.rent')}} : </span> $ {{$rental->rent}} <span class="label label-default"> {{trans('user.lengthOfTenancy')}} : </span> {{$rental->tenancyLength}} {{trans('user.years')}}  </p>
                        <br>
                        <p class=" margin-none"><i class="fa fa-user fa-fw text-muted"></i> {{$rental->landlordFirstName}} {{$rental->landlordLastName}}, <i class="fa fa-enveloper"></i> {{$rental->landlordEmail}}, <i class="fa fa-phone"></i> {{$rental->landlordPhone}} </p>

                    </div>
                    @endforeach
                @else
                    <div class="innerAll">
                        <p class="margin-none">
                            {{trans('user.notAvailable')}}
                        </p>
                    </div>
                @endif
            </div>
        </div>
        <div class="widget">
            <div class="widget-head border-bottom bg-gray">
                <h5 class="innerAll pull-left margin-none">{{trans('user.workHistory')}}</h5>

            </div>
            <div class="widget-body padding-none">
                @if(is_array($user->work_history))
                    @foreach($user->work_history as $work)
                    <div class="innerAll">
                        <p class=" margin-none"><i class="fa fa-building fa-fw text-muted"></i> {{$work->designation}} {{trans('user.at')}} {{$work->company}}, $ {{$work->salary}}, {{$work->employmentLength}} {{trans('user.years')}} </p>
                    </div>
                    @endforeach
                @else
                    <div class="innerAll">
                        <p class="margin-none">
                            {{trans('user.notAvailable')}}
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="widget">
            <div class="widget-head border-bottom bg-gray">
                <h5 class="innerAll pull-left margin-none">{{trans('user.moreInfo')}}</h5>

            </div>
            <div class="widget-body padding-none">
                <div class="innerAll">
                    <p class=" margin-none"><i class="fa fa-phone fa-fw text-muted"></i> {{$user->phone}} </p>
                </div>
                <div class="border-top innerAll">
                    <p class=" margin-none"><i class="fa fa-home fa-fw text-muted"></i> {{$user->address}}</p>
                </div>
                <div class="border-top innerAll">
                    <p class=" margin-none"><i class="fa fa-dollar fa-fw text-muted"></i> {{$user->salary}}
                </div>
                <div class="border-top innerAll">
                    <p class=" margin-none"><i class="fa fa-bug fa-fw text-muted"></i> @foreach($user->pets as $pet) {{$pet}} @endforeach
                </div>
            </div>
        </div>
        @if(is_array($user->additional_income))
        <div class="widget">
            <div class="widget-head border-bottom bg-gray">
                <h5 class="innerAll pull-left margin-none">{{trans('user.additionalIncomeSources')}}</h5>

            </div>
            <div class="widget-body padding-none">

                    @foreach($user->additional_income as $income)
                    <div class="innerAll">
                        <p class=" margin-none"><i class="fa fa-dollar fa-fw text-muted"></i> {{$income->amount}} <span class="label label-default">{{$income->source}}</span> </p>
                    </div>
                    @endforeach

            </div>
        </div>
        @endif
    </div>
</div>
@stop