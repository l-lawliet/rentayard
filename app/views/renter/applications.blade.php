@extends('renter.layout')

@section('content')

@foreach($applications as $application)

@if($application->status!='denied')
    <div class="col-md-3">
        <div class="widget">
            <h5 class="innerAll margin-none border-bottom bg-gray">{{trans('landlord.application')}} #{{$application->id}}</h5>
            <div class="widget-body padding-none">
            @foreach($application->applicants as $applicant)
                <div class="media border-bottom innerAll margin-none">
                    <?php $image = ($applicant->user['profile_picture'])?$applicant->user['profile_picture']:Config::get('app.default_dp'); ?>
                    <!-- <img src="" class="pull-left media-object"> -->

                                        <img src="{{asset(Image::path('/uploads/'.$image, 'resizeCrop', 32, 32))}}" class="pull-left media-object" style="width:35%;height:35%">
 
                    <div class="media-body">
                        <h5 class="margin-none">
                        @if($applicant->user)
                            <a href="{{URL::route('viewRenter',$applicant->user['id'])}}" target="_blank">{{$applicant->user['first_name']}} {{$applicant->user['last_name']}}</a>
                        @else
                            {{$applicant->email}}
                        @endif
                        </h5>
                        <small> $ {{$applicant->amount}} </small>
                    </div>
                </div>
            @endforeach
                <div class="row">
                        @if($application->status=="sent")
                            <div class="ribbon-wrapper"><div class="ribbon primary"> Sent </div></div>
                        @elseif($application->status=="approved")
                            <div class="ribbon-wrapper"><div class="ribbon primary"> {{trans('landlord.approved')}} </div></div>
                        @elseif($application->status=="accepted")
                            <div class="ribbon-wrapper"><div class="ribbon primary"> {{trans('landlord.accepted')}} </div></div>
                        @elseif($application->status=="rejected")
                            <div class="ribbon-wrapper"><div class="ribbon danger"> {{trans('landlord.rejected')}} </div></div>
                        @endif
                </div>
            </div>
        </div>
    </div>
@endif

@endforeach

@stop