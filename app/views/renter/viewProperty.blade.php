@extends('renter.layout')

@section('content')

<!--
@if(!$tenant->depositPaid)
    <div class="alert alert-danger">
        {{trans('user.payDeposit')}}
        <span>
            <a class="btn btn-sm btn-danger" href="{{URL::Route('payDeposit',$property->id)}}"> {{trans('user.clickHereToPay')}} $ {{$property->deposit}}</a>
        </span>
    </div>
@elseif(!$tenant->rentPaid)

    <div class="alert alert-danger">
        {{trans('user.payRent')}}
        <span>
            <a class="btn btn-sm btn-danger" href="{{URL::Route('payRent',$property->id)}}"> {{trans('user.clickHereToPay')}}</a>
        </span>
    </div>

@else

<div class="widget widget-heading-simple widget-body-gray">
    <div class="widget-body center">
        <p class="lead margin-none">{{trans('user.nodue')}}</p>
    </div>
</div>

@endif
-->

@if($tenant->ask_for_vacate == 0)
<div class="alert alert-danger">
    {{trans('user.vacate')}}
        <span>
            <a href="{{route('renterVacate',$tenant->property_id)}}" class="btn btn-sm btn-info"> {{trans('user.clickforvacate')}} <?php echo date('Y-m-d', strtotime("+30 days")); ?>  </a> (+30 days from today)
        </span>
</div>
@elseif($tenant->ask_for_vacate == 1 && $tenant->vacate_from == 1 && $tenant->vacate_status == 1)
<div class="alert alert-danger">
    {{trans('user.vacate')}}
        <span>
            Status <a class="btn btn-sm btn-info"> Pending  </a> Waiting for Landlord approval
        </span>
</div>
@elseif($tenant->ask_for_vacate == 1 && $tenant->vacate_from == 1 && $tenant->vacate_status == 2)
<div class="alert alert-danger">
    {{trans('user.vacate')}}
        <span>
            Landlord <a class="btn btn-sm btn-info"> Approved  </a> your vacate. on {{$tenant->vacate_date}} (<?php $now = time(); $your_date = strtotime($tenant->vacate_date);$datediff = $now - $your_date; echo abs(floor($datediff/(60*60*24))); ?> days more )
        </span>
</div>
@elseif($tenant->ask_for_vacate == 1 && $tenant->vacate_from == 1 && $tenant->vacate_status == 3)
<div class="alert alert-danger">
    {{trans('user.vacate')}}
        <span>
            Previous Request Declined Please Try Again <a href="{{route('renterVacate',$tenant->property_id)}}" class="btn btn-sm btn-info"> {{trans('user.clickforvacate')}} <?php echo date('Y-m-d', strtotime("+30 days")); ?>  </a> (+30 days from today)
        </span>
</div>
@endif



<div class="widget">
	<div class="widget-head">
		<h4 class="heading">{{trans('user.transactions')}}</h4>
	</div>
	<div class="widget-body innerAll inner-2x">

		<!-- Table -->
		<table class="table table-striped table-responsive swipe-horizontal table-primary">

			<!-- Table heading -->
			<thead>
				<tr>
					<th> {{trans('user.amount')}} </th>
					<th> {{trans('user.as')}} </th>
					<th> {{trans('user.mode')}} </th>
					<th> {{trans('user.timestamp')}} </th>
				</tr>
			</thead>
			<!-- // Table heading END -->

			<!-- Table body -->
			<tbody>


                @foreach($rents as $rent)

                <!-- Table row -->
                <tr class="gradeA">
                    <td> $ {{$rent->amount}} </td>
                    <td> {{trans('user.rent')}} </td>
                    <td> {{$rent->mode}} </td>
                    <td> {{$rent->created_at}} </td>
                </tr>
                <!-- // Table row END -->

                @endforeach

                @if(isset($deposit))

                <!-- Table row -->
                <tr class="gradeA">
                    <td> $ {{$deposit->amount}} </td>
                    <td> {{trans('user.deposit')}} </td>
                    <td> {{$deposit->mode}} </td>
                    <td> {{$deposit->created_at}} </td>
                </tr>
                <!-- // Table row END -->

                @endif

			</tbody>
			<!-- // Table body END -->

		</table>
		<!-- // Table END -->

	</div>
</div>




@stop