@extends('layout')

@section('content')

<div class="layout-app col-fs log">
    <div class="row row-app">
        <div class="col-md-12">
            <div class="col-separator col-separator-first box col-unscrollable col-fs">
                <div class="col-table">
                    <div class="col-table-row">
                        <div class="col-app col-unscrollable tab-content">
                            <div class="col-app lock-wrapper lock-bg-1 tab-pane active animated fadeIn" id="lock-1-1">

                                <h3 class="text-white innerB text-center">Account Access</h3>
                                <div class="lock-container">
                                    @include('partials.notifications')
                                    <div class="innerAll text-center">
                                        {{Form::open(array('url'=>URL::route('processLogin')))}}
                                        <img src="{{asset('assets/images/people/100/22.jpg')}}" class="img-circle"/>
                                        <div class="innerLR">
                                            <input class="form-control text-center bg-gray" name="email" type="email"  placeholder="Enter your Login Email"/>
                                        </div>
                                        <div class="innerT">
                                            <button type="submit" class="btn btn-primary">Login <i class="fa fa-fw fa-unlock-alt"></i></a>
                                        </div>
                                        {{Form::close()}}
                                        <a href="{{URL::Route('signup')}}">
                                            {{trans('user.newToSite')}}
                                        </a><br /> 
                                        
					 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@stop
