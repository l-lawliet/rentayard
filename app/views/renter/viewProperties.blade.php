@extends('renter.layout')

@section('content')


    <br>
    <h3 class="pull-left margin-none innerR">{{Setting::get('sitename')}}</h3>
 
    <br>
    <div class="row" style="margin-top: 3%;">

    @foreach($propertiesArray['data'] as $property)

        <div class="col-md-4">
            <a href="{{URL::route('viewProperty',$property['id'])}}" target="_blank">
            <div class="widget">
                <div class="timeline-cover">
                    <div class="cover image ">
                        <div class="top">
                            @if(count($property['photos']))
                                <img src="{{ asset(Image::path('/uploads/property_photos/'.$property['photos'][0]['filename'], 'resizeCrop', 400, 200))}}" class="img-responsive">
                            @else
                                <img src="{{ asset(Image::path('/uploads/default_property_photo.jpg', 'resizeCrop', 400, 200))}}" class="img-responsive">
                            @endif
                        </div>
                    </div>
                    <div class="widget cover image">
                        <div class="widget-body padding-none margin-none">
                            <div class="photo">
                                @if($property['landlord']['profile_picture'])
                                <img src="{{asset(Image::path('/uploads/'.$property['landlord']['profile_picture'], 'resizeCrop', 400, 200))}}" style="width:55px;height:55px;" class="img-circle">
                                @else
                                <img src="{{asset(Image::path('uploads/default.jpg', 'resizeCrop', 400, 200))}}" style="width:55px;height:55px;" class="img-circle">
                                @endif 
                        </div>
                            <div class="innerAll pull-left">
                                <p class="lead margin-none "> <i class="fa fa-home text-muted fa-fw"></i> {{ucfirst($property['name'])}} </p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="text-center innerAll">
                    <p class="lead names margin-none "> <i class="fa fa-location-arrow text-muted fa-fw"></i> {{ucfirst($property['city'])}}, {{ucfirst($property['state'])}}, {{$property['zip']}} </p>
                    @if($property['rent'] > 0)
                    <p class="lead margin-none price ">  Rent : $ {{$property['rent']}} </p>
                    <p>For rent</p>
                    @else
                    <p class="lead margin-none price">  Price : $ {{$property['price']}} </p>
                    <p>For sale</p>
                    @endif
                </div>


            </div>
            </a>
        </div>
    @endforeach

    </div>

    {{$properties->links()}}

<br>
<br>

@stop