@extends('layoutUser')
 
@section('content')

<div class="row home-search-bg">
    <div class="container">
<br>
        <br>
        <h2 class="text-center">&nbspRentaYawd locally across the island.</h2>   
        <br> 
        <form class="home-search" action="{{URL::route('search')}}" method="get">
            <div class="col-md-2 col-sm-2">
                <select class="form-control" style="display: none; padding: 14px 12px;" name="type">
                    <!-- <option value="buy" selected>Buy</option> -->
                    <option value="rent">Rent</option>
                </select>
            </div>
            
                <div class="col-md-8 col-sm-8">
                <input class="form-control" type="text" required autocomplete="off" id="search-address" name="search" placeholder="search using address" />
                </div>
                <div class="col-md-2 col-sm-2">
                <button class="form-control" type="submit">Search</button>
                </div>
        </form>
        <br><br>

        <div class="text-center">
        <a class="btn btn-success btn-lg" href="{{URL::to('signup')}}"> Join Now </a>
        </div>
        
        <br><br><br>
    </div>
</div>
<br><br>

<div class="row home-service text-center">
    <div class="container">

    <div class="col-md-4">
        <h3>What will you get?</h3>
        <br>
        <img class="roundPic" src="{{asset('assets/images/ser1.gif')}}">
        <br>  <br>  <br> 
        
        <p>A chance to interact with the best host for guest across the island.</p>

    </div>

    <div class="col-md-4">
        <h3>Moving? Get a quote.</h3>
        <br>
        <img class="roundPic" src="{{asset('assets/images/ser2.gif')}}">
        <br> <br>  <br> 
        <p>We make relocating easier. We provide the best and most suitable host rents from our listed hosts</p>
        
    </div>

    <div class="col-md-4">
        <h3>What’s your home worth?</h3>
        <br>
        <img class="roundPic" src="{{asset('assets/images/ser3.gif')}}">
        <br> <br>  <br> 
        <p>Instantly find out your home’s value and connect with a local agent to guide you through the selling process.</p>
        
    </div>


    </div>
    <br><br><br>
</div>

<!--
<div class="row home-find">
    <div class="container">

        <div class="col-md-6">
        <br><br><br>
        <h1>Find homes on the go</h1>
        <br>
        <p>Roomly's top-rated mobile app shows homes for sale near you—anytime, anywhere. With hi-res photo galleries and detailed neighborhood maps, your future home will be at your fingertips.</p>
        <br>
        <p>Exploring homes! find right choice for right people. Ultimater destination for home seekers, the most convenient, efficent and faster means than ever. </p>
        <br>
        <p>Searching property within your budget was never so easy unless without the best solution provider for home seekers. Buying a home is now a hazel free with roomy.  Your future dream home is just at your fingertips.</p>
        <br>
        </div>
        <div class="col-md-6">
            <br><br><br>
        <img src="" style="width:80%;">
        </div>

    </div>

<br><br><br>
</div>
-->

<!-- <div class="row home-afford text-center">
    <div class="container">
    <br><br>
        <h1>How much can you afford?</h1>
        <h3><strong>Renting a home is a big deal. We can help you figure out how much money you have to work with and bring clarity to the process.</strong></h3>
        <br><br>
        <img src="" >
        <br><br><br><br>
<!-- <a class="button" href="#first-step">TAKE THE FIRST STEP</a> 
        <br><br><br><br>
    </div>
</div> -->

<div class="row footer">
    <div class="container">
@include('footer')

    </div>
</div>





@stop
