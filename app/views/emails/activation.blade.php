<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Activate your Account</h2>

<div>
    Hi {{$name}},

    {{Setting::get('email.activationMail')}}

    <p>
        <a href="{{URL::Route('activate',array('code' => $activationCode))}}"> Click here </a> to activate your account
    </p>
</div>
</body>
</html>
