<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<div>

    {{Setting::get('email.coapplicantInvite')}}

    <p>
        <a href="{{$propertyUrl}}"> Click here </a> to view the property.
    </p>

</div>
</body>
</html>
