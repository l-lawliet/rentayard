@extends('layoutUser')
 
@section('title',$property->name)

@section('content')

<div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <br>

                        <h3 class="pull-left margin-none innerR">{{Setting::get('sitename')}}</h3>
                        @if(Session::has('msg'))
                            <p>{{Session::get('msg')}}</p>
                        @endif
                        @if($savesearch != null)
                         @if(($savesearch->property_id == null))
                         <a class="btn btn-success pull-right shadow" style="margin-left:10px;" href="{{URL::route('saveHome', array('id' => $property->id))}}"> Save Home </a>
                         @else
                         <a class="btn btn-success pull-right shadow" style="margin-left:10px;" href="#" disabled="true"> Home already saved </a>
                         @endif
                        @else
                         <a class="btn btn-success pull-right shadow" style="margin-left:10px;" href="{{URL::route('saveHome', array('id' => $property->id))}}"> Save Home </a>
                        @endif
                        <a class="btn btn-success pull-right shadow" href="{{URL::previous()}}"> Back </a>
                        <div class="clearfix"></div>
                        <br>
            </div>
        </div>
@include('partials.notifications')

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="timeline-cover">
                    <div class="cover image ">
                        <div class="top">
                            @if(count($property->photos))
                               
                                 <img src="{{asset(Image::path('/uploads/property_photos/'.$property->photos[0]->filename, 'resizeCrop', 400, 200))}}" alt="photo" class="img-responsive" />

                            @else
           <img src="{{asset(Image::path('/uploads/property_photos/default_property_photo.jpg', 'resizeCrop', 400, 200))}}" alt="photo" class="img-responsive" />
                            @endif
                        </div>
                    </div>
                    <div class="widget cover image">
                        <div class="widget-body padding-none margin-none">
                           <div class="photo">
                                @if($property['landlord']['profile_picture'])
                                <img src="{{asset(Image::path('/uploads/'.$property['landlord']['profile_picture'], 'resizeCrop', 400, 200))}}" style="width:55px;height:55px;" class="img-circle">

                                @else
                                <img src="{{asset(Image::path('/uploads/default.jpg', 'resizeCrop', 55, 55))}}" style="width:55px;height:55px;" class="img-circle">
                                @endif 
                        </div>
                            <div class="innerAll border-right pull-left">
                                <h3 class="margin-none">{{ucfirst($property->landlord->first_name)}} {{ucfirst($property->landlord->last_name)}}</h3>
                            </div>
                            <div class="innerAll pull-left">
                                <p class="lead margin-none "> <i class="fa fa-home text-muted fa-fw"></i> {{ucfirst($property->name)}} </p>
                            </div>
                            <div class="innerAll pull-left">
                                <p class="lead margin-none "> <i class="fa fa-location-arrow text-muted fa-fw"></i> {{ucfirst($property->city)}}, {{ucfirst($property->state)}}, {{$property->zip}} </p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row sell-fst">
                    <div class="col-md-3">
                        <div class="widget innerAll inner-2x text-center">
                            <h3 class="margin-none"> Rent </h3>
                            <div class="separator bottom"></div>
                            <p class="innerB margin-none inner-2x text-xlarge text-success">$ {{$property->rent}}</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="widget innerAll inner-2x text-center">
                            <h3 class="margin-none"> Deposit </h3>
                            <div class="separator bottom"></div>
                            <p class="innerB margin-none inner-2x text-xlarge text-condensed text-success">$ {{$property->deposit}}</p>
                        </div>
                    </div>
<!--
                    <div class="col-md-3">
                        <div class="widget innerAll inner-2x text-center">
                            <h3 class="margin-none"> Square Footage </h3>
                            <div class="separator bottom"></div>
                            <p class="innerB margin-none inner-2x text-xlarge text-condensed text-success">{{$property->sqft}}</p>
                        </div>
                    </div>
-->
                    <div class="col-md-3">
                        <div class="widget innerAll">
                            <ul>
                                <li>Bathrooms : {{$property->bathrooms}}</li>
                                <li>Bedrooms : {{$property->bedrooms}}</li>
                                 <!-- add kitchen here-->
                                <li>Parking : {{$property->parking}}</li>
                            </ul>
                        </div>

                        @if($property->accept_application)
                            @if(Auth::check())
                                @if(Auth::user()->role_id!=2)
                                    <a class="btn btn-danger btn-block shadow1" href="{{URL::Route('login')}}"> {{trans('user.loginAsRenterToApply')}} </a>
                                @else
                                    @if(!$application)
                                    <a class="btn btn-success btn-block shadow" href="#applyNow"> {{trans('property.applyNow')}} </a>
                                    @else
                                    <a class="btn btn-danger btn-block shadow1" href="#"> Already Applied</a>
                                    @endif
                                @endif
                            @else
                                <a class="btn btn-danger btn-block shadow1" href="{{URL::Route('login')}}"> {{trans('user.signInToApply')}} </a>
                            @endif
                        @else
                            <a class="btn btn-danger btn-block shadow1" href="javascript:;"> {{trans('property.applicationClosed')}} </a>
                        @endif

                    </div>
                    
                </div>
                <div class="row">
                    
                    <div class="widget widget-gallery col-md-3" data-toggle="collapse-widget" style="height:333px;">
                        <div class="widget-head"><h4 class="heading">Details about House</h4></div>
                        <div class="widget-body">
                            <textarea readonly style="margin: 0px; height: 253px; width: 219px;resize: none;resize: none;" readonly>{{$property->des}}</textarea>
                        </div>
                    </div>
                    <div class="widget widget-gallery col-md-3" data-toggle="collapse-widget" style="height:333px;">
                        <div class="widget-head"><h4 class="heading">View Map</h4></div>
                        <div class="widget-body">
                         <div id="map-canvas" style="width: 55%; height: 260px;"></div>
                             
                        </div>
                    </div>
                    <div class="widget widget-gallery col-md-6" data-toggle="collapse-widget">
                        <div class="widget-head"><h4 class="heading">Contact info</h4></div>
                        <div class="widget-body">
                            <form class="form-horizontal" action="{{URL::Route('requestContact', array('id' => $userdel->id))}}" method="post">

                            <input type="hidden" name="property_id" value="{{$property->id}}"></input>

                                <div class="row innerLR">
                                    <div class="col-md-12">


                                        <div class="form-group">
                                            <label for="first_name" class="control-label col-md-3">{{trans('user.landlord_first_name')}}</label>
                                                <div class="col-md-5">
                                                    <input class="form-control" type="text" name="first_name" id="first_name" placeholder="First name" value="{{$userdel->first_name}}" disabled="true" />
                                                </div>
                                                <div class="col-md-4">
                                                    <input class="form-control" type="text" name="last_name" id="last_name" placeholder="Last name" value="{{$userdel->last_name}}" disabled="true" />
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="control-label col-md-3">{{trans('user.email')}}</label>
                                            <div class="col-md-9">
                                                <input class="form-control" type="text" name="email" id="email" placeholder="Landlord Email" value="{{$userdel->email}}" disabled="true"/>
                                            </div>
                                        </div>
                                        <hr style="margin-bottom: 14px;">
                                        <div class="form-group">
                                            <label for="phone" class="control-label col-md-3">{{trans('user.user_phone_num')}}</label>
                                            <div class="col-md-9">
                                                <input class="form-control" type="text" name="phone" id="phone" placeholder="{{trans('user.user_phone_num')}}"  />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="des" class="control-label col-md-3">{{trans('user.des')}}</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" width="40px" style="height:60px;" type="text" name="des" id="des"  >{{trans('user.description')}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-2"></div>
                                        @if(Auth::user()->role_id!=2)
                                            <a class="btn btn-danger btn-block shadow1" href="{{URL::Route('login')}}"> {{trans('user.loginAsRenterToApply')}} </a>
                                        @else
                                            <div class="col-md-4">
                                                <button class="btn btn-success" type="Submit">{{trans('user.request')}}</button>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="{{URL::previous()}}" class="btn btn-danger btn-stroke">{{trans('user.cancel')}}</a>
                                            </div>
                                        @endif
                                        <div class="col-md-2"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    @if(count($property->photos))
                    <div class="widget widget-gallery" data-toggle="collapse-widget">
                        <div class="widget-head"><h4 class="heading">Photo Gallery</h4></div>
                        <div class="widget-body">
                            <div class="gallery gallery-2">


                                <ul class="row">
                                    @foreach($property->photos as $photo)

                                    <li class="col-md-3 hidden-phone">
                                        <a class="thumb" data-gallery="gallery-2" href="{{asset(Image::path('/uploads/property_photos/'.$photo->filename, 'resizeCrop', 400, 200))}}"><img src="{{asset(Image::path('/uploads/property_photos/'.$photo->filename, 'resizeCrop', 400, 200))}}" alt="photo" class="img-responsive" /></a>
                                    </li>

                                    @endforeach
                                </ul>

                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="row">
                    <div class="widget widget-gallery" data-toggle="collapse-widget">
                        <div class="widget-head"><h4 class="heading">Ratings</h4></div>
                         <div id="aja" data="{{URL::route('ratings',array('id' => $property->id))}}"></div>
                        <div class="widget-body">
                            <!-- <div class="row">
                                <div class="col-md-3">
                                    <p>Rate this area</p>
                                </div>
                                <div id="aja" data="{{URL::route('ratings',array('id' => $property->id))}}"></div>
                                <div class="col-md-2">
                                    <div id="jRate" style="height:20px;width: 55px;"></div>
                                </div>
                                <div class="col-md-2">
                                    <p id="demo-onchange-value"></p>
                                </div>
                                <div class="col-md-2">
                                    <p>Overall area rating:</p>
                                </div>
                                <div class="col-md-1">
                                    <div id="jRate5" style="height:20px;width: 55px;"></div>
                                </div>
                            </div> -->
                            <!-- <div class="row">
                                <div class="col-md-3">
                                    <p><b>Rate these categories</b></p>
                                    <p>Safety</p>
                                    <p>Pet-friendly</p>
                                    <p>Walkablility</p>
                                    <p>Restaurant & Shopping</p>
                                </div>
                                <div class="col-md-2">
                                    <br>
                                    <div id="jRate1" style="height:20px;width: 55px;"></div>
                                    <div id="jRate2" style="height:20px;width: 55px;"></div>
                                    <div id="jRate3" style="height:20px;width: 55px;"></div>
                                    <div id="jRate4" style="height:20px;width: 55px;"></div>
                                </div>
                                <div class="col-md-2">
                                    <br>
                                    <p id="demo-onchange-value1"></p>
                                    <p id="demo-onchange-value2"></p>
                                    <p id="demo-onchange-value3"></p>
                                    <p id="demo-onchange-value4"></p>
                                </div>
                                <div class="col-md-2">
                                    <p><b>Top Rated categories</b></p>
                                    <div id="jRate6" style="height:20px;width: 55px;"></div>
                                    <div id="jRate7" style="height:20px;width: 55px;"></div>
                                    <div id="jRate8" style="height:20px;width: 55px;"></div>
                                    <div id="jRate9" style="height:20px;width: 55px;"></div>
                                </div>
                                <div class="col-md-2">
                                    <br>
                                    <p>Safety</p>
                                    <p>Pet-friendly</p>
                                    <p>Walkablility</p>
                                    <p>Restaurant & Shopping</p>
                                </div>
                            </div> -->
                            <div class="clearfix"></div>
                            <div class="row">
                            @if(Auth::check())

                            @if(Auth::user()->role_id!=2)
                                <div class="col-md-5">
                                <a class="btn btn-danger btn-lg" href="{{URL::Route('login')}}"> {{trans('user.loginAsRenterToRating')}} </a>
                                </div>
                                <div class="col-md-2">
                                </div>
                            @else
                                <div class="col-md-7">
                                    <a class="btn btn-success btn-lg " data-toggle="modal" data-target="#myModal">Rating and Review</a>
                                </div>
                            @endif
                            @else
                                <a class="btn btn-danger btn-block shadow1" href="{{URL::Route('login')}}"> {{trans('user.signInToApply')}} </a>
                            @endif
                                <div class="col-md-5">
                                   <a class="btn btn-info btn-lg pull-right" id ="displayRating">View all Ratings</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="ratingReview">
                    <div class="widget widget-gallery" data-toggle="collapse-widget">
                        <div class="widget-head"><h4 class="heading">All Ratings and Reviews</h4></div>
                        <div class="widget-body">
                        <div class="row">
                            <div class="col-md-4">
                                <p><b>Rate these categories</b></p>
                                <p>Safety</p>
                                <p>Pet-friendly</p>
                                <p>Walkablility</p>
                                <p>Restaurant & Shopping</p>
                                <p>Parks & Recreation</p>
                                <p>Schools</p>
                                <p>Traffic</p>
                                <p>Parking</p>
                                <p>Entertainment & Nightlife</p>
                                <p>Public Transportation</p>
                                <p>Community Involvement</p>
                                <p>Cleanliness</p>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <div id="jRate23" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate24" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate25" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate26" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate27" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate28" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate29" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate30" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate31" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate32" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate33" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                                <div id="jRate34" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                            </div>
                            <div class="col-md-6">
                                @foreach($ratings as $rate)
                                @if($rate->review_title)
                                    <?php $username = DB::table('users')->where('id',$rate->user_id)->first(); ?>
                                   <label>{{$username->first_name}} - </label>
                                @endif
                                   <label>{{$rate->review_title}}</label> <br>
                                   <label> {{$rate->description}}</label><br>
                                @endforeach
                                {{$ratings->links()}}
                            </div>
                        </div> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="widget widget-gallery" data-toggle="collapse-widget">
                        <div class="widget-head"><h4 class="heading">{{trans('property.homeLike')}}</h4></div>
                        <div class="widget-body">
                            <div class="row like-feature like-slide">    
                            @foreach($homeLike as $hlike)
 
                            <div class="col-md-2">
                                <a href="{{URL::route('viewProperty',$hlike->id)}}">
                                <div class="widget">
                                    <div class="timeline-cover">
                                        <div class="cover image">
                                            <div class="top">
                                                @if(count($hlike['photos']))
                                                    <img src="{{ asset(Image::path('/uploads/property_photos/'.$hlike['photos'][0]['filename'], 'resizeCrop', 400, 200))}}" class="img-responsive">
                                                @else
                                                    <img src="{{ asset(Image::path('/uploads/default_property_photo.jpg', 'resizeCrop', 400, 200))}}" class="img-responsive">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="widget cover image">
                                            <div class="widget-body padding-none margin-none">
                                                <div class="photo">
                                                    @if($property['landlord']['profile_picture'])
                                                    <img src="{{asset(Image::path('/uploads/'.$property['landlord']['profile_picture'], 'resizeCrop', 400, 200))}}" style="width:55px;height:55px;" class="img-circle">
                                                    @else
                                                    <img src="{{ asset(Image::path('/uploads/default.jpg', 'resizeCrop', 55, 55))}}" style="width:55px;height:55px;" class="img-circle">
                                                    @endif 
                                            </div>
                                                <div class="innerAll pull-left">
                                                    <p class="lead margin-none "> <i class="fa fa-home text-muted fa-fw"></i> {{ucfirst($hlike['name'])}} </p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="text-center innerAll">
                                        <p class="lead margin-none "> <i class="fa fa-location-arrow text-muted fa-fw"></i> {{ucfirst($hlike['city'])}}, {{ucfirst($hlike['state'])}}, {{$hlike['zip']}} </p>
                                        @if($hlike['rent'] > 0)
                                        <p class="lead margin-none ">  Rent : $ {{$hlike['rent']}} </p>
                                        <p>For rent</p>
                                        @else
                                        <p class="lead margin-none ">  Price : $ {{$hlike['price']}} </p>
                                        <p>For sale</p>
                                        @endif
                                    </div>


                                </div>
                                </a>
                            </div>
                        @endforeach
                            </div>                     
                        </div>
                    </div>
                </div>
                @if($property->accept_application)
                    @if(Auth::check())
                        @if(Auth::user()->role_id==2)
                            @if(!$application)
                            <div class="widget" id="applyNow">
                                <div class="widget-head">
                                    <h4 class="heading">{{trans('property.applyNow')}}</h4>
                                </div>
                                <div class="widget-body innerAll">
                                    <form class="form-horizontal" action="{{URL::Route('applyForProperty',array('id'=>$property->id))}}" method="post" enctype="multipart/form-data">
                                       <input type="hidden" name="paypal_email" value="{{$user->paypal_email}}"> </input>
                                        <div class="row innerLR">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="first_name" class="control-label col-md-4">{{trans('user.first_name')}}</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" type="text" name="first_name" id="first_name" value="{{$user->first_name}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="last_name" class="control-label col-md-4">{{trans('user.last_name')}}</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" type="text" name="last_name" id="last_name" value="{{$user->last_name}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone" class="control-label col-md-4">{{trans('user.phone')}}</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" type="text" name="phone" id="phone" value="{{$user->phone}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="salary" class="control-label col-md-4">{{trans('user.monthly_salary')}}</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">$</span>
                                                            <input class="form-control" type="text" name="salary" id="salary" value="{{$user->salary}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="designation" class="control-label col-md-4">{{trans('user.designation')}}</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" type="text" name="designation" id="designation" value="{{$user->designation}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="pets" class="control-label col-md-4">{{trans('user.pets')}}</label>
                                                    <div class="col-md-8">
                                                        <select multiple="multiple" name="pets[]" style="width: 55%;" id="pets">
                                                            <option value="cat" {{(in_array('cat',$user->pets))?'selected':''}} >Cat</option>
                                                            <option value="dog" {{(in_array('dog',$user->pets))?'selected':''}} >Dog</option>
                                                            <option value="fish" {{(in_array('fish',$user->pets))?'selected':''}} >Fish</option>
                                                            <option value="reptile" {{(in_array('reptile',$user->pets))?'selected':''}} >Reptile</option>
                                                            <option value="other" {{(in_array('other',$user->pets))?'selected':''}} >Other</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="about" class="control-label col-md-4">{{trans('user.about_yourself')}}</label>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control" name="about" id="about">{{$user->about}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address" class="control-label col-md-4">{{trans('user.address')}}</label>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control" name="address" id="address">{{$user->address}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <h5 class="innerT"> {{trans('user.rentalHistory')}} </h5>
                                        <div id="rentalHistory">
                                        @if($user->rental_history!=NULL)
                                            @foreach($user->rental_history as $rental)
                                            <div class="widget bg-gray innerAll margin-none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="houseAddress[]" class="control-label col-md-2">
                                                                {{trans('user.address')}}
                                                            </label>
                                                            <div class="col-md-10">
                                                                <input class="form-control" name="houseAddress[]" id="houseAddress[]" value="{{$rental->address}}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="houseRent[]" class="control-label col-md-4">
                                                                {{trans('user.rent')}}
                                                            </label>
                                                            <div class="input-group">
                                                                  <span class="input-group-addon">
                                                                    $
                                                                  </span>
                                                                <input class="form-control" type="text" name="houseRent[]" id="houseRent[]" value="{{$rental->rent}}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="houseTenancyLength[]" class="control-label col-md-4">
                                                                {{trans('user.lengthOfTenancy')}}
                                                            </label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text" name="houseTenancyLength[]" id="houseTenancyLength[]" value="{{$rental->tenancyLength}}"/>
                                                                  <span class="input-group-addon">
                                                                    years
                                                                  </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="houseLandlordFirstName[]" class="control-label col-md-4">
                                                                {{trans('user.landlordFirstName')}}
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" type="text" name="houseLandlordFirstName[]" id="houseLandlordFirstName[]" value="{{$rental->landlordFirstName}}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="houseLandlordLastName[]" class="control-label col-md-4">
                                                                {{trans('user.landlordLastName')}}
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" type="text" name="houseLandlordLastName[]" id="houseLandlordLastName[]" value="{{$rental->landlordLastName}}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="houseLandlordPhone[]" class="control-label col-md-4">
                                                                {{trans('user.landlordPhone')}}
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" type="text" name="houseLandlordPhone[]" id="houseLandlordPhone[]" value="{{$rental->landlordPhone}}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="houseLandlordEmail[]" class="control-label col-md-4">
                                                                {{trans('user.landlordEmail')}}
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" type="email" name="houseLandlordEmail[]" id="houseLandlordEmail[]" value="{{$rental->landlordEmail}}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-danger btn-block" type="button" onclick="deleteRental(this)">
                                                        {{trans('user.deleteThisRental')}}
                                                    </button>
                                                </div>
                                            </div>
                                            @endforeach
                                        @endif
                                        </div>
                                        <button class="btn btn-success btn-sm addRental" type="button">{{trans('user.addRental')}}</button>

                                        <h5 class="innerT"> {{trans('user.workHistory')}} </h5>
                                        <div id="workHistory">
                                        @if($user->work_history!=NULL)
                                            @foreach($user->work_history as $work)
                                            <div class="widget bg-gray innerAll margin-none">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="workDesignation[]" class="control-label col-md-4">{{trans('user.designation')}}</label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" type="text" name="workDesignation[]" id="workDesignation[]" value="{{$work->designation}}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="workCompany[]" class="control-label col-md-4">{{trans('user.company')}}</label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" type="text" name="workCompany[]" id="workCompany[]" value="{{$work->company}}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="workSalary[]" class="control-label col-md-4">{{trans('user.monthly_salary')}}</label>
                                                            <div class="input-group"><span class="input-group-addon">$</span>
                                                                <input class="form-control" type="text" name="workSalary[]" id="workSalary[]" value="{{$work->salary}}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="workEmploymentLength[]" class="control-label col-md-4">{{trans('user.lengthOfEmployement')}}</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text" name="workEmploymentLength[]" id="workEmploymentLength[]" value="{{$work->employmentLength}}" /><span class="input-group-addon">years</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-danger btn-block" type="button" onclick="deleteWork(this)">{{trans('user.deleteThisWork')}}</button>
                                                </div>
                                            </div>
                                            @endforeach
                                        @endif
                                        </div>
                                        <button class="btn btn-success btn-sm addWork" type="button">{{trans('user.addWork')}}</button>


                                        <h5 class="innerT"> {{trans('user.incomeSources')}} </h5>
                                        <div id="incomeSources">
                                            @if($user->additional_income!=NULL)
                                            @foreach($user->additional_income as $income)
                                            <div class="widget bg-gray innerAll margin-none">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="incomeSource[]" class="control-label col-md-4">{{trans('user.incomeSource')}}</label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" type="text" name="incomeSource[]" id="incomeSource[]" value="{{$income->source}}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="incomeAmount[]" class="control-label col-md-4">{{trans('user.amount')}}</label>
                                                            <div class="input-group"><span class="input-group-addon">$</span>
                                                                <input class="form-control" type="text" name="incomeAmount[]" id="incomeAmount[]" value="{{$income->amount}}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 pull-right col-md-offset-1">
                                                        <button class="btn btn-danger" type="button" onclick="deleteIncome(this)">{{trans('user.delete')}}</button>
                                                    </div>

                                                </div>
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                        <button class="btn btn-success btn-sm addIncome" type="button">{{trans('user.addIncomeSource')}}</button>

                                        <h5 class="innerT"> {{trans('user.coapplicants')}} </h5>
                                        <div id="coapplicants">

                                        </div>

                                        <button class="btn btn-success btn-sm addCoapplicant" type="button">{{trans('user.inviteCoapplicant')}}</button>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button class="btn btn-success" type="Submit">{{trans('property.applyNow')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endif
                        @endif
                    @endif
                @endif
            </div>
        </div>
    </div>


<!-- Blueimp Gallery -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev no-ajaxify">‹</a>
    <a class="next no-ajaxify">›</a>
    <a class="close no-ajaxify">×</a>
    <a class="play-pause no-ajaxify"></a>
    <ol class="indicator"></ol>
</div>
<!-- // Blueimp Gallery END -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">What do you think of this area?</h4>
      </div>
      <div class="modal-body">
      <div class="row">
          <div class="col-md-6">
              <p>Overall rating </p>
          </div>
          <div class="col-md-6">
                <div id="jRate10" style="height:20px;width: 55px;"></div>       
          </div>
          <br><br>
      </div>
        <div class="row rate-str">
            <div class="col-md-4">
                <p><b>Rate these categories</b></p>
                <p>Safety</p>
                <p>Pet-friendly</p>
                <p>Walkablility</p>
                <p>Restaurant & Shopping</p>
                <p>Parks & Recreation</p>
                <p>Schools</p>
                <p>Traffic</p>
                <p>Parking</p>
                <p>Entertainment & Nightlife</p>
                <p>Public Transportation</p>
                <p>Community Involvement</p>
                <p>Cleanliness</p>
            </div>
            <div class="col-md-2">
                <p></p>
                <div class="str" id="jRate11" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate12" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate13" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate14" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate15" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate16" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate17" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate18" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate19" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate20" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate21" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
                <div class="str" id="jRate22" style="height:20px;width: 55px;padding-bottom: 27px;!important"></div>
            </div>
            <div class="col-md-6">
            <br>
            <form action="{{{route('review', array('id' => $property->id))}}}" method="post">
                <p style="padding-left:14px;"><b>Add Your Review</b></p>
                <div class="form-group" style="padding-left:14px;">
                    <label for="email" class="control-label">Review Title</label><br>
                    <input class="form-control" type="text" name="review_title" id="review_title" placeholder="Review title"/>
                </div>
                <div class="form-group" style="padding-left:14px;">
                    <label for="email" class="control-label col-md-4">Description</label><br>
                        <textarea class="form-control" width="274px" height="101px" type="text" name="description" id="des" placeholder="Add your description" ></textarea>
                </div>
                <div class="form-group" style="padding-left:14px;">
                    <label for="place" class="control-label">How do you know this area?</label><br>
                    <select name="place">
                        <option>--select--</option>
                        <option value="I live here">I live here</option>
                        <option value="I used to live here">I used to live here</option>
                        <option value="I want to live here">I want to live here</option>
                        <option value="I work here">I work here</option>
                        <option value="I visited here">I visited here</option>
                    </select>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
@stop

@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $("#ratingReview").hide();
});
    $("#displayRating").click(function()
    {
        $("#ratingReview").show();
    });
</script>
<script src="{{asset('assets/rating/test/jquery.min.js')}}"></script>
<script src="{{asset('assets/rating/dist/jRate.min.js')}}"></script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 0
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate1").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 1
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value1').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate2").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 2
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value2').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate3").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 3
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value3').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate4").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 4
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate5").jRate({
            rating: "{{$overall}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate6").jRate({
            rating: "{{$safety}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate7").jRate({
            rating: "{{$pet_friendly}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate8").jRate({
            rating: "{{$walkability}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate9").jRate({
            rating: "{{$restaurants}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate10").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 0
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate11").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 1
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate12").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 2
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate13").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 3
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate14").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 4
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate15").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 5
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate16").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 6
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate17").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 7
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate18").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 8
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate19").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 9
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate20").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 10
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate21").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 11
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate22").jRate({
            rating: 0,
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
                var url = $("#aja").attr("data");
                $.ajax(url,{
                   data : {
                        rate : rating,
                        val : 12
                   },
                   type :"GET",
                   success:function(response){
                     if(response.success)
                     {
                        console.log(response.msg);
                    }
                   }
                 });
            },
            onChange: function(rating) {
            $('#demo-onchange-value4').text("Your Rating: "+rating);
        }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate23").jRate({
            rating: "{{$safety}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate24").jRate({
            rating: "{{$pet_friendly}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate25").jRate({
            rating: "{{$walkability}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate26").jRate({
            rating: "{{$restaurants}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate27").jRate({
            rating: "{{$parks}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate28").jRate({
            rating: "{{$schools}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate29").jRate({
            rating: "{{$traffic}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate30").jRate({
            rating: "{{$parking}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate31").jRate({
            rating: "{{$entertainment}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate32").jRate({
            rating: "{{$transportation}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate33").jRate({
            rating: "{{$community}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {
        var that = this;
        $("#jRate34").jRate({
            rating: "{{$cleanliness}}",
            strokeColor: 'black',
            width: 20,
            height: 20,
            onChange: function(rating) {
                console.log("OnChange: Rating: "+rating);
            },
            onSet: function(rating) {
                console.log("OnSet: Rating: "+rating);
            }
        });
        
    });
</script>

    <script src="{{asset('assets/plugins/media_blueimp/js/blueimp-gallery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/media_blueimp/js/jquery.blueimp-gallery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/forms_elements_select2/js/select2.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script>
        $("#pets").select2({
            placeholder: "Select the Pets",
            allowClear: true
        });


        var rentalHistory = '<div class="widget bg-gray innerAll margin-none"><div class="row"><div class="col-md-12"><div class="form-group"><label for="houseAddress[]" class="control-label col-md-2">{{trans('user.address')}}</label><div class="col-md-10"><input class="form-control" name="houseAddress[]" id="houseAddress[]"/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseRent[]" class="control-label col-md-4">{{trans('user.rent')}}</label><div class="input-group"><span class="input-group-addon">$</span><input class="form-control" type="text" name="houseRent[]" id="houseRent[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseTenancyLength[]" class="control-label col-md-4">{{trans('user.lengthOfTenancy')}}</label><div class="input-group"><input class="form-control" type="text" name="houseTenancyLength[]" id="houseTenancyLength[]" value=""/><span class="input-group-addon">years</span></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordFirstName[]" class="control-label col-md-4">{{trans('user.landlordFirstName')}}</label><div class="col-md-8"><input class="form-control" type="text" name="houseLandlordFirstName[]" id="houseLandlordFirstName[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordLastName[]" class="control-label col-md-4">{{trans('user.landlordLastName')}}</label><div class="col-md-8"><input class="form-control" type="text" name="houseLandlordLastName[]" id="houseLandlordLastName[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordPhone[]" class="control-label col-md-4">{{trans('user.landlordPhone')}}</label><div class="col-md-8"><input class="form-control" type="text" name="houseLandlordPhone[]" id="houseLandlordPhone[]" value=""/></div></div></div><div class="col-md-6"><div class="form-group"><label for="houseLandlordEmail[]" class="control-label col-md-4">{{trans('user.landlordEmail')}}</label><div class="col-md-8"><input class="form-control" type="email" name="houseLandlordEmail[]" id="houseLandlordEmail[]" value=""/></div></div></div><button class="btn btn-danger btn-block" type="button" onclick="deleteRental(this)"> {{trans('user.deleteThisRental')}} </button></div></div>';

        function deleteRental(e){
            $(e).parent().parent().remove();
        }

        $('.addRental').on('click',function(){
            $('#rentalHistory').append(rentalHistory);
        });

        var workHistory = '<div class="widget bg-gray innerAll margin-none"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label for="workDesignation[]" class="control-label col-md-4">{{trans('user.designation')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="workDesignation[]" id="workDesignation[]" value="" /> </div> </div> </div> <div class="col-md-6"> <div class="form-group"> <label for="workCompany[]" class="control-label col-md-4">{{trans('user.company')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="workCompany[]" id="workCompany[]" value="" /> </div> </div> </div> <div class="col-md-6"> <div class="form-group"> <label for="workSalary[]" class="control-label col-md-4">{{trans('user.monthly_salary')}}</label> <div class="input-group"><span class="input-group-addon">$</span> <input class="form-control" type="text" name="workSalary[]" id="workSalary[]" value="" /> </div> </div> </div> <div class="col-md-6"> <div class="form-group"> <label for="workEmploymentLength[]" class="control-label col-md-4">{{trans('user.lengthOfEmployement')}}</label> <div class="input-group"> <input class="form-control" type="text" name="workEmploymentLength[]" id="workEmploymentLength[]" value="" /><span class="input-group-addon">years</span> </div> </div> </div> <button class="btn btn-danger btn-block" type="button" onclick="deleteWork(this)">{{trans('user.deleteThisWork')}}</button> </div> </div>';

        function deleteWork(e){
            $(e).parent().parent().remove();
        }

        $('.addWork').on('click',function(){
            $('#workHistory').append(workHistory);
        });

        var income = '<div class="widget bg-gray innerAll margin-none"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label for="incomeSource[]" class="control-label col-md-4">{{trans('user.incomeSource')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="incomeSource[]" id="incomeSource[]" value="" /> </div> </div> </div> <div class="col-md-3"> <div class="form-group"> <label for="incomeAmount[]" class="control-label col-md-4">{{trans('user.amount')}}</label> <div class="input-group"><span class="input-group-addon">$</span> <input class="form-control" type="text" name="incomeAmount[]" id="incomeAmount[]" value="" /> </div> </div> </div> <div class="col-md-2 pull-right col-md-offset-1"> <button class="btn btn-danger" type="button" onclick="deleteIncome(this)">{{trans('user.delete')}}</button> </div> </div> </div>';

        function deleteIncome(e){
            $(e).parent().parent().remove();
        }

        $('.addIncome').on('click',function(){
            $('#incomeSources').append(income);
        });

        var coapplicant = '<div class="widget bg-gray innerAll margin-none"> <div class="row">  <div class="col-md-6"> <div class="form-group"> <label for="coapplicantEmail" class="control-label col-md-4">{{trans('user.email')}}</label> <div class="col-md-8"> <input class="form-control" type="text" name="coapplicantEmail[]" id="coapplicantEmail" value=""/> </div> </div> </div> <div class="col-md-4"> <div class="form-group"> <label for="coapplicantAmountShare" class="control-label col-md-6">{{trans('user.amount')}}</label> <div class="col-md-6"> <div class="input-group"> <span class="input-group-addon">$</span> <input class="form-control" type="text" name="coapplicantAmountShare[]" id="coapplicantAmountShare" value=""/> </div> </div> </div> </div> <div class="col-md-2 pull-right"> <button class="btn btn-danger btn-block" type="button" onclick="deleteCoapplicant(this)">{{trans('user.delete')}}</button> </div> </div> </div>';

        function deleteCoapplicant(e){
            $(e).parent().parent().parent().remove();
        }

        $('.addCoapplicant').on('click',function(){
            $('#coapplicants').append(coapplicant);
        });

    </script>

    <script src="http://maps.googleapis.com/maps/api/js"></script>

<script>
$( document ).ready(function() {
var lat = {{$property->latitude}};
var log = {{$property->longitude}};
var myCenter=new google.maps.LatLng(lat,log);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:8,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);


}


google.maps.event.addDomListener(window, 'load', initialize);

});

</script>

<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>
   <script type="text/javascript">
        $(document).ready(function(){
            $('.like-slide').slick({
  dots: false,
  speed: 1300,
  slidesToShow: 6,
  slidesToScroll: 1,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 1300,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 6,
        slidesToScroll: 6,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
                
        });
    </script>     


@stop