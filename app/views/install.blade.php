
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if gt IE 8]> <html class="ie paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if !IE]><!--><html class="paceCounter paceSocial sidebar sidebar-social footer-sticky"><!-- <![endif]-->
<head>
    <title>@yield('title')</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <link rel="icon" href="{{Setting::get('logo')}}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

    <!--[if lt IE 9]><link rel="stylesheet" href="../assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->

    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:700' rel='stylesheet' type='text/css'>
    <link href="{{asset('assets/css/skins/module.admin.stylesheet-complete.skin.green.min.css')}}" rel="stylesheet" />
     <link href="{{asset('assets/css/custom-style.css')}}" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.3.15/slick.css"/>

        <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('assets/images/apple-touch-icon-144-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('assets/images/apple-touch-icon-114-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('assets/images/apple-touch-icon-72-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" href="{{asset('assets/images/apple-touch-icon-57-precomposed.png')}}">
  <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}">
  <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script src="{{asset('assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
    <script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
<style>
h3.logo
{
font-family: 'Dancing Script', cursive;
color:#8bbf61;
font-size: 38px;
font-weight: bold;
}
</style>

</head>
<body>
 

  
      <div class="form-group" style="margin-top:10%">
                                           
        <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <form role="form" action="{{URL::route('installationprocess')}}" method="post">
                <div class="col-md-6">
                     <label> Admin Username</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="User Name" name="username">
                </div><br>

                <div class="col-md-6">
                    <label> Admin Email </label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Email" name="email">
                </div>

                <div class="col-md-6">
                    <label> password </label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="password" name="password">
                </div>

                <div class="col-md-6">
                    <label> Mandrill Username</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Mandrill username" name="mandrill_username">
                </div>

                <div class="col-md-6">
                    <label> Mandrill Password</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Mandrill password" name="mandrill_password">
                </div>

                

                <div class="col-md-6">
                    <label> DataBase Name </label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Database Name" name="database_name">
                </div>

                 <div class="col-md-6">
                    <label> DataBase User Name </label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Database user Name" name="database_user_name">
                </div>

                 <div class="col-md-6">
                    <label> DataBase password </label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Database Password" name="database_password">
                </div>

                <input type="submit" class="btn btn-primary btn-block" value="Setup Site">
                     </form>
            </div>

        <div class="col-md-3"></div>
            
        </div>
                                             
   




</body>
</html>